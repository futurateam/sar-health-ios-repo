//
//  SalesManufacture.swift
//  SAR Health
//
//  Created by Anargha K on 17/09/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit


    class SalesManufacture: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
        @IBOutlet var valueTable: UITableView!
        @IBOutlet var totalText: UILabel!
        @IBOutlet var valueText: UILabel!
        @IBOutlet var valueAmount: UILabel!
        
        
        
        var noDataArray : NSMutableArray = [], noDataDic = [String :String](), branchCode = "", custArray : NSMutableArray = [], manCode = ""
        var elementValue: String?, manfactArray : NSMutableArray = []
        var element : String?
        var noData : Bool = false, salesManfactArray : NSMutableArray = [], salesManfact = [String:String]()
        var outstandingDetArray : NSMutableArray = [], outstandingDet = [String : String]()
        var total : Double = 0, custCode = "", custmerName = "", branchName = "", amt = [Double](), fromDate = "" , toDate = ""
        var outstandingValue : NSMutableArray = [], num = NSNumberFormatter()
        var no = ["No."], manfact = ["Manufacture"], amount = ["Amount(₹)"], customerLeg = [String](), customerLegend = [String]()
        override func viewDidLoad() {
            super.viewDidLoad()
            num.numberStyle = NSNumberFormatterStyle.DecimalStyle
            let indiaLocale = NSLocale(localeIdentifier: "en_IN")
            num.locale = indiaLocale
          
            for (var i = 0; i < outstandingValue.count; i += 1){
                no.append(String(i+1))
                manfact.append((outstandingValue[i].valueForKey("MANUFATURE") as? String)!)
                amount.append((outstandingValue[i].valueForKey("AMOUNT") as? String)!)
                total = total + Double((outstandingValue[i].valueForKey("AMOUNT") as? String)!)!
            }
            //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
            totalText.text = num.stringFromNumber(total)
            
            //        no = ["1", "2", "3", "4", "5", "6", "7"]
            //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
            //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
            //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
           
                valueText.text = "Total sales in " + branchName + " branch is"
            
            valueAmount.text = "₹ " + num.stringFromNumber(total)!
            // Do any additional setup after loading the view.
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        
        // MARK: - Navigation
        
        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
            /*  if segue.identifier == "toGraphOutstandingSum"{
             amt.removeAll()
             let dest = segue.destinationViewController as! GraphViewController
             for (var i = 1; i < amount.count ; i += 1) {
             amt.append(Double(amount[i])!)
             }
             
             dest.chartData = amt
             dest.xaxisName = "Customer"
             dest.yaxisName = "Amount"
             
             
             dest.cusprodText = "Customer"
             if custmerName == "" {
             dest.cusProdValue = "All"
             }
             else {
             dest.cusProdValue = custmerName
             }
             for (var i = 1 ; i < customer.count ; i++) {
             customerLeg.append(customer[i])
             }
             dest.chartLegend = customerLeg
             dest.titleTab = "Outstanding Report"
             
             }*/
            if segue.identifier == "toPieChart"{
                print(outstandingValue)
                amt.removeAll()
                customerLegend.removeAll()
                let dest = segue.destinationViewController as! PieChartViewController
                dest.fromDateStr = ""
                dest.toDateStr = ""
                dest.to = ""
                dest.titleTab = "Sales Performance(Manufacture)"
                for (var i = 0 ; i < outstandingValue.count ; i++) {
                    customerLegend.append(outstandingValue[i].valueForKey("MANUFATURE") as! String)
                    amt.append(Double(outstandingValue[i].valueForKey("AMOUNT") as! String)!)
                }
                dest.chartData = amt
                dest.chartLegend = customerLegend
            }
            if segue.identifier == "toSalesManfactDet" {
                let dest = segue.destinationViewController as? SalesPerfCustDetailed
                
                    dest?.custArray = salesManfactArray
                
                dest?.cellNo = 8
                dest?.fromDate = fromDate
                dest?.toDate = toDate
                dest?.branchName = branchName
                
                dest?.branchCode = branchCode
               
                
                
                
                
            }
            

            
        }
        
        func getSalesManDet()
        {
            
            HUD.showUIBlockingIndicatorWithText("Loading...")
            
            // let scriptUrl = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED?BRANCH=1&COMPANY=1&DIVISION=1&CUSTOMER=nahas&FRMDATE=2016-03-10&TODATE=2016-03-30"
            
            let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_MANUFACTURE_DETAIL?MANUFACTURE=" + manCode + "&BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&FRMDATE=" + fromDate + "&TODATE=" + toDate
            let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
            print("URL : ",scriptUrl)
            // Add one parameter
            //let urlWithParams = scriptUrl + "?BRANCH=1"
            
            // Create NSURL Ibject
            let myUrl = NSURL(string: scriptUrl);
            
            // Creaste URL Request
            let request = NSMutableURLRequest(URL:myUrl!);
            
            // Set request HTTP method to GET. It could be POST as well
            request.HTTPMethod = "GET"
            
            // If needed you could add Authorization header value
            // Add Basic Authorization
            /*
             let username = "myUserName"
             let password = "myPassword"
             let loginString = NSString(format: "%@:%@", username, password)
             let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
             let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
             request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
             */
            
            // Or it could be a single Authorization Token value
            //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
            
            // Excute HTTP Request
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                // Check for error
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                // Print out response string
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                
                
                
                print("data=\(data?.length)")
                let parser = NSXMLParser(data: data!)
                parser.delegate = self
                parser.parse()
                print("Cust Det Array",self.salesManfactArray)
                
                
                //            for (var i=0; i < self.customerArray.count; i++ ) {
                //                for (var j=(i+1); j < self.customerArray.count; j++) {
                //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
                //                        self.customerArray.removeObjectAtIndex(i);
                //                        j--;
                //                    }
                //                }
                //            }
                //            for(var i = 0; i < self.customerArray.count; i++){
                //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
                //            }
                dispatch_async(dispatch_get_main_queue(), {
                    
                    if (self.noData == true){
                        HUD.hideUIBlockingIndicator()
                        Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                        
                        
                    }
                    else{
                        HUD.hideUIBlockingIndicator()
                        self.performSegueWithIdentifier("toSalesManfactDet", sender: self)
                        
                        
                        
                    }
                })
                //Convert server json response to NSDictionary
                //                        do {
                //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                //
                //                                // Print out dictionary
                //                                print(convertedJsonIntoDict)
                //
                //                                // Get value by key
                //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
                //                                print(firstNameValue!)
                //
                //                            }
                //                        }
                //                        catch let error as NSError {
                //                            print(error.localizedDescription)
                //                        }
                //            if self.branchLabel.text == "All"{
                //
                //
                //                self.performSegueWithIdentifier("toProductAll", sender: self)
                //            }
                //            else{
                //                //getDataProductAll()
                //                if self.check == true{
                //
                //                    print(self.productStock)
                //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
                //                }
                //                else {
                //                    HUD.hideUIBlockingIndicator()
                //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
                //
                //                }
                //            }
                //
                
            }
            
            task.resume()
            
            
        }
        

        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return no.count
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = valueTable.dequeueReusableCellWithIdentifier("SalesManfactCell", forIndexPath: indexPath) as! SalesPerfManfactCell
            cell.no.text = no[indexPath.row]
            cell.manfact.text = manfact[indexPath.row]
            // cell.outstanding.text = amount[indexPath.row]
            if indexPath.row == 0 {
                cell.amount.text = amount[indexPath.row]
                
                cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            }
            else {
                cell.amount.text = num.stringFromNumber(Double(amount[indexPath.row])!)
                
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            return cell
        }
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            salesManfactArray.removeAllObjects()
            //        for (var i = 0; i < custArray.count; i += 1){
            //            if custArray[i].valueForKey("CUSTOMER") as? String == customer[indexPath.row]{
            //                custCode = (custArray[i].valueForKey("CODE") as? String)!
            //            }
            //        }
            
          
            for(var i = 0 ; i < manfactArray.count ; i++){
            if (outstandingValue[indexPath.row - 1].valueForKey("MANUFATURE") as! String == manfactArray[i].valueForKey("MFG_NAME") as! String) {
                    manCode = manfactArray[i].valueForKey("PR_MFG_ID") as! String
                }
            }
            
           print(manCode)
            getSalesManDet();

        }
        
        
        
              func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
            if (elementName == "Table1"){
                if elementName == "Column1" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    noDataDic[elementName] = ""
                }
            }
            
            if (elementName == "PR_MFG_ID" || elementName == "MANUFATURE" || elementName  == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "QTY" || elementName == "AMOUNT"){
                
                noData = false
                elementValue = String()
                element = elementName
                salesManfact[elementName] = ""
                
            }
        }
        
        func parser(parser: NSXMLParser, foundCharacters string: String) {
            if (element == "PR_MFG_ID" || element == "MANUFATURE" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "QTY" || element == "AMOUNT"){
                if string.rangeOfString("\n") == nil{
                    salesManfact[element!] = string
                        
                }
            }
                
        }
        

        
        func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
                       if (elementName == "AMOUNT"){
                
                salesManfactArray.addObject(salesManfact)
                
            }
        }
        
        
        
        
        
        @IBAction func backPressed(sender: AnyObject) {
            navigationController?.popViewControllerAnimated(true)
        }
        @IBAction func viewGraphPressed(sender: AnyObject) {
            
            // performSegueWithIdentifier("toGraphOutstandingSum", sender: self)
            performSegueWithIdentifier("toPieChart", sender: self)
        }
}

