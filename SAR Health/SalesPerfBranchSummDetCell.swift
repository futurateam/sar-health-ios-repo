
//
//  SalesPerfBranchSummDetCell.swift
//  SAR Health
//
//  Created by Anargha K on 11/07/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchSummDetCell: UITableViewCell {
    @IBOutlet var no: UILabel!
    
    @IBOutlet var manfact: UILabel!
    
    @IBOutlet var product: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
}
