//
//  SalesPerfItemIndCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 09/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfItemIndCell: UITableViewCell {
    @IBOutlet var invNo: UILabel!
    @IBOutlet var invDate: UILabel!
    @IBOutlet var customerName: UILabel!
    @IBOutlet var batch: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var custName: UILabel!
   
    
    
}
