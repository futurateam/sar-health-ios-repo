//
//  SalesPerfItemBranchDetCell.swift
//  SAR Health
//
//  Created by Anargha K on 23/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfItemBranchDetCell: UITableViewCell {

    
    @IBOutlet var no: UILabel!
    @IBOutlet var custName: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var category: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
