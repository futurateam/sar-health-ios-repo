//
//  BranchTableSelectCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 13/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class BranchTableSelectCell: UITableViewCell {

    @IBOutlet var branchName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
