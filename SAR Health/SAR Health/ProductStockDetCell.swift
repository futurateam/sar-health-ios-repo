//
//  ProductStockDetCell.swift
//  SAR Health
//
//  Created by Anargha K on 23/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductStockDetCell: UITableViewCell {

    @IBOutlet var batch: UILabel!
    @IBOutlet var stock: UILabel!
}
