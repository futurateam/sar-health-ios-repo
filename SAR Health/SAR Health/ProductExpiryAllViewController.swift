//
//  ProductExpiryAllViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 16/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductExpiryAllViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    
    @IBOutlet var productExpiryText: UILabel!
    @IBOutlet var productExpiryTable: UITableView!
    @IBOutlet var scroller: UIScrollView!
    
//    var no = [String]()
//    var branch = [String]()
//    var batch = [String]()
//    var quantity = [String]()
//    var date = [String]()
    var productName = ""
    var productExpiry : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var productArray : NSMutableArray = []
    var expiry = [String:String]()
    var finyear = [String:String](), finyearArray : NSMutableArray = [], branchCode = "" , prodCode = ""
    var branchName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       getFinyear()
        for (var i=0; i < productExpiry.count; i++ ) {
            for (var j=(i+1); j < productExpiry.count; j++) {
                if ((productExpiry[i].valueForKey("BATCH") as? String == productExpiry[j].valueForKey("BATCH") as? String) && (productExpiry[i].valueForKey("BRANCH") as? String == productExpiry[j].valueForKey("BRANCH") as? String) && (productExpiry[i].valueForKey("EXPIRY") as? String == productExpiry[j].valueForKey("EXPIRY") as? String) && (productExpiry[i].valueForKey("STOCK") as? String == productExpiry[j].valueForKey("STOCK") as? String)) {
                    productExpiry.removeObjectAtIndex(i);
                    j--;
                }
            }
        }
      
//        no = ["1", "2", "3", "4", "5", "6"]
//        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6"]
//        batch = ["Batch 1", "Batch 2", "Batch 3", "Batch 4", "Batch 5", "Batch 6"]
//        quantity = ["10", "10", "10", "10", "10", "10"]
//        date = ["02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015"]
        self.productExpiryTable.tableFooterView = UIView.init(frame: CGRectZero)
        productExpiryText.text = "Product Expiry of " + productName + " in All Branches"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if segue.identifier == "toProductExpiry"{
            let dest = segue.destinationViewController as! ProductExpiryBranch
            dest.branchName = branchName
            dest.productName = productName
            dest.productExpiry = productArray
          
        }

    }

    
    
    //Mark: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productExpiry.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = productExpiryTable.dequeueReusableCellWithIdentifier("ProductExpiryDataCell", forIndexPath: indexPath) as! ProductExpiryAllCell
        cell.no.text = String(indexPath.row + 1)
        cell.branch.text = productExpiry[indexPath.row].valueForKey("BRANCH") as? String
        
        cell.quantity.text = productExpiry[indexPath.row].valueForKey("STOCK") as? String
        
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = productExpiryTable.dequeueReusableCellWithIdentifier("HeaderCell") as! ProductExpiryAllHeaderCell
               //cell.frame.size.height = 40.0
                return cell
    
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        branchCode = productExpiry[indexPath.row].valueForKey("BRN_NO") as! String
        branchName = productExpiry[indexPath.row].valueForKey("BRANCH") as! String
        productArray.removeAllObjects()
        getProductExpiry()
    }
    
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    func getProductExpiry()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
       let scriptUrl1 = Constants().API + "MOBAPP_PRODUCT_EXPIRY?BRANCH="+branchCode+"&PRODUCT="+prodCode+"&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_EXPIRY?BRANCH=1&PRODUCT=8260"
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parserProduct = NSXMLParser(data: data!)
            parserProduct.delegate = self
            parserProduct.parse()
            dispatch_async(dispatch_get_main_queue(), {
                print(self.productArray)
                if (self.productArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No data found..", viewController: self)
                    
                }
                else{
                     HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toProductExpiry", sender: self)
                    }
            })
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "BATCH" || elementName == "BRANCH" || elementName == "EXPIRY" || elementName == "STOCK"){
                
                elementValue = String()
                element = elementName
                expiry[elementName] = ""
                
            }
            if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
               
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
        }
        
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "BATCH" || element == "BRANCH" || element == "EXPIRY" || element == "STOCK"){
                if (string.rangeOfString("\n") == nil){
                    expiry[element!] = string
                }
            }
            if ((element == "FIN_YEAR_ID" || element == "FINYEAR")) {
                if (string.rangeOfString("\n") == nil) {
                    finyear[element!] = string
                }
            }
        }
        
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName == "EXPIRY"){
            productArray.addObject(expiry)
        }
        if (elementName == "FINYEAR"){
            finyearArray.addObject(finyear)
        }

    }

    
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
   
    @IBAction func checkAnotherPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

}
