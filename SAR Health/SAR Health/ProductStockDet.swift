//
//  ProductStockDet.swift
//  SAR Health
//
//  Created by Anargha K on 23/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductStockDet: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var branchTable: UITableView!
   
    @IBOutlet var stockText: UILabel!
    
    
    var productStock : NSMutableArray = []
    var batch = ["Batch"], stock = ["Stock"]
      var stockTotal : NSInteger = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.hideUIBlockingIndicator()
       
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6"];
        //        result = ["100", "250", "50", "40", "160", "140"];
     
        for (var i = 0; i < productStock.count; i++){
            
            batch.append((productStock[i].valueForKey("batch") as? String)!)
            stock.append((productStock[i].valueForKey("stock") as? String)!)
            stockTotal = stockTotal + Int((productStock[i].valueForKey("stock") as? String)!)!
        }
      
        
        
        
        
        stockText.text = String(stockTotal)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return batch.count;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = branchTable.dequeueReusableCellWithIdentifier("ProductStockDetCell", forIndexPath: indexPath) as! ProductStockDetCell
        
        
        
        if(batch[indexPath.row] == "Batch"){
            
            
            var longestWordRange = (batch[indexPath.row] as NSString).rangeOfString(batch[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: batch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.batch.attributedText = attributedString
            longestWordRange = (stock[indexPath.row] as NSString).rangeOfString(stock[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: stock[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.stock.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.batch.text = batch[indexPath.row]
            cell.stock.text = stock[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        return cell;
        
    }
    

    
    

    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
