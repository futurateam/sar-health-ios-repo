//
//  CorporateOutSalesCell.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSalesCell: UITableViewCell {
    
    
    @IBOutlet var customer: UILabel!
    @IBOutlet var no: UILabel!
    @IBOutlet var branch: UILabel!
   
    @IBOutlet var manufacture: UILabel!
    @IBOutlet var qty: UILabel!
    @IBOutlet var amount: UILabel!

}
