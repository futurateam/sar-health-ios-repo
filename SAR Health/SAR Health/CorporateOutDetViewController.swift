//
//  CorporateOutDetViewController.swift
//  SAR Health
//
//  Created by Anargha K on 19/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutDetViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
  
    
    
   // @IBOutlet weak var totalText: UILabel!
    @IBOutlet var outstandingTable: UITableView!
    @IBOutlet var outstandingText: UILabel!
    @IBOutlet var amountText: UILabel!
    
    @IBOutlet var totalText: UILabel!
    var elementValue: String?
    var element : String?
    var sum : Double = 0, manfactName = "", noData : Bool = true
    var corpDet : NSMutableArray = [], branchCode = "", prodCode = "", manCode = "", outstandingInd = [String : String](), outstandingIndArray : NSMutableArray = [], customerArray : NSMutableArray = [], custCode = ""
    var no = ["No."]
    var customer = ["Customer"]
    var billDate = ["Bill Date"]
    var billNo = ["Bill No"]
    var days = ["Days"]
    var value = ["Outstanding(₹)"], brn = [String]()
    var customerLegend = [String](), amount = [Double]()
    var  i = 0, qtyTot : Int = 0
    var j = 0
    var count = 0
    var limit = 1
    var total : Double = 0
    var customerName = "", num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        for (i = 0 ; i < corpDet.count ; i++){
            no.append(String(i + 1))
            brn.append(corpDet[i].valueForKey("branch") as! String)
            customer.append(corpDet[i].valueForKey("customer") as! String)
               value.append(num.stringFromNumber(Double((corpDet[i].valueForKey("outstanding") as? String)!)!)!)
             total = total + Double((corpDet[i].valueForKey("outstanding") as? String)!)!
            print(total)
        }
        totalText.text = num.stringFromNumber(total)
      /*  for ( i = 0; i < corpDet.count ; i += 1){
            print("i: ",i)
            count = i
            limit = 1
          //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
            
            
            let branch = corpDet[i].valueForKey("branch") as? String
//            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
//            prName.append("Product")
            
            brn.append(branch!)
            brn.append("")
            no.append(String(limit))
            customer.append((corpDet[i].valueForKey("CUSTOMER") as? String)!)
            billDate.append((corpDet[i].valueForKey("BillDate") as? String)!)
            billNo.append((corpDet[i].valueForKey("BillNo") as? String)!)
            days.append((corpDet[i].valueForKey("Days") as? String)!)
            value.append(num.stringFromNumber(Double((corpDet[i].valueForKey("Outstanding") as? String)!)!)!)
            total = Double((corpDet[i].valueForKey("Outstanding") as? String)!)!
            for ( j = i+1 ; j < corpDet.count ; j += 1){
                if corpDet[j].valueForKey("branch") as? String == branch{
                    brn.append("")
                     customer.append((corpDet[j].valueForKey("CUSTOMER") as? String)!)
                    billDate.append((corpDet[j].valueForKey("BillDate") as? String)!)
                    billNo.append((corpDet[j].valueForKey("BillNo") as? String)!)
                    days.append((corpDet[j].valueForKey("Days") as? String)!)
                    value.append(num.stringFromNumber(Double((corpDet[j].valueForKey("Outstanding") as? String)!)!)!)
                    total = total + Double((corpDet[j].valueForKey("Outstanding") as? String)!)!
                    limit++
                    no.append(String(limit))
                    count += 1
                    
                }
                
            }
            let tot =  num.stringFromNumber(total)!
           // let qty = "Total Qty = " + String(qtyTot)
            value.append(tot)
            sum = sum + total
            //brn.append("")
            brn.append("")
            //no.append("")
            no.append("")
            no.append("No.")
            //customer.append("")
            customer.append("")
            customer.append("Customer")
           // billDate.append("")
             billDate.append("")
             billDate.append("Bill Date")
            //billNo.append("")
            billNo.append("")
            billNo.append("Bill No")
            days.append("Total=")
           // days.append("")
            days.append("Days")
            //value.append("")
            value.append("Outstanding(₹)")
            i = count
            
        }
       // no.removeAtIndex(no.count - 1)
        no.removeAtIndex(no.count - 1)
      //  brn.removeAtIndex(brn.count - 1)
       // customer.removeAtIndex(customer.count - 1)
        customer.removeAtIndex(customer.count - 1)
        //billDate.removeAtIndex(billDate.count - 1)
        billDate.removeAtIndex(billDate.count - 1)
       // billNo.removeAtIndex(billNo.count - 1)
        billNo.removeAtIndex(billNo.count - 1)
       // days.removeAtIndex(days.count - 1)
        days.removeAtIndex(days.count - 1)
        value.removeAtIndex(value.count - 1)
       // value.removeAtIndex(value.count - 1)
        print("Branch", brn)
        print("No",no)
        print("Customer", customer)
        print("Bill Date", billDate)
        print("Bill No", billNo)
        print("Days", days)
        print("Outstanding",value)
         print("Branch", brn.count)
        print("No",no.count)
         print("Customer", customer.count)
        print("Bill Date", billDate.count)
        print("Bill No", billNo.count)
        print("Days", days.count)
        print("Outstanding",value.count)*/
        
        //self.outstandingTable.tableFooterView = UIView.init(frame: CGRectZero)
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
        outstandingText.text = "Total outstanding for " + customerName + " is "
        amountText.text = "₹ " + num.stringFromNumber(total)!
        //        totalText.text = String(total)
        //
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphCorporateOutDet"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Customer"
            dest.yaxisName = "Outstanding"
            dest.cusprodText = "Customer"
            
                dest.cusProdValue = customerName
            
            dest.from = ""
            dest.to = ""
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("customer") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("outstanding") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
            
        }*/
        if segue.identifier == "toPieCorporateSales"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = ""
            dest.toDateStr = ""
            dest.to = ""
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("customer") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("outstanding") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
        }

        if segue.identifier == "toCorporateOutSumInd" {
            let dest = segue.destinationViewController as! CorporateOutSummInd
            dest.outstandingInd = outstandingIndArray
        }
    }
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = outstandingTable.dequeueReusableCellWithIdentifier("CorporateOutDetCell", forIndexPath: indexPath) as! CorporateOutDetCell
        //        cell.no.text = no[indexPath.row]
        //        cell.product.text = prName[indexPath.row]
        //        cell.quantity.text = quantity[indexPath.row]
        //       // cell.amount.text = value[indexPath.row]
        //        if no[indexPath.row] == "No." {
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        //        }
        //        else {
        //
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor.whiteColor()
        //        }
        
        if no[indexPath.row] == "No." {
            cell.branchName.text = brn[indexPath.row]
            cell.branchName.backgroundColor = UIColor.whiteColor()
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            longestWordRange = (customer[indexPath.row] as NSString).rangeOfString(customer[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: customer[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.customer.attributedText = attributedString

           /* longestWordRange = (billDate[indexPath.row] as NSString).rangeOfString(billDate[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: billDate[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.billDate.attributedText = attributedString
            longestWordRange = (billNo[indexPath.row] as NSString).rangeOfString(billNo[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: billNo[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.billNo.attributedText = attributedString

            longestWordRange = (days[indexPath.row] as NSString).rangeOfString(days[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: days[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.days.attributedText = attributedString*/
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.outstanding.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.branchName.text = ""
            cell.no.text = no[indexPath.row]
            cell.customer.text = customer[indexPath.row]
           /* cell.billDate.text = billDate[indexPath.row]
            cell.billNo.text = billNo[indexPath.row]
            cell.days.text = days[indexPath.row]*/
            cell.outstanding.text = value[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
//        if no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "" {
//            let longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
//            
//            let attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
//            
//            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
//            cell.product.attributedText = attributedString
//        }
       /* if days[indexPath.row].rangeOfString("Total") != nil {
            var longestWordRange = (days[indexPath.row] as NSString).rangeOfString(days[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: days[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.days.attributedText = attributedString
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.outstanding.attributedText = attributedString
            
        }*/
        
//        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "") || (quantity[indexPath.row].rangeOfString("Total") != nil){
//            cell.selectionStyle = UITableViewCellSelectionStyle.None
//        }
//        else {
//            cell.selectionStyle = UITableViewCellSelectionStyle.Gray
//        }
        
        
        
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       custCode = corpDet[indexPath.row - 1].valueForKey("custcode") as! String
        
        outstandingIndArray.removeAllObjects()
        getCorporateOutSumInd()
    }
    
    
    
    func getCorporateOutSumInd()
        
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_OUTSTANDING_DETAIL_CUSTOMERWISE?COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.outstandingIndArray)
            if self.outstandingInd.count == 0 {
                 dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                 });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutSumInd", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "CUSTOMER" || elementName == "BillDate" || elementName == "BillNo" || elementName == "Outstanding" || elementName == "Days"){
                noData = false
                elementValue = String()
                element = elementName
                outstandingInd[elementName] = ""
            }
        }
    }
            func parser(parser: NSXMLParser, foundCharacters string: String) {
                if elementValue != nil{
                    if (element == "CUSTOMER" || element == "BillDate" || element == "BillNo" || element == "Outstanding" || element == "Days"){
                        if (string.rangeOfString("\n") == nil){
                            outstandingInd[element!] = string
                        }
                        
                    }
                }
            }
            func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
                if elementName == "Days" {
                    // count = 0
                    outstandingIndArray.addObject(outstandingInd)
                   
                }
                
                
            }
            


       @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        performSegueWithIdentifier("toPieCorporateSales", sender: self)
    }
    
    
    
}
