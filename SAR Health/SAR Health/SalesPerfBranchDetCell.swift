//
//  SalesPerfBranchDetCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 29/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchDetCell: UITableViewCell {

    @IBOutlet var no: UILabel!
   
    @IBOutlet var branch: UILabel!
    @IBOutlet var manfact: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
}
