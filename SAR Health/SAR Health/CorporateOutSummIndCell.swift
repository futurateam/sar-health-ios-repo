//
//  CorporateOutSummIndCell.swift
//  SAR Health
//
//  Created by Anargha K on 17/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSummIndCell: UITableViewCell {

    @IBOutlet weak var billDate: UILabel!
    @IBOutlet weak var billNo: UILabel!
    //@IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var days: UILabel!
    @IBOutlet weak var outstanding: UILabel!
    @IBOutlet var category: UILabel!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
