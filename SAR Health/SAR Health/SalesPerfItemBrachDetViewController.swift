
//
//  SalesPerfItemBrachDetViewController.swift
//  SAR Health
//
//  Created by Anargha K on 23/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfItemBrachDetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {

    @IBOutlet var salesItemTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var salesText: UILabel!
    @IBOutlet var amtText: UILabel!
    @IBOutlet var amtTot: UILabel!
    @IBOutlet var qtyTot: UILabel!
    
    var sales = [Double](), custName = [String](), custCode = "", custSel = ""
    var branchName = "", productName = "", count = 0, count_1 = 0, limit = 0, i = 0 , j = 0 , sum : Double = 0, branch = String()
    var fromDate = ""
    var toDate = "", branchCode = "", prodCode = "", flag = true
    var total : Double = 0
    var salesItem : NSMutableArray = [], no = ["No"] , customer = ["Customer"] , quantity = ["Qty"] , amount = ["Amount (₹)"], brn = [String](), qty = 0
    var num = NSNumberFormatter()
    var elementValue: String?
    var element : String?
    var noData : Bool = false, salesItemInd = [String:String](), salesItemIndArray : NSMutableArray = [], salesDet : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        for (var i = 0 ; i < salesItem.count ; i++) {
            no.append(String(i + 1))
            customer.append((salesItem[i].valueForKey("CUST_NAME") as? String)!)
            quantity.append((salesItem[i].valueForKey("QTY") as? String)!)
            amount.append((salesItem[i].valueForKey("VALUE") as? String)!)
            total = total + Double((salesItem[i].valueForKey("VALUE") as? String)!)!
            qty = qty + Int((salesItem[i].valueForKey("QTY") as? String)!)!
        }
        qtyTot.text = String(qty)
        amtTot.text = num.stringFromNumber(total)!
           /* for ( i = 0; i < salesItem.count ; i += 1){
                total = 0
                print("i: ",i)
                print("Count", count)
                count = i
                limit = 0
                //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
                
                
                let custName = salesItem[i].valueForKey("CUST_NAME") as? String
                //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
                //            prName.append("Product")
                if (i == 0 ){
                    brn.append(custName!)
                    for ( j = i ; j < salesItem.count ; j += 1){
                        
                        if salesItem[j].valueForKey("CUST_NAME") as? String == custName{
                            brn.append("")
                            customer.append((salesItem[j].valueForKey("CUST_NAME") as? String)!)
                            
                            
                            quantity.append((salesItem[j].valueForKey("CUST_NAME") as? String)!)
                            amount.append(num.stringFromNumber(Double((salesItem[j].valueForKey("VALUE") as? String)!)!)!)
                            total = total + Double((salesItem[j].valueForKey("VALUE") as? String)!)!
                            limit++
                            no.append(String(limit))
                            //count += 1
                            
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                    // let qty = "Total Qty = " + String(qtyTot)
                    amount.append(tot)
                    sum =  total
                    //brn.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    customer.append("")
                    customer.append("Customer Name")
                    // billDate.append("")
                    
                    //billNo.append("")
                    quantity.append("Total =")
                    // days.append("")
                    quantity.append("Quantity")
                    //value.append("")
                    amount.append("Amount (₹)")
                    // i = count
                    
                }else {
                    if brn.contains(custName!){
                        
                    }
                    else {
                        brn.append(custName!)
                        for ( j = 0 ; j < salesItem.count ; j += 1){
                            
                            if salesItem[j].valueForKey("CUST_NAME") as? String == custName{
                                brn.append("")
                                customer.append((salesItem[j].valueForKey("CUST_NAME") as? String)!)
                                
                                
                                quantity.append((salesItem[j].valueForKey("QTY") as? String)!)
                                amount.append(num.stringFromNumber(Double((salesItem[j].valueForKey("VALUE") as? String)!)!)!)
                                total = total + Double((salesItem[j].valueForKey("VALUE") as? String)!)!
                                limit++
                                no.append(String(limit))
                                // count += 1
                                
                            }
                            
                        }
                        let tot =  num.stringFromNumber(total)!
                        // let qty = "Total Qty = " + String(qtyTot)
                        amount.append(tot)
                        sum = sum + total
                        
                        //brn.append("")
                        brn.append("")
                        //no.append("")
                        no.append("")
                        no.append("No")
                        //customer.append("")
                        customer.append("")
                        customer.append("Customer Name")
                        // billDate.append("")
                        
                        //billNo.append("")
                        quantity.append("Total =")
                        // days.append("")
                        quantity.append("Quantity")
                        //value.append("")
                        amount.append("Amount (₹)")
                        //i = count
                        
                        
                    }
                }
                
                //                brn.append("")
                //                no.append(String(limit))
                //                customer.append((salesItem[i].valueForKey("CUST_NAME") as? String)!)
                //
                //                quantity.append((salesItem[i].valueForKey("QTY") as? String)!)
                //
                //                amount.append(num.stringFromNumber(Double((salesItem[i].valueForKey("VALUE") as? String)!)!)!)
                //                total = Double((salesItem[i].valueForKey("VALUE") as? String)!)!
                
            }
            /*for(i = 0 ; i < salesItem.count ; i++){
            limit = 0
            branch = (salesItem[i].valueForKey("Branch") as! String)
            print("Branch", branch)
            
            print("Branch Array", brn)
            
            
            
            for (j = i  ; j < salesItem.count ; j++){
            if salesItem[j].valueForKey("Branch") as? String == branch{
            brn.append("")
            customer.append((salesItem[j].valueForKey("CUST_NAME") as? String)!)
            
            
            quantity.append((salesItem[j].valueForKey("QTY") as? String)!)
            amount.append(num.stringFromNumber(Double((salesItem[j].valueForKey("VALUE") as? String)!)!)!)
            total = total + Double((salesItem[j].valueForKey("VALUE") as? String)!)!
            limit++
            no.append(String(limit))
            //count += 1
            
            }
            
            
            
            }
            let tot =  num.stringFromNumber(total)!
            // let qty = "Total Qty = " + String(qtyTot)
            amount.append(tot)
            sum = sum + total
            //brn.append("")
            brn.append("")
            //no.append("")
            no.append("")
            no.append("No")
            //customer.append("")
            customer.append("")
            customer.append("Customer Name")
            // billDate.append("")
            
            //billNo.append("")
            quantity.append("Total =")
            // days.append("")
            quantity.append("Quantity")
            //value.append("")
            amount.append("Amount (₹)")
            
            
            
            
            
            
            
            
            }*/
            // no.removeAtIndex(no.count - 1)
            no.removeAtIndex(no.count - 1)
            //  brn.removeAtIndex(brn.count - 1)
            // customer.removeAtIndex(customer.count - 1)
            customer.removeAtIndex(customer.count - 1)
            //billDate.removeAtIndex(billDate.count - 1)
            // billNo.removeAtIndex(billNo.count - 1)
            
            // days.removeAtIndex(days.count - 1)
            quantity.removeAtIndex(quantity.count - 1)
            amount.removeAtIndex(amount.count - 1)
            // value.removeAtIndex(value.count - 1)
        */
            print("Branch", brn)
            print("No",no)
            print("Customer", customer)
            
            print("Quantity", quantity)
            print("Amount", amount)
            print("Branch", brn.count)
            print("No",no.count)
            print("Customer", customer.count)
            print("Quantity", quantity.count)
            print("Amount", amount.count)
            
            
          //  self.salesItemTable.tableFooterView = UIView.init(frame: CGRectZero)
            //        for(var  i = 0; i < salesCust.count; i++){
            //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
            //        }
            //
            /*   outstandingText.text = "Total outstanding for " + customerName + " is "
            amountText.text = "₹ " + num.stringFromNumber(sum)!*/
            
        
        
        //        for(var i = 0; i < salesItem.count; i += 1){
        //            total = total + Double((salesItem[i].valueForKey("VALUE") as? String)!)!
        //        }
        
        //totalText.text = num.stringFromNumber(sum)
        salesText.text = "Total sales in " + branchName + " from " + fromDate + " to " + toDate + " is "
        amtText.text = "₹ " + num.stringFromNumber(total)!
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphSalesPerfItemBranchDet" {
            sales.removeAll()
            custName.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Customer Name"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Product:"
            dest.cusProdValue = productName
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Item)"
            for(var  i = 0; i < salesItem.count; i += 1){
                sales.append(Double((salesItem[i].valueForKey("VALUE") as? String)!)!)
                custName.append((salesItem[i].valueForKey("CUST_NAME") as? String)!)
            }
            dest.chartData = sales
            dest.chartLegend = custName
            
        }*/
        if segue.identifier == "toPieSalesPerfItemDet" {
            sales.removeAll()
            custName.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Sales Performance (Product)"
            for(var  i = 0; i < salesItem.count; i += 1){
                sales.append(Double((salesItem[i].valueForKey("VALUE") as? String)!)!)
                custName.append((salesItem[i].valueForKey("CUST_NAME") as? String)!)
            }
            dest.chartData = sales
            dest.chartLegend = custName
        }
        if segue.identifier == "toSalesBranchDetInd" {
            sales.removeAll()
            custName.removeAll()
            let dest = segue.destinationViewController as! SalesPerfItemInd
            dest.salesDet = salesItemIndArray
            dest.custName = custSel
            
        }
        
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    
    /*    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return salesItem.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = salesItemTable.dequeueReusableCellWithIdentifier("SalePerfCell", forIndexPath: indexPath) as? SalesPerfmItemBranchCell
    cell?.no.text = String(indexPath.row + 1)
    cell?.custName.text = salesItem[indexPath.row].valueForKey("CUST_NAME") as? String
    cell?.quantity.text = salesItem[indexPath.row].valueForKey("QTY") as? String
    cell?.amount.text = num.stringFromNumber(Double((salesItem[indexPath.row].valueForKey("VALUE") as? String)!)!)
    return cell!
    
    }
    
    //    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let cell = salesItemTable.dequeueReusableCellWithIdentifier("HeaderCell") as? SalesPerfmItemBranchHeaderCell
    //        return cell
    //    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if Reachability.isConnectedToNetwork(){
    salesItemIndArray.removeAllObjects()
    prodCode = salesItem[indexPath.row].valueForKey("PR_CAT_ID") as! String
    custCode = salesItem[indexPath.row].valueForKey("SO_CUST_CODE") as! String
    //getSalesItemInd()
    }
    else{
    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
    }
    
    
    
    //  performSegueWithIdentifier("toSalesPerfItemInd", sender: self)
    }*/
   func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesItemTable.dequeueReusableCellWithIdentifier("SalePerfBranchDetCell", forIndexPath: indexPath) as! SalesPerfItemBranchDetCell
       // cell.category.text = brn[indexPath.row]
        if no[indexPath.row] == "No" {
            //cell.category.backgroundColor = UIColor.whiteColor()
            //cell.amount.textAlignment = NSTextAlignment.Center
           // cell.quantity.textAlignment = NSTextAlignment.Center
            
            cell.no.attributedText = headFont(no[indexPath.row])
            cell.custName.attributedText = headFont(customer[indexPath.row])
            cell.quantity.attributedText = headFont(quantity[indexPath.row])
            cell.amount.attributedText = headFont(amount[indexPath.row])
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            
            cell.no.text = no[indexPath.row]
            //cell.amount.textAlignment = NSTextAlignment.Center
            //cell.quantity.textAlignment = NSTextAlignment.Center
            cell.custName.text = customer[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = amount[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        if quantity[indexPath.row] == "Total =" {
            //cell.quantity.textAlignment = NSTextAlignment.Right
            //cell.amount.textAlignment = NSTextAlignment.Left
            var longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.quantity.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.amount.attributedText = attributedString
            
        }
        
        
        
        
        
        return cell
    }
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && amount[indexPath.row] == "" && brn[indexPath.row] != "") || (quantity[indexPath.row] == "Total ="){
        }
        else {
            salesItemIndArray.removeAllObjects()
            for (var i = 0 ; i < salesItem.count ; i++){
                if (customer[indexPath.row] == salesItem[i].valueForKey("CUST_NAME") as! String) {
                    
                    branchCode = salesItem[i].valueForKey("Branch_id") as! String
                    prodCode = salesItem[i].valueForKey("PR_CAT_ID") as! String
                    custCode = salesItem[i].valueForKey("SO_CUST_CODE") as! String
                    
                }
            }
            custSel = customer[indexPath.row]
            getSalesItemInd()
            //performSegueWithIdentifier("toSalesPerfCustInd", sender: self)
        }
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       
            return 44.0;
    }
    
    func getSalesItemInd()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let scriptUrl1 = Constants().API + "MOBAPP_PRODUCT_SALE_DETAIL_CUSTOMERWISE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode + "&CUSTOMER=" + custCode +  "&FRMDATE="  + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Sales Ind", self.salesItemIndArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesBranchDetInd", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "INV_NO" || elementName == "INV_DATE" || elementName == "CUST_NAME" || elementName == "Batch" || elementName == "Qty" || elementName == "VALUE"){
                noData = false
                elementValue = String()
                element = elementName
                salesItemInd[elementName] = ""
            }
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "INV_NO" || element == "INV_DATE" || element == "CUST_NAME" || element == "Batch" || element == "Qty" || element == "VALUE"){
                if (string.rangeOfString("\n") == nil){
                    
                    if element  == "INV_DATE"{
                        if count_1 == 0{
                            salesItemInd[element!] = string
                            count_1 += 1
                        }
                    }
                    else {
                        salesItemInd[element!] = string
                    }
                    
                }
                
            }
            
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "VALUE" {
            count_1 = 0
            salesItemIndArray.addObject(salesItemInd)
            
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    
    
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        //performSegueWithIdentifier("toGraphSalesPerfItemBranchDet", sender: self)
        performSegueWithIdentifier("toPieSalesPerfItemDet", sender: self)
    }
}
