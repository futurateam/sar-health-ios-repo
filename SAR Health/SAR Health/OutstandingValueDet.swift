//
//  OutstandingValueDet.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class OutstandingValueDet: UIViewController {
    

    @IBOutlet var valueTable: UITableView!
    @IBOutlet var valueText: UILabel!
    @IBOutlet var valueAmount: UILabel!
    @IBOutlet var totalText: UILabel!
    
    
    
    var total : Double = 0, custCode = "", custmerName = "", branchName = "", amt = [Double](), sum :Double = 0
    var outstandingValue : NSMutableArray = [], num = NSNumberFormatter(), i = 0 , j = 0, count = 0 , limit = 0, brn = [String]()
    var no = ["No"], billNo = ["Bill No"], billDate = ["Bill Date"] ,customer = ["Customer"], amount = ["Outstanding(₹)"], days = ["Days"], customer1 = [String](), totalData = [Double](), customerLegend = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        for ( i = 0; i < outstandingValue.count ; i += 1){
            total = 0
            print("i: ",i)
            print("Count", count)
            count = i
            limit = 0
            //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
            
            
            let customer = outstandingValue[i].valueForKey("CUSTOMER") as? String
            //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
            //            prName.append("Product")
            if (i == 0 ){
                brn.append(customer!)
                //branchAllArray.append(branch!)
                for ( j = i ; j < outstandingValue.count ; j += 1){
                    
                    if outstandingValue[j].valueForKey("CUSTOMER") as? String == customer{
                        //branchAllArray.append(branch!)
                        brn.append("")
                       billNo.append((outstandingValue[j].valueForKey("BillNo") as? String)!)
                        billDate.append((outstandingValue[j].valueForKey("BillDate") as? String)!)
                        amount.append(num.stringFromNumber(Double((outstandingValue[j].valueForKey("Outstanding") as? String)!)!)!)
                        total = total + Double((outstandingValue[j].valueForKey("Outstanding") as? String)!)!
                        days.append((outstandingValue[j].valueForKey("Days") as? String)!)
                        limit++
                        no.append(String(limit))
                        //count += 1
                        
                    }
                    
                }
                let tot =  num.stringFromNumber(total)!
                // let qty = "Total Qty = " + String(qtyTot)
                days.append(tot)
                totalData.append(total)
                sum =  total
                //brn.append("")
                //branchAllArray.append("")
                brn.append("")
                //no.append("")
                no.append("")
                no.append("No")
                //customer.append("")
                billNo.append("")
                billNo.append("Bill No")
                // billDate.append("")
                billDate.append("")
                billDate.append("Bill Date")
                //billNo.append("")
                amount.append("Total =")
                // days.append("")
                amount.append("Outstanding (₹)")
                //value.append("")
                days.append("Days")
                // i = count
                
            }else {
                if brn.contains(customer!){
                    
                }
                else {
                    brn.append(customer!)
                   // branchAllArray.append(branch!)
                    for ( j = 0 ; j < outstandingValue.count ; j += 1){
                        
                         if outstandingValue[j].valueForKey("CUSTOMER") as? String == customer{
                            brn.append("")
                            billNo.append((outstandingValue[j].valueForKey("BillNo") as? String)!)
                            billDate.append((outstandingValue[j].valueForKey("BillDate") as? String)!)
                            amount.append(num.stringFromNumber(Double((outstandingValue[j].valueForKey("Outstanding") as? String)!)!)!)
                            total = total + Double((outstandingValue[j].valueForKey("Outstanding") as? String)!)!
                            days.append((outstandingValue[j].valueForKey("Days") as? String)!)
                            limit++
                            no.append(String(limit))
                            //count += 1
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                    // let qty = "Total Qty = " + String(qtyTot)
                    days.append(tot)
                    totalData.append(total)
                    sum = sum + total
                    
                    //brn.append("")
                    //branchAllArray.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    billNo.append("")
                    billNo.append("Bill No")
                    // billDate.append("")
                    billDate.append("")
                    billDate.append("Bill Date")
                    //billNo.append("")
                    amount.append("Total =")
                    // days.append("")
                    amount.append("Outstanding (₹)")
                    //value.append("")
                    days.append("Days")
                    //i = count
                    
                    
                }
            }
            
            
        }
        // no.removeAtIndex(no.count - 1)
        no.removeAtIndex(no.count - 1)
        //  brn.removeAtIndex(brn.count - 1)
        // customer.removeAtIndex(customer.count - 1)
        billNo.removeAtIndex(billNo.count - 1)
        //billDate.removeAtIndex(billDate.count - 1)
        // billNo.removeAtIndex(billNo.count - 1)
        
        // days.removeAtIndex(days.count - 1)
        billDate.removeAtIndex(billDate.count - 1)
        amount.removeAtIndex(amount.count - 1)
        days.removeAtIndex(days.count - 1)
        // value.removeAtIndex(value.count - 1)
        print("Branch", brn)
       // print("Branch All", branchAllArray)
        print("No",no)
        print("Bill No", billNo)
        print("Bill Date", billDate)
        print("Outstanding", amount)
        print("Days", days)
        print("Branch", brn.count)
        // print("Branch All", branchAllArray)
        print("No",no.count)
        print("Bill No", billNo.count)
        print("Bill Date", billDate.count)
        print("Outstanding", amount.count)
        print("Days", days.count)
        
        
        self.valueTable.tableFooterView = UIView.init(frame: CGRectZero)
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
        /*   outstandingText.text = "Total outstanding for " + customerName + " is "
        amountText.text = "₹ " + num.stringFromNumber(sum)!*/
        
        
        
        //        for(var i = 0; i < salesItem.count; i += 1){
        //            total = total + Double((salesItem[i].valueForKey("VALUE") as? String)!)!
        //        }
        
        //totalText.text = num.stringFromNumber(sum)     
        if custCode == "0" {
            valueText.text = "Total outstanding for All customers in " + branchName + " branch is"
        }
        else {
            valueText.text = "Total oustanding for " + custmerName + " in " + branchName + " branch is"
        }
        valueAmount.text = "₹ " + num.stringFromNumber(sum)!
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        /*if segue.identifier == "toGraphOutstandingDet" {
            amt.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            for (var i = 0; i < outstandingValue.count ; i += 1) {
                amt.append(Double((outstandingValue[i].valueForKey("Outstanding") as? String)!)!)
            }
            for (var  i = 0 ; i < brn.count ; i++){
                if brn[i] != "" {
                    customerLegend.append(brn[i])
                }
            }
                        dest.chartData = amt
            dest.xaxisName = "Customer"
            dest.yaxisName = "Amount"
            
            
            dest.cusprodText = "Customer"
            if custmerName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = custmerName
            }
            for (var i = 0 ;i < outstandingValue.count; i += 1 ){
                customer1.append((outstandingValue[i].valueForKey("CUSTOMER") as? String)!)
            }
            dest.chartLegend = customer1
            dest.titleTab = "Outstanding Report"
        }*/
        if segue.identifier == "toPieOutstandingDet" {
            customerLegend.removeAll()
            for (var  i = 0 ; i < brn.count ; i++){
                if brn[i] != "" {
                    customerLegend.append(brn[i])
                }
            }
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = ""
            dest.toDateStr = ""
            dest.to = ""
            dest.titleTab = "Outstanding Report"
            dest.chartLegend = customerLegend
            dest.chartData = totalData
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = valueTable.dequeueReusableCellWithIdentifier("OutstandingDetCell", forIndexPath: indexPath) as! OutstandingValueDetCell
        cell.category.text = brn[indexPath.row]
        if no[indexPath.row] == "No" {
            cell.category.backgroundColor = UIColor.whiteColor()
            cell.days.textAlignment = NSTextAlignment.Center
            cell.outstanding.textAlignment = NSTextAlignment.Center
            cell.no.attributedText = headFont(no[indexPath.row])
            cell.billNo.attributedText = headFont(billNo[indexPath.row])
            cell.billDate.attributedText = headFont(billDate[indexPath.row])
            cell.outstanding.attributedText = headFont(amount[indexPath.row])
            cell.days.attributedText = headFont(days[indexPath.row])
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            
            cell.no.text = no[indexPath.row]
            cell.days.textAlignment = NSTextAlignment.Center
            cell.outstanding.textAlignment = NSTextAlignment.Center
            cell.billNo.text = billNo[indexPath.row]
            cell.billDate.text = billDate[indexPath.row]
            cell.outstanding.text = amount[indexPath.row]
            cell.days.text = days[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        if amount[indexPath.row] == "Total =" {
            cell.outstanding.textAlignment = NSTextAlignment.Right
            cell.days.textAlignment = NSTextAlignment.Left
            var longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.outstanding.attributedText = attributedString
            longestWordRange = (days[indexPath.row] as NSString).rangeOfString(days[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: days[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.days.attributedText = attributedString
            
        }
        
        
        
        
        
        return cell
    }
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    

  
    @IBAction func viewGraphPressed(sender: AnyObject) {
        //performSegueWithIdentifier("toGraphOutstandingDet", sender: self)
        performSegueWithIdentifier("toPieOutstandingDet", sender: self)
    }
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
