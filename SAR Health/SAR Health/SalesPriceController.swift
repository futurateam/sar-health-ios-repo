//
//  SalesPriceController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPriceController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tabTitle: UILabel!
    @IBOutlet var salesPriceTable: UITableView!
  
    @IBOutlet var salesText: UILabel!
    
    var num = NSNumberFormatter()
    var productName = "", cellNo = 0
    var no = ["No"], branch = ["Branch"], price = [String]()
    var salesPrice : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        self.salesPriceTable.tableFooterView = UIView.init(frame: CGRectZero)
        if cellNo == 1{
            price.append("Sales Price")
            tabTitle.text = "Sales Price"
        for (var i = 0; i < salesPrice.count; i += 1){
            no.append(String(i+1))
            branch.append((salesPrice[i].valueForKey("brn_name") as? String)!)
            price.append(num.stringFromNumber(Float((salesPrice[i].valueForKey("PRODUCT_SELL_RATE") as? String)!)!)!)
            
        }
        salesText.text = "Sales Price of " + productName
        }
        else if cellNo == 2 {
            price.append("Purchase Price")
            tabTitle.text = "Purchase Price"
            for (var i = 0; i < salesPrice.count; i += 1){
                no.append(String(i+1))
                branch.append((salesPrice[i].valueForKey("brn_name") as? String)!)
                price.append(num.stringFromNumber(Float((salesPrice[i].valueForKey("PRODUCT_BUY_RATE") as? String)!)!)!)
                
            }
            salesText.text = "Purchase Price of " + productName

        }
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
       
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
                // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesPriceTable.dequeueReusableCellWithIdentifier("SalesPriceAll", forIndexPath: indexPath) as! SalesPriceAllCell
        
        
                if(no[indexPath.row] == "No"){
            
            
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.branch.attributedText = attributedString
            longestWordRange = (price[indexPath.row] as NSString).rangeOfString(price[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: price[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.price.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.no.text = no[indexPath.row]
            cell.branch.text = branch[indexPath.row]
                    cell.price.text = price[indexPath.row]

            cell.backgroundColor = UIColor.whiteColor()
        }
        return cell
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func checkAnotherPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
