//
//  ProductExpiryAllCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 18/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductExpiryAllCell: UITableViewCell {
    
    @IBOutlet var no: UILabel!
    @IBOutlet var branch: UILabel!
    @IBOutlet var batchNo: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var date: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
