//
//  BranchTargetController.swift
//  SAR Health
//
//  Created by Anargha K on 22/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class BranchTargetController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
    @IBOutlet var branchTable: UITableView!
    @IBOutlet var branchName: UILabel!
    @IBOutlet var branchDetTable: UITableView!
    @IBOutlet var branchDetAllTable: UITableView!
    
    var elementValue: String?
    var element : String?
    var btag = 0, branchCode = "", branch = ["All"], branchCount : Int = 0, branchtgt = [String : String](), branchtgtArray : NSMutableArray = [], branchDet = [String :String](), branchDetArray : NSMutableArray = []
    var branchList : NSMutableArray = [], lowStockArray : NSMutableArray = [], branchDetAll = [String : String](), branchDetAllDet : NSMutableArray = []
    var lowStock = [String : String](), finyear = [String:String](), finyearArray : NSMutableArray = []
    var noData : Bool = false
    var no = ["No"], from = ["From Period"], to = ["To Period"], target = ["Target (₹)"], sales = ["Sales (₹)"], acheivement = ["Acheivement (₹)"], branchArray = ["Branch"];
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.branchDetTable.hidden = true
        branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
        
        if Reachability.isConnectedToNetwork(){
            getFinyear()
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        for (var i = 0 ;i < branchList.count ; i += 1) {
                       branch.append((branchList[i].valueForKey("BRN_NAME") as? String)!)
        }
        print(branchList)
        branchTable.hidden = true
          self.branchDetTable.tableFooterView = UIView.init(frame: CGRectZero)
        self.branchDetAllTable.tableFooterView = UIView.init(frame: CGRectZero)
        self.branchDetAllTable.hidden = true
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
            }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == branchTable {
             return branch.count;
        }
        else if tableView == branchDetAllTable {
            return branchArray.count
        }
        else {
            return from.count
        }
       
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      
        if tableView == branchDetTable {
            let cell = branchDetTable.dequeueReusableCellWithIdentifier("BranchDetCell", forIndexPath: indexPath) as! BranchTargetTableCell
            
            
            if indexPath.row == 0 {
                var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
                
                var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.no.attributedText = attributedString
                longestWordRange = (from[indexPath.row] as NSString).rangeOfString(from[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: from[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.fromPeriod.attributedText = attributedString
                
                longestWordRange = (to[indexPath.row] as NSString).rangeOfString(to[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: to[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.toPeriod.attributedText = attributedString
                
                longestWordRange = (sales[indexPath.row] as NSString).rangeOfString(sales[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: sales[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.sales.attributedText = attributedString
                longestWordRange = (target[indexPath.row] as NSString).rangeOfString(target[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: target[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.targetLabel.attributedText = attributedString
                
                longestWordRange = (acheivement[indexPath.row] as NSString).rangeOfString(acheivement[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: acheivement[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.acheivement.attributedText = attributedString
                
                cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
                
                
            }
            else {
                cell.no.text = no[indexPath.row]
                cell.fromPeriod.text = from[indexPath.row]
                cell.toPeriod.text = to[indexPath.row]
                cell.sales.text = sales[indexPath.row]
                cell.targetLabel.text = target[indexPath.row]
                cell.acheivement.text = acheivement[indexPath.row]
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            return cell

        }
        else if tableView == branchDetAllTable {
            let cell = branchDetAllTable.dequeueReusableCellWithIdentifier("BranchDetAllCell", forIndexPath: indexPath) as! BranchTargetAllCell
            
            
            if indexPath.row == 0 {
                var longestWordRange = (branchArray[indexPath.row] as NSString).rangeOfString(branchArray[indexPath.row])
                
                var attributedString = NSMutableAttributedString(string: branchArray[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.no.attributedText = attributedString
              
                
                
                longestWordRange = (sales[indexPath.row] as NSString).rangeOfString(sales[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: sales[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.sales.attributedText = attributedString
                longestWordRange = (target[indexPath.row] as NSString).rangeOfString(target[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: target[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.targetLabel.attributedText = attributedString
                
                longestWordRange = (acheivement[indexPath.row] as NSString).rangeOfString(acheivement[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: acheivement[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
                cell.acheivement.attributedText = attributedString
                
                cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
                
                
            }
            else {
                cell.no.text = branchArray[indexPath.row]
                cell.sales.text = sales[indexPath.row]
                cell.targetLabel.text = target[indexPath.row]
                cell.acheivement.text = acheivement[indexPath.row]
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            return cell
            

        }
        else {
            let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! BranchTargetBranchCell
            //print("Array",branchList)
            // print("count", branchList.count)
            //print("Indexpath", indexPath.row)
            //print (branch[indexPath.row])
            
            cell.branchName.text = branch[indexPath.row]
            return cell
           
        }
        
        
        
       
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        branchtgtArray.removeAllObjects()
        if tableView == branchTable {
            branchName.text = branch[indexPath.row]
            
            if branchName.text == "All" {
                branchCode = "0"
                getBranchTargetDet()
            }
            else {
                
                
                branchCode = (branchList[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
                getBranchTarget()
                
            }

        }
        else if tableView == branchDetAllTable{
            branchName.text = branchDetAllDet[indexPath.row - 1].valueForKey("BRN_NAME") as? String
            branchCode = branchDetAllDet[indexPath.row - 1].valueForKey("branchId") as! String
            getBranchTarget()
        }
        
                      branchTable.hidden = true
        
        
    }
    
    @IBAction func findPressed(sender: AnyObject) {
        lowStockArray.removeAllObjects()
        if branchName.text == "Branch"{
            Common().showAlert(Constants().APP_NAME, message: "Please Select Branch",viewController: self)
        }
            
        else if Reachability.isConnectedToNetwork(){
         
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        
        
        
        //            if branchLabel.text == "All"{
        //
        //
        //                //self.performSegueWithIdentifier("toProductAll", sender: self)
        //            }
        //            else{
        //                // getDataProductAll()
        //                if check == false{
        //
        //                    // print(productStock)
        //                //self.performSegueWithIdentifier("toProductBranch", sender: self)
        //               }
        //                else {
        //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
        //                }
        //            }
        //
        
        
    }
    
    
    @IBAction func branchButton(sender: AnyObject) {
        self.branchDetTable.hidden = true
        self.branchDetAllTable.hidden = true
        view.endEditing(true)
        if(btag == 0){
            self.branchTable.hidden = true
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                var frame = self.branchTable.frame
                frame.size.height = 0;
                self.branchTable.frame = frame
                }, completion:{finished in} )
            btag = 1;
        }
        else{
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                var frame = self.branchTable.frame;
                frame.size.height = 175;
                self.branchTable.frame = frame;
                }, completion: { finished in })
            self.branchTable.hidden = false
            
            btag = 0;
        }
        
        
    }
    
    func getBranchTarget()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=0&DIVISION=1&COMPANY=1&MANUFACTURE=3250&FRMDATE=2016-03-10&TODATE=2016-03-30"
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_TARGET?BRANCH=" + branchCode + "&DIVISION=1&COMPANY=1"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("Branch Summ", self.branchtgtArray)
            dispatch_async(dispatch_get_main_queue(), {
                if (self.noData == true) {
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    let alert = UIAlertController(title: "Alert", message: self.branchtgtArray[0].valueForKey("Column1") as? String , preferredStyle: UIAlertControllerStyle.Alert)
                  
                    self.presentViewController(alert, animated: true, completion: nil)
                    alert.addAction(UIAlertAction(title: "FIND", style: .Default, handler: { action in
                        switch action.style{
                        case .Default:
                            self.branchDetArray.removeAllObjects()
                            self.getBranchTargetDet()
                            
                        case .Cancel:
                            print("cancel")
                            
                        case .Destructive:
                            print("destructive")
                        }
                    }))
                      //  Common().showAlert(Constants().APP_NAME, message: self.branchtgtArray[0].valueForKey("Column1") as! String, viewController: self)
                    
                    
                }
            });
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            //            dispatch_async(dispatch_get_main_queue(), {
            //
            //                if (self.noData == true){
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            //
            //
            //                }
            //                else{
            //
            //                    HUD.hideUIBlockingIndicator()
            //                    self.performSegueWithIdentifier("toSalesPerfExecSumm", sender: self)
            //
            //
            //
            //                }
            //            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getBranchTargetDet()
    {
        branchDet.removeAll()
        branchDetArray.removeAllObjects()
        branchtgtArray.removeAllObjects()
        branchDetAllDet.removeAllObjects()
        no.removeAll()
        from.removeAll()
        to.removeAll()
        target.removeAll()
        sales.removeAll()
        acheivement.removeAll()
        no = ["No"]
        from = ["From Period"]
        to = ["To Period"]
        target = ["Target (₹)"]
        sales = ["Sales (₹)"]
        acheivement = ["Acheivement (₹)"]

        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=0&DIVISION=1&COMPANY=1&MANUFACTURE=3250&FRMDATE=2016-03-10&TODATE=2016-03-30"
          let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_TARGET_DETAIL?BRANCH=" + branchCode + "&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("Branch Summ", self.branchDetArray)
            print("Branch", self.branchDetAllDet)
            dispatch_async(dispatch_get_main_queue(), {
                if (self.noData == true) {
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    if self.branchName.text == "All"{
                        if self.branchDetAllDet.count == 0 {
                            HUD.hideUIBlockingIndicator()
                            Common().showAlert(Constants().APP_NAME, message: "No data", viewController: self)
                        }
                        else {
                            for (var i = 0; i < self.branchDetAllDet.count; i += 1){
                                self.branchArray.append((self.branchDetAllDet[i].valueForKey("BRN_NAME") as? String)!)
                                self.target.append((self.branchDetAllDet[i].valueForKey("target") as? String)!)
                                self.sales.append((self.branchDetAllDet[i].valueForKey("Sales") as? String)!)
                                self.acheivement.append((self.branchDetAllDet[i].valueForKey("Achievement") as? String)!)
                            }
                            self.branchDetTable.hidden = true
                            self.branchDetAllTable.hidden = false
                            self.branchDetAllTable.reloadData()

                        }
                                            }
                    else {
                        if self.branchDetArray.count == 0 {
                            HUD.hideUIBlockingIndicator()
                            Common().showAlert(Constants().APP_NAME, message: "No data", viewController: self)
                        }
                        else {
                            for (var i = 0; i < self.branchDetArray.count; i += 1){
                                self.no.append(String(i+1))
                                self.from.append((self.branchDetArray[i].valueForKey("frmPeriod") as? String)!)
                                self.to.append((self.branchDetArray[i].valueForKey("toPeriod") as? String)!)
                                self.target.append((self.branchDetArray[i].valueForKey("target") as? String)!)
                                self.sales.append((self.branchDetArray[i].valueForKey("sales") as? String)!)
                                self.acheivement.append((self.branchDetArray[i].valueForKey("Achievment") as? String)!)
                            }
                            self.branchDetTable.hidden = false
                            self.branchDetAllTable.hidden = true
                            self.branchDetTable.reloadData()
                        }
                       
                    }
                    

                    //  Common().showAlert(Constants().APP_NAME, message: self.branchtgtArray[0].valueForKey("Column1") as! String, viewController: self)
                    
                    
                }
            });
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            //            dispatch_async(dispatch_get_main_queue(), {
            //
            //                if (self.noData == true){
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            //
            //
            //                }
            //                else{
            //
            //                    HUD.hideUIBlockingIndicator()
            //                    self.performSegueWithIdentifier("toSalesPerfExecSumm", sender: self)
            //
            //
            //
            //                }
            //            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    

    
       func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
                       if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
                noData = false
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
            if (elementName == "frmPeriod" || elementName == "toPeriod" || elementName == "target" || elementName == "sales" || elementName == "Achievment") {
                noData = false
                elementValue = String()
                element = elementName
                branchDet[elementName] = ""
            }
            
            if (elementName == "branchId" || elementName == "BRN_NAME" || elementName == "target" || elementName == "Sales" || elementName == "Achievement"){
                elementValue = String()
                element = elementName
                branchDetAll[elementName] = ""
            }
            
            if(elementName == "Column1"){
                noData = false
                elementValue = String()
                element = elementName
                branchtgt[elementName] = ""
            }
            

            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "branch" || element == "product" || element == "stock"){
                if (string.rangeOfString("\n") == nil){
                    lowStock[element!] = string
                }
                
            }
            if (element == "FIN_YEAR_ID" || element == "FINYEAR") {
                if (string.rangeOfString("\n") == nil){
                    finyear[element!] = string
                }
                
            }
            if (element == "frmPeriod" || element == "toPeriod" || element == "target" || element == "sales" || element == "Achievment"){
                if string.rangeOfString("\n") == nil {
                    branchDet[element!] = string
                }
            }
            if (element == "branchId" || element == "BRN_NAME" || element == "target" || element == "Sales" || element == "Achievement"){
                if string.rangeOfString("\n") == nil {
                    branchDetAll[element!] = string
                }
            }
            
            if element == "Column1" {
                if string.rangeOfString("\n") == nil {
                    branchtgt[element!] = string
                }
            }
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "stock" {
            lowStockArray.addObject(lowStock)
            
        }
        if elementName == "FINYEAR" {
            finyearArray.addObject(finyear)
        }
        if elementName == "Column1" {
            branchtgtArray.addObject(branchtgt)
        }
        if elementName == "Achievment" {
            branchDetArray.addObject(branchDet)
           
        }
        if elementName == "Achievement" {
             branchDetAllDet.addObject(branchDetAll)
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

}
