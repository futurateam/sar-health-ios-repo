//
//  CorporateOutReportViewController.swift
//  SAR Health
//
//  Created by Anargha K on 17/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutReportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {

    @IBOutlet var valueTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var valueText: UILabel!
    @IBOutlet var valueAmount: UILabel!
    
    var elementValue: String?
    var element : String?
    var total : Double = 0, custCode = "", custmerName = "", branchName = "", amt = [Double]()
    var outstandingValue : NSMutableArray = [], num = NSNumberFormatter(), outstandingInd = [String : String](), outstandingIndArray : NSMutableArray = [], outstandingDet = [String : String](), outstandingDetArray : NSMutableArray = [], branchCode = "", branchArray : NSMutableArray = [], customerArray : NSMutableArray = []
    var no = ["No."], customer = ["Customer"], amount = ["Outstanding(₹)"], branch = ["Branch"], sales = ["Sales"], customerLegend = [String](), noData : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        for (var i = 0; i < outstandingValue.count; i += 1){
            no.append(String(i+1))
            branch.append((outstandingValue[i].valueForKey("branch") as? String)!)
            //customer.append((outstandingValue[i].valueForKey("customer") as? String)!)
            //sales.append((outstandingValue[i].valueForKey("sales") as? String)!)
            amount.append((outstandingValue[i].valueForKey("outstanding") as? String)!)
                      total = total + Double((outstandingValue[i].valueForKey("outstanding") as? String)!)!
        }
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        totalText.text = num.stringFromNumber(total)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
            valueText.text = "Total oustanding for " + custmerName + " is"
        
        valueAmount.text = "₹ " + num.stringFromNumber(total)!
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphCorportOutSum"{
            amt.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
         
            
            dest.chartData = amt
            dest.xaxisName = "Customer"
            dest.yaxisName = "Amount"
            
            
            dest.cusprodText = "Customer"
            if custmerName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = custmerName
            }
            for (var i = 0 ; i < outstandingValue.count ; i++) {
                customerLegend.append((outstandingValue[i].valueForKey("customer") as? String)!)
            }
            dest.chartLegend = customerLegend
            dest.titleTab = "Outstanding Report"
            
        }*/
        if segue.identifier == "toPieChartCorporateOut" {
            amt.removeAll()
            customerLegend.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = ""
            dest.toDateStr = ""
            dest.to = ""
            dest.titleTab = "Corporate Customers (Sales)"
            for (var i = 0 ; i < outstandingValue.count ; i++) {
                customerLegend.append((outstandingValue[i].valueForKey("branch") as? String)!)
            }
            for (var i = 1; i < amount.count ; i += 1) {
                amt.append(Double(amount[i])!)
            }
            dest.chartLegend = customerLegend
            dest.chartData = amt
        }
       
        if segue.identifier == "toCorporateOutDet" {
            let dest = segue.destinationViewController as! CorporateOutDetViewController
            dest.corpDet = outstandingDetArray
            dest.customerName = custmerName
            dest.customerArray = outstandingValue
        }

    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = valueTable.dequeueReusableCellWithIdentifier("CorporateOutCell", forIndexPath: indexPath) as! CorporateOutReportCell
       
        // cell.outstanding.text = amount[indexPath.row]
        if indexPath.row == 0 {
           
            cell.no.attributedText = headFont(no[indexPath.row])
            cell.branch.attributedText = headFont(branch[indexPath.row])
            //cell.sales.attributedText = headFont(sales[indexPath.row])
            //cell.customer.attributedText = headFont(sales[indexPath.row])
            cell.outstanding.attributedText = headFont(amount[indexPath.row])
           
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.no.text = no[indexPath.row]
            cell.branch.text = branch[indexPath.row]
            //cell.sales.text = sales[indexPath.row]
            //cell.customer.text = customer[indexPath.row]
            cell.outstanding.text = num.stringFromNumber(Double(amount[indexPath.row])!)
            
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
    }
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (no[indexPath.row] != "No."){
            //outstandingIndArray.removeAllObjects()
            outstandingDetArray.removeAllObjects()
           /* for (var i = 0 ; i < outstandingValue.count ; i++){
                if (customer[indexPath.row] == outstandingValue[i].valueForKey("customer") as! String) {
                    
                    custCode = outstandingValue[i].valueForKey("custcode") as! String
                    
                }
            }
            getCorporateOutSumInd()*/
            branchCode = outstandingValue[indexPath.row - 1].valueForKey("BRN_ID") as! String
            //custCode = outstandingValue[indexPath.row - 1].valueForKey("custcode") as! String
            getCorporateOutDet()

        }
    }
    
       func getCorporateOutDet()
        
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_OUTSTANDING_DETAIL?COMPANY=1&DIVISION=1&BRANCH=" + branchCode + "&CUSTOMER=" + custmerName
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.outstandingDetArray)
            if self.outstandingDetArray.count == 0 {
                dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "CUSTOMER" || elementName == "BillDate" || elementName == "BillNo" || elementName == "Outstanding" || elementName == "Days"){
                noData = false
                elementValue = String()
                element = elementName
                outstandingInd[elementName] = ""
            }
            if (elementName == "BRN_ID" || elementName == "branch" || elementName == "custcode" || elementName == "customer" || elementName == "sales" || elementName == "outstanding"){
                noData = false
                elementValue = String()
                element = elementName
                outstandingDet[elementName] = ""
            }
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "CUSTOMER" || element == "BillDate" || element == "BillNo" || element == "Outstanding" || element == "Days"){
                if (string.rangeOfString("\n") == nil){
                    outstandingInd[element!] = string
                }
                
            }
            if(element == "BRN_ID" || element == "branch" || element == "custcode" || element == "customer" || element == "sales" || element == "outstanding"){
                if (string.rangeOfString("\n") == nil){
                    outstandingDet[element!] = string
                }
            }
//            if (element == "custcode" || element == "customer" || element == "outstanding") {
//                if (string.rangeOfString("\n") == nil){
//                    if element == "customer"{
//                        if count == 0{
//                            outstandingSum[element!] = string
//                            
//                            count += 1
//                        }
//                    }
//                    else {
//                        outstandingSum[element!] = string
//                    }
//                    
//                    
//                }
//            }
            
            
            
        }
        
        //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
        //                if (string.rangeOfString("\n") == nil){
        //                    price[element!] = string
        //                }
        //            }
        //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
        //                if (string.rangeOfString("\n") == nil){
        //                    expiry[element!] = string
        //                }
        //            }
        
        
    }
    
    
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "Days" {
           // count = 0
            outstandingIndArray.addObject(outstandingInd)
                     }
        if elementName == "outstanding" {
            outstandingDetArray.addObject(outstandingDet)

        }
       
       
    }
    

    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func viewGraphPressed(sender: AnyObject) {
        
       // performSegueWithIdentifier("toGraphCorportOutSum", sender: self)
        performSegueWithIdentifier("toPieChartCorporateOut", sender: self)
    }

}
