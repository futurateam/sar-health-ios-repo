//
//  CorporateOutViewController.swift
//  SAR Health
//
//  Created by Anargha K on 16/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutViewController: UIViewController, NSXMLParserDelegate, UITableViewDataSource, UITableViewDelegate {

       @IBOutlet var customerLabel: UITextField!
    
    @IBOutlet var customerTable: UITableView!

    @IBOutlet weak var summerzdImage: UIImageView!
    
    @IBOutlet weak var detaildImage: UIImageView!
    
    @IBOutlet var summLabel: UILabel!
    
   
    @IBOutlet var detlabel: UILabel!
    
    
   
    var customer = [String : String]()
    var customerArray : NSMutableArray = []
    var salesItemBranch = [String : String](), outstandingSum = [String : String](), outstandingSummArray : NSMutableArray = [], outstandingDet = [String : String](), outstandingDetArray : NSMutableArray = [], branchArray : NSMutableArray = [], manfact : NSMutableArray = []
    //var elementProduct : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var custCode = ""
    var noData : Bool = false
   var count = 0
    var radiotag1 = 0
    var radiotag2 = 1
    var radioText = "summerized"
        var i = 0
    
    var flag : Bool = false
    var btag : NSInteger = 0
    var searchActive : Bool = false
    var filter = [String]()
    var itemArray = [String](), productFilter = [String]()
    var manfactfilter = [String]()
    var cellNo = 0
    var largeNumber = 36000
    //var branchList = [String]()
    //var itemlist = [String]()
    var itemList : NSMutableArray = []
    
    var num = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        customerTable.hidden = true
        filter.removeAll()
        if Reachability.isConnectedToNetwork(){
            getCustomerList()
        }
        else{
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
        }
        if cellNo == 4 {
            summLabel.text = "Outstanding"
            detlabel.text = "Sales"
        }
        else {
            summLabel.text = "Summerized"
            detlabel.text = "Detailed"
        }
        self.customerTable.tableFooterView = UIView.init(frame: CGRectZero)
        print("Number",num.stringFromNumber(largeNumber)!)
        
    }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toCorporateOutSumm" {
            let dest = segue.destinationViewController as! CorporateOutReportViewController
            dest.outstandingValue = outstandingSummArray
            dest.custmerName = customerLabel.text!
            dest.branchArray = branchArray
            print(customerArray)
            dest.customerArray = customerArray
            customerLabel.text = ""
        }
        if segue.identifier == "toCorporateOutDet" {
            let dest = segue.destinationViewController as! CorporateOutDetViewController
            dest.customerArray = customerArray
            dest.corpDet = outstandingDetArray
            dest.customerName = customerLabel.text!
        }
        if segue.identifier == "toOutstandingSales" {
            let dest = segue.destinationViewController as! CorporateCustomerSalesMain
            dest.cellNo = cellNo
            dest.outstandingCust = customerLabel.text!
            dest.branch = branchArray
            dest.manfactArray = manfact
           // dest.custCode = custCode
            
        }
       

        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
            if(searchActive){
                return filter.count
            }
            
                return itemArray.count;
            
        
        
       
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
            let cell = customerTable.dequeueReusableCellWithIdentifier("CustomerCell", forIndexPath: indexPath) as! CorporateCustomerTableViewCell
            
                
                if(searchActive){
                    cell.customerName.text = filter[indexPath.row]
                }
                else{
                    cell.customerName.text = itemArray[indexPath.row]
                }
            
            return cell;
            
        
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
            
           
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = itemArray[indexPath.row]
                }
        
            
            view.endEditing(true)
            
        
        
        
        
                                // HUD.hideUIBlockingIndicator()
        
        
        }
        
    
    
    
    //Mark: - Textfields Methods
    func textFieldDidBeginEditing(textField: UITextField) {
        
        for(var i = 0; i < customerArray.count; i += 1){
            self.itemArray.append((self.customerArray[i].valueForKey("CUST_COMPANY_GROUP") as? String)!)
        }
        print("Item Array", self.itemArray)
        
        flag = false
        searchActive = true
        textField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        textFieldDidChange(textField)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        customerTable.hidden = true;
        searchActive = false
    }
    
    
    func textFieldDidChange(textField:UITextField){
        if flag == false{
            flag = true
        }
        
        if textField.text?.characters.count >= 2{
           
                
                if flag == true{
                    
                    customerTable.hidden = false;
                    filter = itemArray.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    for (var i=0; i < self.filter.count; i += 1 ) {
                        for (var j=(i+1); j < self.filter.count; j += 1) {
                            if ((self.filter[i] == self.filter[j])) {
                                self.filter.removeAtIndex(j)
                                j -= 1;
                            }
                        }
                    }
                     print("Item Array", self.filter)
                    self.customerTable.reloadData()
                }
            
            
        }
        else {
            customerTable.hidden = true
        }
        
    }
    
    
       @IBAction func findPressed(sender: AnyObject) {
        customerArray.removeAllObjects()
        outstandingSummArray.removeAllObjects()
               if radioText == "summerized"{
            
                if (customerLabel.text == ""){
                    Common().showAlert(Constants().APP_NAME, message: "Please Select Customer",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
                   getCorporateOutSumm()
                     //getCustomerList1()
                }
                else {
                    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
            
            
        }
        else if radioText == "detailed"{
           
                if (customerLabel.text == ""){
                    Common().showAlert(Constants().APP_NAME, message: "Please Select Customer",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
                    self.performSegueWithIdentifier("toOutstandingSales", sender: self)
                    //getCorporateOutDet()
                }
                else{
                    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
        }
        //        branchLabel.text = "Branch"
        //        customerLabel.text = ""
        filter.removeAll()
        
    }
    
        @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func summrzdPressed(sender: AnyObject) {
        // radiotag = summrzdButton.tag
        if radiotag1 == 0 {
            summerzdImage.image = UIImage(named: "rdChecked.png")
            detaildImage.image  = UIImage(named: "rdUnChecked.png")
            radioText = "summerized"
            radiotag2 = 1
        }
        else {
            summerzdImage.image = UIImage(named: "rdUnChecked.png")
            detaildImage.image = UIImage(named: "rdChecked.png")
            radioText = "detailed"
            radiotag2 = 0
        }
        
    }
    
    @IBAction func detaildPressed(sender: AnyObject) {
        //radiotag = detaildButton.tag
        if radiotag2 == 0{
            detaildImage.image = UIImage(named: "rdUnChecked.png")
            summerzdImage.image = UIImage(named: "rdChecked.png")
            radioText = "summerized"
            radiotag1 = 1
        }
        else {
            detaildImage.image = UIImage(named: "rdChecked.png")
            summerzdImage.image = UIImage(named: "rdUnChecked.png")
            radioText = "detailed"
            radiotag1 = 0
        }
        if radioText == "detailed"{
            
            if (customerLabel.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "Please Select Customer",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                self.performSegueWithIdentifier("toOutstandingSales", sender: self)
                //getCorporateOutDet()
            }
            else{
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }

    }
    
    func getCustomerList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_CUSTOMERS?CUSTOMER="
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
 print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.customerArray)
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getCorporateOutSumm()

    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_OUTSTANDING?COMPANY=1&DIVISION=1&CUSTOMER=" + customerLabel.text!
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
           print("customer",self.outstandingSummArray)
            if self.outstandingSummArray.count == 0 {
                 dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
                    
                    
                })

            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getCorporateOutDet()
        
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_OUTSTANDING_DETAIL?COMPANY=1&DIVISION=1&CUSTOMER=" + customerLabel.text!
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.outstandingDetArray)
            if self.noData == true {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }


    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "CUST_COMPANY_GROUP"){
                noData = false
                elementValue = String()
                element = elementName
                customer[elementName] = ""
            }
            
            if (elementName == "BRN_ID" || elementName == "branch" || elementName == "outstanding") {
                noData = false
                elementValue = String()
                element = elementName
                outstandingSum[elementName] = ""
            }
            if (elementName == "branch" || elementName == "CUSTOMER" || elementName == "BillDate" || elementName == "BillNo" || elementName == "Outstanding" || elementName == "Days" ){
                noData = false
                elementValue = String()
                element = elementName
                outstandingDet[elementName] = ""
            }
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "CUST_COMPANY_GROUP"){
                if (string.rangeOfString("\n") == nil){
                    customer[element!] = string
                }
                
            }
            if (element == "BRN_ID" || element == "branch" || element == "outstanding") {
                if (string.rangeOfString("\n") == nil){
//                    if element == "customer"{
//                        if count == 0{
//                            outstandingSum[element!] = string
//
//                            count += 1
//                        }
//                    }
//                    else {
//                        outstandingSum[element!] = string
//                    }
  outstandingSum[element!] = string
                    
                }
            }
            
            if(element == "branch" || element == "CUSTOMER" || element == "BillDate" || element == "BillNo" || element == "Outstanding" || element == "Days" ){
                if (string.rangeOfString("\n") == nil){
                    outstandingDet[element!] = string
                }
            }
            
            
            
        }
        
        //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
        //                if (string.rangeOfString("\n") == nil){
        //                    price[element!] = string
        //                }
        //            }
        //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
        //                if (string.rangeOfString("\n") == nil){
        //                    expiry[element!] = string
        //                }
        //            }
        
        
    }
    
    
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "CUST_COMPANY_GROUP" {
            customerArray.addObject(customer)
            
        }
        if elementName == "outstanding" {
            count = 0
            outstandingSummArray.addObject(outstandingSum)
        }
        if elementName == "Days" {
            outstandingDetArray.addObject(outstandingDet)
        }
    }
    
    

}
