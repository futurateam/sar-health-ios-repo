//
//  SalesPerfExecSumm.swift
//  SAR Health
//
//  Created by Anargha K on 21/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfExecSumm: UIViewController, UITableViewDataSource, UITableViewDelegate{
  
    @IBOutlet var salesTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var salesText: UILabel!
    @IBOutlet var salesAmount: UILabel!
    
    var no = ["No"], executive = ["Executive"], quantity = ["Qty"], amount = ["Amount (₹)"], brn = [String]()
    var branchName = "", fromDate = "", toDate = "", execName = ""
    var salesSumm : NSMutableArray = [], executiveLegend = [String](), amountData = [Double](), i = 0, j = 0, qtyTot = 0
    var total : Double = 0, limit = 0, sum : Double = 0
    var num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        for ( i = 0; i < salesSumm.count ; i += 1){
            total = 0
            qtyTot = 0
            print("i: ",i)
//            print("Count", count)
//            count = i
            limit = 0
            //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
            
            
            let branch = salesSumm[i].valueForKey("Branch") as? String
            //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
            //            prName.append("Product")
            if (i == 0 ){
                brn.append(branch!)
               // branchAllArray.append(branch!)
                for ( j = i ; j < salesSumm.count ; j += 1){
                    
                    if salesSumm[j].valueForKey("Branch") as? String == branch{
                        //branchAllArray.append(branch!)
                        brn.append("")
                        
                        
                        executive.append((salesSumm[j].valueForKey("SalesPerson") as? String)!)
                        quantity.append((salesSumm[j].valueForKey("SaleQty") as? String)!)
                        amount.append(num.stringFromNumber(Double((salesSumm[j].valueForKey("Amount") as? String)!)!)!)
                        total = total + Double((salesSumm[j].valueForKey("Amount") as? String)!)!
                        qtyTot = qtyTot + Int ((salesSumm[j].valueForKey("SaleQty") as? String)!)!
                        limit++
                        no.append(String(limit))
                        //count += 1
                        
                    }
                    
                }
                let tot =  "Total = " + num.stringFromNumber(total)!
                 let qty = "Total Qty = " + String(qtyTot)
                amount.append(tot)
                sum =  total
                //brn.append("")
               // branchAllArray.append("")
                brn.append("")
                //no.append("")
                no.append("")
                no.append("No")
                //customer.append("")
                
                // billDate.append("")
                executive.append("")
                executive.append("Executive")
                //billNo.append("")
                quantity.append(qty)
                // days.append("")
                quantity.append("Qty")
                //value.append("")
                amount.append("Amount (₹)")
                // i = count
                
            }else {
                if brn.contains(branch!){
                    
                }
                else {
                    brn.append(branch!)
                   // branchAllArray.append(branch!)
                    for ( j = 0 ; j < salesSumm.count ; j += 1){
                        
                        if salesSumm[j].valueForKey("Branch") as? String == branch{
                            //branchAllArray.append(branch!)
                            brn.append("")
                          
                            
                            executive.append((salesSumm[j].valueForKey("SalesPerson") as? String)!)
                            quantity.append((salesSumm[j].valueForKey("SaleQty") as? String)!)
                            amount.append(num.stringFromNumber(Double((salesSumm[j].valueForKey("Amount") as? String)!)!)!)
                            total = total + Double((salesSumm[j].valueForKey("Amount") as? String)!)!
                             qtyTot = qtyTot + Int ((salesSumm[j].valueForKey("SaleQty") as? String)!)!
                            limit++
                            no.append(String(limit))
                            // count += 1
                            
                        }
                        
                    }
                    let tot =  "Total = " + num.stringFromNumber(total)!
                    let qty = "Total Qty = " + String(qtyTot)
                    amount.append(tot)
                    sum = sum + total
                    
                    //brn.append("")
                   // branchAllArray.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    
                    // billDate.append("")
                    executive.append("")
                    executive.append("Executive")
                    //billNo.append("")
                    quantity.append(qty)
                    // days.append("")
                    quantity.append("Qty")
                    //value.append("")
                    amount.append("Amount (₹)")
                    //i = count
                    
                    
                }
            }
            
            
        }
        // no.removeAtIndex(no.count - 1)
        no.removeAtIndex(no.count - 1)
        //  brn.removeAtIndex(brn.count - 1)
        // customer.removeAtIndex(customer.count - 1)
       
        //billDate.removeAtIndex(billDate.count - 1)
        // billNo.removeAtIndex(billNo.count - 1)
        
        // days.removeAtIndex(days.count - 1)
        executive.removeAtIndex(executive.count - 1)
        quantity.removeAtIndex(quantity.count - 1)
        amount.removeAtIndex(amount.count - 1)
        // value.removeAtIndex(value.count - 1)
        print("Branch", brn)
        //print("Branch All", branchAllArray)
        print("No",no)
       
        print("Exxecutive", executive)
        print("Quantity", quantity)
        print("Amount", amount)
        print("Branch", brn.count)
        print("No",no.count)
        //print("Branch All", branchAllArray.count)
        
        print("Executive", executive.count)
        print("Quantity", quantity.count)
        print("Amount", amount.count)
        
        
        self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
        /*   outstandingText.text = "Total outstanding for " + customerName + " is "
        amountText.text = "₹ " + num.stringFromNumber(sum)!*/
        
        
        
        //        for(var i = 0; i < salesItem.count; i += 1){
        //            total = total + Double((salesItem[i].valueForKey("VALUE") as? String)!)!
        //        }
        
        //totalText.text = num.stringFromNumber(sum)
        salesText.text = "Total sales in " + branchName + " from " + fromDate + " to " + toDate + " is "
        salesAmount.text = "₹ " + num.stringFromNumber(sum)!
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphSalesPerfExecSumm" {
            executiveLegend.removeAll()
            amountData.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Executive"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Manufacturer"
            if execName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = execName
            }
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Branch)"
            for(var  i = 0; i < salesSumm.count; i += 1){
                executiveLegend.append((salesSumm[i].valueForKey("SalesPerson") as? String)!)
                amountData.append(Double((salesSumm[i].valueForKey("Amount") as? String)!)!)
            }
            dest.chartData = amountData
            dest.chartLegend = executiveLegend
        }*/
        if segue.identifier == "toPieSalesPerfExec" {
            executiveLegend.removeAll()
            amountData.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Sales Performance (Executive)"
            for(var  i = 0; i < salesSumm.count; i += 1){
                executiveLegend.append((salesSumm[i].valueForKey("SalesPerson") as? String)!)
                amountData.append(Double((salesSumm[i].valueForKey("Amount") as? String)!)!)
            }
            dest.chartData = amountData
            dest.chartLegend = executiveLegend
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalePerfExecSummCell", forIndexPath: indexPath) as! SalesPerfExecSummCell
        cell.category.text = brn[indexPath.row]
        cell.quantity.textAlignment = NSTextAlignment.Center
         cell.quantity.textAlignment = NSTextAlignment.Center
        if no[indexPath.row] == "No" {
            cell.category.backgroundColor = UIColor.whiteColor()
            
            cell.no.attributedText = headFont(no[indexPath.row])
            
            cell.executive.attributedText = headFont(executive[indexPath.row])
            cell.quantity.attributedText = headFont(quantity[indexPath.row])
            cell.amount.attributedText = headFont(amount[indexPath.row])
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            
            cell.no.text = no[indexPath.row]
            
            
            cell.executive.text = executive[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = amount[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        if quantity[indexPath.row].containsString("Total" ) {
            cell.quantity.textAlignment = NSTextAlignment.Right
            cell.amount.textAlignment = NSTextAlignment.Right
            var longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.quantity.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.amount.attributedText = attributedString
            
        }
        
        
        
        
        
        return cell
    }
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(13)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }


    @IBAction func viewGraphPressed(sender: AnyObject) {
        //performSegueWithIdentifier("toGraphSalesPerfExecSumm", sender: self)
        performSegueWithIdentifier("toPieSalesPerfExec", sender: self)
    }
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    
}
