//
//  CreditNoteDet.swift
//  SAR Health
//
//  Created by Anargha K on 24/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CreditNoteDet: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var creditNoteTable: UITableView!
    @IBOutlet var totalText: UILabel!
    
    var noDataArray : NSMutableArray = [], noDataDic = [String :String]()
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var branchDetArray : NSMutableArray = []
    var creditNoteDet = [String : String](), branchCode = "", manCode = "", creditNoteDetArray : NSMutableArray = [], mfgArray : NSMutableArray = []
    var txn = "", finyearArray : NSMutableArray = [], finyear = [String : String]()
    var creditNote : NSMutableArray = [], manfactName = "", amountArray = [Double](), qtyTot : Double = 0, brn = [String]()
   var custName = ""
    var no = ["No"]
    var product = ["Product"]
    var batch = ["Batch No"]
    
    var amount = ["Amount(₹)"]
    var qty = ["Return Qty"]
    var branchId = ["Branch ID"], txnNo = ["Txn No"]
    var fromDate = ""
    var toDate = "", branchName = "", count = 0, limit = 0, sum : Double = 0, i = 0, j = 0
    var total : Double = 0
    var productArray = [String]()
    var num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        for (var i = 0; i < creditNote.count; i += 1){
            no.append(String(i+1))
            product.append((creditNote[i].valueForKey("Product") as? String)!)
            batch.append((creditNote[i].valueForKey("Batch_No") as? String)!)
            qty.append((creditNote[i].valueForKey("Return_Qty") as? String)!)
            amount.append((creditNote[i].valueForKey("Amount") as? String)!)
        }
        //self.creditNoteTable.tableFooterView = UIView.init(frame: CGRectZero)
        for(var  i = 0; i < creditNote.count; i += 1){
            total = total + Double((creditNote[i].valueForKey("Amount") as? String)!)!
            
        }
        
        
        
        
        
        totalText.text = num.stringFromNumber(total)
        // qtyText.text = String(qtyTot)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        /* if segue.identifier == "toGraphCreditNoteDet" {
         productArray.removeAll()
         amountArray.removeAll()
         let dest = segue.destinationViewController as! GraphViewController
         dest.xaxisName = "Product"
         dest.yaxisName = "Amount"
         dest.cusprodText = "Prodct"
         
         dest.cusProdValue = custName
         
         
         dest.titleTab = "Credit Note"
         for(var  i = 0; i < creditNote.count; i += 1){
         productArray.append((creditNote[i].valueForKey("Product") as? String)!)
         amountArray.append(Double((creditNote[i].valueForKey("Amount") as? String)!)!)
         }
         dest.chartData = amountArray
         dest.chartLegend = productArray
         
         }*/
        if segue.identifier == "toPieOutstandingSumm"{
            productArray.removeAll()
            amountArray.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Credit Note"
            for(var  i = 0; i < creditNote.count; i += 1){
                productArray.append((creditNote[i].valueForKey("Product") as? String)!)
                amountArray.append(Double((creditNote[i].valueForKey("Amount") as? String)!)!)
            }
            dest.chartData = amountArray
            dest.chartLegend = productArray
        }

        /* if segue.identifier == "toSalesPerfBranchDet" {
         let dest = segue.destinationViewController as? SalesPerfBranchDet
         dest?.branchArray = branchDetArray
         dest?.branchName = branchName
         dest?.fromDate = fromDate
         dest?.toDate = toDate
         dest?.manfactName = manfactName
         dest?.branchCode = branchCode
         
         
         }*/
        
    }
    
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = creditNoteTable.dequeueReusableCellWithIdentifier("CreditNoteDetCell", forIndexPath: indexPath) as! CreditNoteDetCell
        
        
        if no[indexPath.row] == "No" {
            
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            //            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            //
            //            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            //
            //            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            //            cell.branch.attributedText = attributedString
            longestWordRange = (product[indexPath.row] as NSString).rangeOfString(product[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: product[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.product.attributedText = attributedString
            longestWordRange = (batch[indexPath.row] as NSString).rangeOfString(batch[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: batch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.batchNo.attributedText = attributedString
            
            longestWordRange = (qty[indexPath.row] as NSString).rangeOfString(qty[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: qty[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.returnQty.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.amount.attributedText = attributedString
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            
            
        }
        else {
            cell.no.text = no[indexPath.row]
            cell.product.text = product[indexPath.row]
            cell.batchNo.text = batch[indexPath.row]
            
            
            cell.amount.text = amount[indexPath.row]
            cell.returnQty.text = qty[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
         navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("toPieOutstandingSumm", sender: self)
    }

}
