//
//  ProductExpiryBranchCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 18/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductExpiryBranchCell: UITableViewCell {
   
    @IBOutlet var batchNo: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var date: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

}
