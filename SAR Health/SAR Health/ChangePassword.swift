 //
//  ChangePassword.swift
//  SAR Health
//
//  Created by Anargha K on 26/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ChangePassword: UIViewController, NSXMLParserDelegate {

    @IBOutlet var userName: UITextField!
    
    @IBOutlet var oldPass: UITextField!
    
    @IBOutlet var newPass: UITextField!
    
    @IBOutlet var conPass: UITextField!
    var elementValue: String?
    var element : String?
    
    var chngPass = [String : String](), chngPassArray : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toDashBoard"{
            let dest = segue.destinationViewController as! DashboardViewController
            if chngPassArray != []{
                dest.chngPass = chngPassArray
            }
        }
    }

   
    @IBAction func changePassPressed(sender: AnyObject) {
        chngPassArray.removeAllObjects()
        if (userName.text == "" || oldPass.text == "" || newPass.text == "" || conPass.text == "") {
            Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory", viewController: self)
        }
        else if (newPass.text != conPass.text){
            Common().showAlert(Constants().APP_NAME, message: "New Password and Confirm password mismatch", viewController: self)
        }
        else {
            getChangePass()
        }
       
    }
    
    func getChangePass()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let scriptUrl1 = Constants().API + "MOBAPP_CHANGEPASSWORD?USERNAME=" + userName.text! + "&OLDPASSWORD=" + oldPass.text! + "&NEWpASSWORD=" + newPass.text!
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
        parser.parse()
        print("Sales Ind", (self.chngPassArray[0].valueForKey("Column1") as? String))
            
            
            
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                
                    if ((self.chngPassArray[0].valueForKey("Column1") as? String) == "Password changed Successfully"){
                        self.performSegueWithIdentifier("toDashBoard", sender: self)
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: (self.chngPassArray[0].valueForKey("Column1") as? String)!, viewController: self)

                }
                

               
            })
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Column1"){
                elementValue = String()
                element = elementName
                chngPass[elementName] = ""
            }
            
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if element == "Column1"{
                if (string.rangeOfString("\n") == nil){
                    chngPass[element!] = string
                                   }
                
            }
            
            
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "Column1" {
                       chngPassArray.addObject(chngPass)
            
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    


    @IBAction func backPressed(sender: AnyObject) {
        performSegueWithIdentifier("toDashBoard", sender: self)
    }
}
