//
//  SalesPerfCustSumCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 26/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfCustSumCell: UITableViewCell {
    @IBOutlet var no: UILabel!
    
    @IBOutlet var manfact: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!

}
