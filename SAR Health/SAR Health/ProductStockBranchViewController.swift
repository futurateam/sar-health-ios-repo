//
//  ProductStockBranchViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 15/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductStockBranchViewController: UIViewController {

    @IBOutlet var stockResultText: UILabel!
    @IBOutlet var stockText: UILabel!
    
    var longString : NSString = ""
    var productName = ""
    var branchName = ""
    var stockNo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.hideUIBlockingIndicator()
        // Do any additional setup after loading the view.
        stockResultText.text = "Stock Result of " + productName + " in " + branchName
        
        longString = productName + " has " + stockNo + " numbers in stock in " + branchName
        let longestWord = stockNo
        
        let longestWordRange = (longString as NSString).rangeOfString(longestWord)
        
        let attributedString = NSMutableAttributedString(string: longString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(20)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(20), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
        
        
        stockText.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
       @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func checkAnotherPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
