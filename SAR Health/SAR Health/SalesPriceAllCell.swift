//
//  SalesPriceAllCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPriceAllCell: UITableViewCell {
    @IBOutlet var no: UILabel!

    @IBOutlet var branch: UILabel!
    @IBOutlet var price: UILabel!
}
