//
//  ProductTableViewCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 11/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet var productTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
