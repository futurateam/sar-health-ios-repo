//
//  CreditNoteDetCell.swift
//  SAR Health
//
//  Created by Anargha K on 24/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CreditNoteDetCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    
    @IBOutlet var product: UILabel!
    
    @IBOutlet var batchNo: UILabel!
    
    @IBOutlet var returnQty: UILabel!
    
    @IBOutlet var amount: UILabel!
    
}
