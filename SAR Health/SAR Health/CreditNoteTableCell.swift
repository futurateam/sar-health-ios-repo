//
//  CreditNoteTableCell.swift
//  SAR Health
//
//  Created by Anargha K on 24/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CreditNoteTableCell: UITableViewCell {

    @IBOutlet var no: UILabel!
   
    @IBOutlet var branch: UILabel!
    @IBOutlet var customer: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var billNo: UILabel!
    
}
