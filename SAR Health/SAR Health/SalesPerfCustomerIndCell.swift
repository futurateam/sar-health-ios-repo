//
//  SalesPerfCustomerIndCell.swift
//  SAR Health
//
//  Created by Futuralabs on 10/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfCustomerIndCell: UITableViewCell {
    @IBOutlet weak var invNo: UILabel!
    @IBOutlet weak var invDate: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var manfact: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var batch: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var amount: UILabel!

    @IBOutlet var prodName: UILabel!
    @IBOutlet var category: UILabel!
      
}
