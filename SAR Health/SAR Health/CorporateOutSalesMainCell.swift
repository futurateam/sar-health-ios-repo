//
//  CorporateOutSalesMainCell.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit


class CorporateOutSalesMainCell: UITableViewCell {
    @IBOutlet var no: UILabel!
    @IBOutlet var branch: UILabel!
    @IBOutlet var customer: UILabel!
    @IBOutlet var qty: UILabel!
    @IBOutlet var sales: UILabel!

}
