//
//  SalePerformanceItemAll.swift
//  SAR Health
//
//  Created by Prajeesh KK on 19/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalePerformanceItemAll: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
    
    @IBOutlet var salesTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var saleText: UILabel!
    @IBOutlet var amountText: UILabel!
    
    @IBOutlet var qtyTot: UILabel!
    
    
    var salesItemBranchArray : NSMutableArray = [], noDataArray : NSMutableArray = [], noDataDic = [String :String]()
    var salesItemBranch = [String : String](), branchArray : NSMutableArray = [], qty = 0
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var productName = "", branchName = ""
    var branch = [String](), branchCode = "", productCode = ""
    var sales = [Double]()
    var salesItem : NSMutableArray = []
    var total : Double = 0
    var fromDate = ""
    var toDate = "", num = NSNumberFormatter()
//    var no = [String]()
//    var branch = [String]()
//    var quantity = [String]()
//    var amount = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("salesItem",salesItem)
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale

  //   self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for(var  i = 0; i < salesItem.count; i += 1){
                total = total + Double((salesItem[i].valueForKey("VALUE") as? String)!)!
                qty = qty + Int((salesItem[i].valueForKey("QTY") as? String)!)!
        }
        qtyTot.text = String(qty)
        saleText.text = "Total sales in " + branchName + " branches from " + fromDate + " to " + toDate + " is "
        amountText.text = "₹ " + num.stringFromNumber(total)!
        totalText.text = num.stringFromNumber(total)!
        
//        no = ["1", "2", "3", "4", "5", "6", "7"]
//        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
//        quantity = ["50", "100", "75", "100", "100", "100", "100"]
//        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
      /*
         if segue.identifier == "toGraphSalesItemSum" {
            sales.removeAll()
            branch.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Branch"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Product"
            if productName == ""{
             dest.cusProdValue = ""
            }
            else{
                 dest.cusProdValue = productName
            }
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Item)"
            for(var  i = 0; i < salesItem.count; i += 1){
                sales.append(Double((salesItem[i].valueForKey("VALUE") as? String)!)!)
                branch.append((salesItem[i].valueForKey("BRANCH") as? String)!)
            }
            dest.chartData = sales
            dest.chartLegend = branch
        }*/
        if segue.identifier == "toPieSalesItemAll"{
            sales.removeAll()
            branch.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Sales Performance (Product)"
            for(var  i = 0; i < salesItem.count; i += 1){
                sales.append(Double((salesItem[i].valueForKey("VALUE") as? String)!)!)
                branch.append((salesItem[i].valueForKey("BRANCH") as? String)!)
            }
            dest.chartData = sales
            dest.chartLegend = branch
        
        }
        if segue.identifier == "toSalesPerfItemBranchDet" {
            let dest = segue.destinationViewController as? SalesPerfItemBrachDetViewController
            dest?.salesItem = salesItemBranchArray
            dest?.branchName = branchName
            dest?.fromDate = fromDate
            dest?.toDate = toDate
            dest?.productName = productName
            dest?.salesDet = salesItemBranchArray
            //dest?.branchCode = branchCode
        }

    }
    
    
    //MARK: -Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salesItem.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalePerfCell", forIndexPath: indexPath) as! SalesPerformanceItemAllCell
        cell.no.text = String(indexPath.row + 1)
        cell.branch.text = salesItem[indexPath.row].valueForKey("BRANCH") as? String
        cell.quantity.text = salesItem[indexPath.row].valueForKey("QTY") as? String
        cell.amount.text = num.stringFromNumber(Double((salesItem[indexPath.row].valueForKey("VALUE") as? String)!)!)
        return cell
    }
    
   
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = salesTable.dequeueReusableCellWithIdentifier("HeaderCell") as! SalesPerformanceItemAllHeaderCell
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       salesItemBranchArray.removeAllObjects()
        print(salesItem[indexPath.row].valueForKey("BRANCH") as? String)
       
        for(var i = 0; i < branchArray.count; i++){
            
            if  (salesItem[indexPath.row].valueForKey("BRANCH") as? String == branchArray[i].valueForKey("BRN_NAME") as? String){
                 branchCode = (branchArray[i].valueForKey("BRN_ID") as? String)!
            }
        }
        
        getSalesItemAll()
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
       // performSegueWithIdentifier("toGraphSalesItemSum", sender: self)
        performSegueWithIdentifier("toPieSalesItemAll", sender: self)
    }
    func getSalesItemAll()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
       
        var scriptUrl1 = ""
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
                   scriptUrl1 = Constants().API + "MOBAPP_PRODUCT_SALE_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + productCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
          
            print("Nodata", self.noDataArray)
            dispatch_async(dispatch_get_main_queue(), {
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found..", viewController: self)
                    
                    
                }
                else{
                    
                   
                        HUD.hideUIBlockingIndicator()
                        print("Sales Detail",self.salesItemBranchArray)
                        if self.noData == true {
                            Common().showAlert(Constants().APP_NAME, message: "No Data Found..", viewController: self)
                            
                        }
                    
                            //self.performSegueWithIdentifier("toSalesPerfItemDet", sender: self)
                    
                
                            HUD.hideUIBlockingIndicator()
                            self.performSegueWithIdentifier("toSalesPerfItemBranchDet", sender: self)
                
                        
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName == "Table1"){
            if elementName == "Column1" {
                noData = false
                elementValue = String()
                element = elementName
                noDataDic[elementName] = ""
            }
        }
        if (elementName == "SLNO" || elementName == "Branch_id" || elementName == "Branch" || elementName == "SO_CUST_CODE" || elementName == "PR_CAT_ID" || elementName == "CUST_NAME" || elementName == "QTY" || elementName == "VALUE") {
            
            noData = false
            elementValue = String()
            element = elementName
            salesItemBranch[elementName] = ""
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "SLNO" || element == "Branch_id" || element == "Branch" || element == "SO_CUST_CODE" || element == "PR_CAT_ID" || element == "CUST_NAME" || element == "QTY" || element == "VALUE"){
                if string.rangeOfString("\n") == nil{
                    salesItemBranch[element!] = string
                }
            }
            if (element == "Table1"){
                if element == "Column1" {
                    noDataDic[element!] = string
                }
            }
        }
        
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "VALUE" {
            salesItemBranchArray.addObject(salesItemBranch)
        }
        if elementName == "Table1" {
            if elementName == "Column1" {
                noDataArray.addObject(noDataDic)
            }
        }
        
    }

}
