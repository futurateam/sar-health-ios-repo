//
//  LowStockController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class LowStockController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    
    
    @IBOutlet var branchTable: UITableView!
    @IBOutlet var branchName: UILabel!
    
    var elementValue: String?
    var element : String?
    var btag = 0, branchCode = "", branch = [String](), branchCount : Int = 0
    var branchList : NSMutableArray = [], lowStockArray : NSMutableArray = []
    var lowStock = [String : String](), finyear = [String:String](), finyearArray : NSMutableArray = []
    var noData : Bool = false
    override func viewDidLoad() {
    
        super.viewDidLoad()
        branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
        if branchList.count >= branchCount {
            branch = ["All"]
        }
        else {
            branch = []
        }
        if Reachability.isConnectedToNetwork(){
            getFinyear()
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        for (var i = 0 ;i < branchList.count ; i += 1) {
            branch.append((branchList[i].valueForKey("BRN_NAME") as? String)!)
        }
        
        branchTable.hidden = true
                // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if segue.identifier == "toLowStockReport" {
            let dest = segue.destinationViewController as? LowStockReport
            dest?.lowStockValue = lowStockArray
            dest?.branchName = branchName.text!
            dest?.branchCode = branchCode
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return branch.count;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
                  let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! LowStockBranchCell
            //print("Array",branchList)
            // print("count", branchList.count)
            //print("Indexpath", indexPath.row)
            //print (branch[indexPath.row])
            
           cell.branchName.text = branch[indexPath.row]
            return cell
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     
            branchName.text = branch[indexPath.row]
            if branchName.text != "All"{
                
                branchCode = (branchList[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
            }
            else {
                branchCode = "0"
            }
            
            branchTable.hidden = true
        
        
    }
    
    @IBAction func findPressed(sender: AnyObject) {
        lowStockArray.removeAllObjects()
        if branchName.text == "Branch"{
            Common().showAlert(Constants().APP_NAME, message: "Please Select Branch",viewController: self)
        }
       
        else if Reachability.isConnectedToNetwork(){
            getLowStock()
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        
            
            
            //            if branchLabel.text == "All"{
            //
            //
            //                //self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                // getDataProductAll()
            //                if check == false{
            //
            //                    // print(productStock)
            //                //self.performSegueWithIdentifier("toProductBranch", sender: self)
            //               }
            //                else {
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //                }
            //            }
            //
            
       
        }
    
    
    @IBAction func branchButton(sender: AnyObject) {
        view.endEditing(true)
        if(btag == 0){
            self.branchTable.hidden = true
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                var frame = self.branchTable.frame
                frame.size.height = 0;
                self.branchTable.frame = frame
                }, completion:{finished in} )
            btag = 1;
        }
        else{
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                var frame = self.branchTable.frame;
                frame.size.height = 175;
                self.branchTable.frame = frame;
                }, completion: { finished in })
            self.branchTable.hidden = false
            
            btag = 0;
        }
        
        
    }
    

    
    func getLowStock()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        let scriptUrl1 = Constants().API + "MOBAPP_LOWSTOCK_PRODUCT?BRANCH=" + branchCode + "&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
        
            print("low stack",self.lowStockArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            dispatch_async(dispatch_get_main_queue(), {
            
                        if (self.noData == true){
                            HUD.hideUIBlockingIndicator()
                            Common().showAlert(Constants().APP_NAME, message: "No data found..", viewController: self)
            
            
                        }
                        else{
                            
                                
            
                                    HUD.hideUIBlockingIndicator()
                                    self.performSegueWithIdentifier("toLowStockReport", sender: self)
                                
                                
                            
                        }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
     
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }

    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "branch" || elementName == "product" || elementName == "stock"){
                noData = false
                elementValue = String()
                element = elementName
                lowStock[elementName] = ""
            }
            if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
                noData = false
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
                        //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "branch" || element == "product" || element == "stock"){
                if (string.rangeOfString("\n") == nil){
                    lowStock[element!] = string
                }
                
            }
            if (element == "FIN_YEAR_ID" || element == "FINYEAR") {
                if (string.rangeOfString("\n") == nil){
                    finyear[element!] = string
                }
                
            }
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "stock" {
            lowStockArray.addObject(lowStock)
            
        }
        if elementName == "FINYEAR" {
            finyearArray.addObject(finyear)
        }

        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }


   

    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
