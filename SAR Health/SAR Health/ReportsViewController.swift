//
//  ReportsViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 19/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ReportsViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NSXMLParserDelegate {
    @IBOutlet var reportsCollection: UICollectionView!
    
    var reportImage = [String]()
    var reportName = [String](), finyearArray : NSMutableArray = [], finyear = [String : String]()
    
    
    var manfactArray : NSMutableArray = []
    var manfact = [String : String]()
    
    var noData : Bool = false
    var itemArray : NSMutableArray = []
    var elementProduct : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var flag = true
    var item = [String : String]()
    var cellNo = 0
    var branchList : NSMutableArray = [], moduleName = [String]()
    var itemList : NSMutableArray = [], reportsFlag = [Bool](), moduleArray : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        getManfactList()
        getFinyear()
        branchList = NSUserDefaults.standardUserDefaults().valueForKey("Branches") as! NSMutableArray
        itemList = NSUserDefaults.standardUserDefaults().valueForKey("Products") as! NSMutableArray
        moduleName = ["SALES PERFORMANCE(ITEM)", "SALES PERFORMANCE(CUSTOMERS)", "SALES PERFORMANCE(BRANCH)", "SALES PERFORMANCE(EXECUTIVE)", "CORPORATE CUSTOMERS(SALES)", "OUTSTANDING VALUE", "LOW STOCK PRODUCT", "BRANCH TARGET", "SALES PERFORMANCE(MANUFACTURE)"]
        reportImage = ["salesitem.png", "salescutomer.png", "salesbracnh.png", "salesexe.png", "corp.png", "value.png", "lowstock.png", "report.png", "salesitem.png"]
        reportName = ["SALES PERFORMANCE (PRODUCT)", "SALES PERFORMANCE (CUSTOMERS)", "SALES PERFORMANCE (BRANCH)", "SALES PERFORMANCE (EXECUTIVE)", "CORPORATE CUSTOMERS (SALES)", "OUTSTANDING VALUE", "LOW STOCK REPORT", "BRANCH TARGET", "SALES PERFORMANCE(MANUFACTURE)"]
        reportsFlag = [false, false, false, false, false, false, false, false, false]
                // Do any additional setup after loading the view.
        moduleArray = NSUserDefaults.standardUserDefaults().valueForKey("Module_Array") as! NSMutableArray
        print(moduleArray)
        for (var i = 0; i < self.moduleArray.count ; i++){
            for (var j = 0 ; j < self.reportName.count ; j++){
                if(self.moduleArray[i].valueForKey("ModuleName") as? String == self.moduleName[j]){
                    self.reportsFlag[j] = true
                }
                else {
                    if self.reportsFlag[j] != true {
                        self.reportsFlag[j] = false
                    }
                }
                
            }
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
//     MARK: - Navigation
    
     //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//     Get the new view controller using segue.destinationViewController.
//     Pass the selected object to the new view controller.
//        if segue.identifier == "toCorporateOut" {
//             let dest = segue.destinationViewController as? CorporateOutViewController
//            dest?.itemList = itemList
//            dest?.cellNo = cellNo
//
//        }
        if segue.identifier == "toSalesPerfItem"{
            let dest = segue.destinationViewController as? SalesPerformanceItem
            print("Branch   list",branchList)
            //dest?.branch = branchList
            dest?.branch = branchList
            dest?.itemList = itemList
            dest?.cellNo = cellNo
            
        }
        else if segue.identifier == "toSalesPerfCustomer"{
            let dest = segue.destinationViewController as? SalesPerformanceCustomer
            dest?.manfactArray = manfactArray
            dest?.itemList = itemList
            dest?.branch = branchList
            if finyearArray != [] {
                dest?.finyearArray = finyearArray
            }
            dest?.cellNo = cellNo
            
        }
        else if segue.identifier == "toLowStock" {
            let dest = segue.destinationViewController as? LowStockController
            dest?.branchList = branchList
        }
        else if segue.identifier == "toCorporateOut" {
            let dest = segue.destinationViewController as? CorporateOutViewController
            dest?.branchArray = branchList
            dest?.manfact = manfactArray
            dest?.cellNo = cellNo
        }
        else if segue.identifier == "toBranchTarget"{
        let dest = segue.destinationViewController as? BranchTargetController
          print("Branch   list",branchList)
            dest?.branchList = branchList
        
        }
    }
    

    //MARK: -Collection View Methods
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reportName.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = reportsCollection.dequeueReusableCellWithReuseIdentifier("ReportsCell", forIndexPath: indexPath) as! ReportsCollectionViewCell
        cell.repName.text = reportName[indexPath.row]
        if reportsFlag[indexPath.row]{
        let image = UIImage (named: reportImage[indexPath.row])
        cell.repImage.image = image
        }
        else {
            let image = UIImage (named:  "notinuse.png")
            cell.repImage.image = image
        }
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
               return CGSizeMake(88, 119)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(40,20,5,20)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 30.0
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        cellNo = indexPath.row
        if reportsFlag[cellNo] == true {
            if  cellNo == 4{
                performSegueWithIdentifier("toCorporateOut", sender: self)
            }
            else
                if cellNo == 0 || cellNo == 1 || cellNo == 2 || cellNo == 5 || cellNo == 8{
                    performSegueWithIdentifier("toSalesPerfCustomer", sender: self)
                }
                else if cellNo == 3 {
                    getFinyear()
                    performSegueWithIdentifier("toSalesPerfCustomer", sender: self)
                }
                else if cellNo == 6 {
                    performSegueWithIdentifier("toLowStock", sender: self)
            }
                else if cellNo == 7 {
                    performSegueWithIdentifier("toBranchTarget", sender: self)
            }
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "The user can't access this", viewController: self)
        }
    }
    
    
    
//    
//    func getDataItem ()
//    {
//        
//        
//        let scriptUrl = Constants().API + "MOBAPP_PRODUCTS?PRODUCT="
//        
//        // Add one parameter
//        //let urlWithParams = scriptUrl + "?BRANCH=1"
//        
//        // Create NSURL Ibject
//        let myUrl = NSURL(string: scriptUrl);
//        
//        // Creaste URL Request
//        let request = NSMutableURLRequest(URL:myUrl!);
//        
//        // Set request HTTP method to GET. It could be POST as well
//        request.HTTPMethod = "GET"
//        
//        // If needed you could add Authorization header value
//        // Add Basic Authorization
//        /*
//        let username = "myUserName"
//        let password = "myPassword"
//        let loginString = NSString(format: "%@:%@", username, password)
//        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
//        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
//        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
//        */
//        
//        // Or it could be a single Authorization Token value
//        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
//        
//        // Excute HTTP Request
//        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
//            data, response, error in
//            // Check for error
//            if error != nil
//            {
//                print("error=\(error)")
//                return
//            }
//            
//            // Print out response string
//            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//            print("responseString = \(responseString)")
//            let parser = NSXMLParser(data: data!)
//            parser.delegate = self
//            parser.parse()
//            
//        }
//        
//        task.resume()
//        
//        
//    }
//    
//    
//    
//    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
//        //check = false
//        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
//            if (elementName == "ID" || elementName == "BRANCH"){
//                elementValue = String()
//                element = elementName
//                sample[elementName] = ""
//            }
//            else if (elementName == "PRODUCT_CODE" || elementName == "PRODUCT"){
//                elementValue = String()
//                element = elementName
//                sampleProduct[elementName] = ""
//                
//            }
//            
//        }
//        
//    }
//    
//    func parser(parser: NSXMLParser, foundCharacters string: String) {
//        //        if elementValue != nil {
//        //            elementValue! += string
//        //
//        //
//        //
//        //        }
//        
//        if elementValue != nil{
//            
//            
//            //print("String", string)
//            if (element == "ID" || element == "BRANCH"){
//                if (string.rangeOfString("\n") == nil){
//                    sample[element!] = string
//                }
//                
//            }
//            else if (element == "PRODUCT_CODE" || element == "PRODUCT"){
//                if (string.rangeOfString("\n") == nil){
//                    count++
//                    sampleProduct[element!] = string
//                }
//            }
//            
//            
//        }
//        
//    }
//    
//    
//    
//    
//    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
//        
//        if elementName == "BRANCH" {
//            elementArray.addObject(sample)
//        }
//        else if (elementName == "PRODUCT") {
//            
//            elementProduct.addObject(sampleProduct)
//        }
//        
//    }
//    
    
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
              print("Finyeararray",self.finyearArray)
            dispatch_async(dispatch_get_main_queue(), {
               // self.performSegueWithIdentifier("toSalesPerfCustomer", sender: self)
                HUD.hideUIBlockingIndicator()
            })
          
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getManfactList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_MANUFACTURE?MANUFACTURE="
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
                        //self.customerTable.reloadData()
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
            elementValue = String()
            element = elementName
            finyear[elementName] = ""
        }
        if (elementName == "PR_MFG_ID" || elementName == "MFG_NAME"){
            noData = false
            elementValue = String()
            element = elementName
            manfact[elementName] = ""
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if (element == "FIN_YEAR_ID" || element == "FINYEAR") {
            if (string.rangeOfString("\n") == nil){
                finyear[element!] = string
            }
            
        }
        if (element == "PR_MFG_ID" || element == "MFG_NAME"){
            if string.rangeOfString("\n") == nil{
                manfact[element!] = string
            }
        }

    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "FINYEAR" {
            finyearArray.addObject(finyear)
        }
        if elementName == "MFG_NAME" {
            manfactArray.addObject(manfact)
        }
    }
    
    
    @IBOutlet var menuBtn: UIButton!
    @IBAction func menuBtnPressed(sender: AnyObject) {
        self.onSlideMenuButtonPressed(menuBtn)
    }

    
}
