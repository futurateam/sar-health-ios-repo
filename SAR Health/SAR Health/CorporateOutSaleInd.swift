//
//  CorporateOutSaleInd.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSaleInd: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var salesTable: UITableView!
    @IBOutlet weak var totel: UILabel!
    @IBOutlet var qtyTot: UILabel!
    
    
    var invNo = ["No"], invDate = ["Inv Date"], customer = [String](), days = ["Days"], sales = ["Amount (₹)"], batch = ["Batch No"], qtyText = ["Qty"], product = ["Product"]
    var num = NSNumberFormatter(), tot : Double = 0, qtyTotal = 0
    var outstandingInd : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        print(outstandingInd)
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for (var i = 0 ; i < outstandingInd.count ; i += 1){
            //invNo.append(outstandingInd[i].valueForKey("INV_NO") as! String)
            //invDate.append(outstandingInd[i].valueForKey("INV_DATE") as! String)
            invNo.append(String(i+1))
            customer.append(outstandingInd[i].valueForKey("CUST_NAME") as! String)
            //batch.append(outstandingInd[i].valueForKey("BATCH_NO") as! String)
            product.append(outstandingInd[i].valueForKey("PRODUCT") as! String)
            qtyText.append(outstandingInd[i].valueForKey("QTY") as! String)
            sales.append(num.stringFromNumber(Double((outstandingInd[i].valueForKey("SALE") as? String)!)!)!)
            tot = tot + Double((outstandingInd[i].valueForKey("SALE") as? String)!)!
            qtyTotal = qtyTotal + Int((outstandingInd[i].valueForKey("QTY") as! String))!
            
        }
        totel.text = num.stringFromNumber(tot)
        qtyTot.text = String(qtyTotal)
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invNo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("CorporateOutIndCell", forIndexPath: indexPath) as! CorporateOutSalesIndCell
       
        if (indexPath.row == 0){
           
            cell.no.attributedText = headFont(invNo[indexPath.row])
            //cell.invDate.attributedText = headFont(invDate[indexPath.row])
            cell.qty.attributedText = headFont(qtyText[indexPath.row])
            //cell.customerName.attributedText = headFont(customer[indexPath.row])
            //cell.batchNo.attributedText = headFont(batch[indexPath.row])
            cell.sales.attributedText = headFont(sales[indexPath.row])
            cell.product.attributedText = headFont(product[indexPath.row])
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
           cell.product.text = product[indexPath.row]
            cell.no.text = invNo[indexPath.row]
           // cell.invDate.text = invDate[indexPath.row]
            //cell.customerName.text = customer[indexPath.row]
           // cell.batchNo.text = batch[indexPath.row]
            cell.sales.text = sales[indexPath.row]
            cell.qty.text = qtyText[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
    }
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    

}
