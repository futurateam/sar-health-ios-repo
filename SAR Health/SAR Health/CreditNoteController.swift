//
//  CreditNoteController.swift
//  SAR Health
//
//  Created by Anargha K on 24/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CreditNoteController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
  
    
    
    
    @IBOutlet weak var creditNoteTable: UITableView!
    @IBOutlet var totalText: UILabel!
    
    var noDataArray : NSMutableArray = [], noDataDic = [String :String]()
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var branchDetArray : NSMutableArray = []
    var creditNoteDet = [String : String](), branchCode = "", manCode = "", creditNoteDetArray : NSMutableArray = [], mfgArray : NSMutableArray = []
    var txn = "", finyearArray : NSMutableArray = [], finyear = [String : String]()
    var creditNote : NSMutableArray = [], manfactName = "", amountArray = [Double](), qtyTot : Double = 0, brn = [String]()
    var no = ["No"]
    var branch = ["Branch"]
    var customer = ["Customer"]
    var custName = ""
    var amount = ["Amount(₹)"]
    var billNo = ["Bill No"]
    var branchId = ["Branch ID"], txnNo = ["Txn No"]
    var fromDate = ""
    var toDate = "", branchName = "", count = 0, limit = 0, sum : Double = 0, i = 0, j = 0
    var total : Double = 0
    var custArray = [String]()
    var num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        getFinyear()
         for (var i = 0; i < creditNote.count; i += 1){
         no.append(String(i+1))
         branch.append((creditNote[i].valueForKey("BRANCH") as? String)!)
         customer.append((creditNote[i].valueForKey("CUSTOMER") as? String)!)
         billNo.append((creditNote[i].valueForKey("Bill_No") as? String)!)
         amount.append((creditNote[i].valueForKey("AMOUNT") as? String)!)
            branchId.append((creditNote[i].valueForKey("BRN_ID") as? String)!)
            txnNo.append((creditNote[i].valueForKey("Txn_Id") as? String)!)
         }
         //self.creditNoteTable.tableFooterView = UIView.init(frame: CGRectZero)
         for(var  i = 0; i < creditNote.count; i += 1){
         total = total + Double((creditNote[i].valueForKey("AMOUNT") as? String)!)!
         
         }
        
        
        
       
       
        totalText.text = num.stringFromNumber(total)
        // qtyText.text = String(qtyTot)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
      /*  if segue.identifier == "toGraphCreditNote" {
            custArray.removeAll()
            amountArray .removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Customer Name"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Customer Name"
            if manfactName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = custName
            }
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Credit Note"
            for(var  i = 0; i < creditNote.count; i += 1){
                custArray.append((creditNote[i].valueForKey("CUSTOMER") as? String)!)
                amountArray.append(Double((creditNote[i].valueForKey("AMOUNT") as? String)!)!)
            }
            dest.chartData = amountArray
            dest.chartLegend = custArray
            
        }*/
        if segue.identifier == "toPieOutstandingSumm"{
            custArray.removeAll()
            amountArray .removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Credit Note"
            for(var  i = 0; i < creditNote.count; i += 1){
                custArray.append((creditNote[i].valueForKey("CUSTOMER") as? String)!)
                amountArray.append(Double((creditNote[i].valueForKey("AMOUNT") as? String)!)!)
            }
            dest.chartData = amountArray
            dest.chartLegend = custArray
        }

       /* if segue.identifier == "toSalesPerfBranchDet" {
            let dest = segue.destinationViewController as? SalesPerfBranchDet
            dest?.branchArray = branchDetArray
            dest?.branchName = branchName
            dest?.fromDate = fromDate
            dest?.toDate = toDate
            dest?.manfactName = manfactName
            dest?.branchCode = branchCode
            
            
        }*/
        if segue.identifier == "toCreditNoteDet"{
        let dest = segue.destinationViewController as! CreditNoteDet
        dest.creditNote = creditNoteDetArray
            dest.custName = custName
        
        }
        
    }
    
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = creditNoteTable.dequeueReusableCellWithIdentifier("CreditNoteCell", forIndexPath: indexPath) as! CreditNoteTableCell
        
        
        if no[indexPath.row] == "No" {
            
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            //            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            //
            //            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            //
            //            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            //            cell.branch.attributedText = attributedString
             longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
             attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.branch.attributedText = attributedString
            longestWordRange = (customer[indexPath.row] as NSString).rangeOfString(customer[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: customer[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.customer.attributedText = attributedString
            
            longestWordRange = (billNo[indexPath.row] as NSString).rangeOfString(billNo[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: billNo[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.billNo.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.amount.attributedText = attributedString
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            
            
        }
        else {
              cell.no.text = no[indexPath.row]
            cell.branch.text = branch[indexPath.row]
            cell.customer.text = customer[indexPath.row]
           
          
            cell.amount.text = amount[indexPath.row]
             cell.billNo.text = billNo[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 0){
        }
        else {
            branchName = branch[indexPath.row]
            
            for(var i = 0; i < creditNote.count; i++){
                
                if  (branch[indexPath.row] == creditNote[i].valueForKey("BRANCH") as? String){
                    branchCode = (creditNote[i].valueForKey("BRN_ID") as? String)!
                }
            }
            custName = customer[indexPath.row]
            txn = txnNo[indexPath.row]
            creditNoteDetArray.removeAllObjects()
            branchDetArray.removeAllObjects()
            getCreditNoteDet()
            
            
        }
        
    }
    
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    func getCreditNoteDet()
    {
        
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
           let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
               //
        let scriptUrl1 = Constants().API + "MOBAPP_SALE_RETURN_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&FINYEAR=" + finyearCode + "&TXN_NO=" + txn
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            print("Credit Array",self.creditNoteDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                
                HUD.hideUIBlockingIndicator()
                self.performSegueWithIdentifier("toCreditNoteDet", sender: self)
                
                
                
                
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                if elementName == "Column1" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    noDataDic[elementName] = ""
                }
            }
            if (elementName == "Product" || elementName == "Batch_No" || elementName == "Return_Qty" ||  elementName == "Amount"){
                noData = false
                elementValue = String()
                element = elementName
                creditNoteDet[elementName] = ""
                
            }
            
            if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
                noData = false
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
           
        }
        
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "Product" || element == "Batch_No" || element == "Return_Qty" || element == "Amount") {
                if (string.rangeOfString("\n") == nil){
                    //                    if element  == "MANUFACTURER"{
                    //                        if countBranchDet == 0{
                    //                            branchDet[element!] = string
                    //                            countBranchDet += 1
                    //                        }
                    //                    }
                    //                    else {
                   creditNoteDet[element!] = string
                    //}
                }
            }
            if (element == "FIN_YEAR_ID" || element == "FINYEAR") {
                if (string.rangeOfString("\n") == nil){
                    finyear[element!] = string
                }
                
            }
                       if (element == "Table1"){
                if element == "Column1" {
                    noDataDic[element!] = string
                }
            }
        }
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Amount" {
            
            creditNoteDetArray.addObject(creditNoteDet)
            
            
        }
        if elementName == "FINYEAR" {
            finyearArray.addObject(finyear)
        }
        

        if elementName == "Table1" {
            if elementName == "Column1" {
                noDataArray.addObject(noDataDic)
            }
        }
    }
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        performSegueWithIdentifier("toPieOutstandingSumm", sender: self)
    }
    
    
    

}
