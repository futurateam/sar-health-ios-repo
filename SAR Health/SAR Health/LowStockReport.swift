//
//  LowStockReport.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class LowStockReport: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var lowStockTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var stocktext: UILabel!
    @IBOutlet var stock: UILabel!
    
    var total : Int = 0, branchName = "", branchCode = ""
    var lowStockValue : NSMutableArray = []
    var branch = ["Branch"], product = ["Product"], stockValue = ["Stock"], stockArray = [Double](), productArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        for (var i = 0; i < lowStockValue.count; i += 1){
            branch.append((lowStockValue[i].valueForKey("branch") as? String)!)
            product.append((lowStockValue[i].valueForKey("product") as? String)!)
            stockValue.append((lowStockValue[i].valueForKey("stock") as? String)!)
            total = total + Int((lowStockValue[i].valueForKey("stock") as? String)!)!
        }
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        totalText.text = String(total)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        if branchCode == "0" {
            stocktext.text = "Total stock in all branches is"
        }
        else {
             stocktext.text = "Total stock in " + branchName + " is"
        }
       stock.text = String(total)
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if segue.identifier == "toGraphLowStock" {
            stockArray.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            for (var i = 1; i < stockValue.count ; i += 1) {
                stockArray.append(Double(stockValue[i])!)
                productArray.append(product[i])
            }
            print("Chart Data",stockArray)
            dest.chartData = stockArray
            dest.xaxisName = "Product"
            dest.yaxisName = "Stock"
            dest.chartLegend = product
            dest.titleTab = "Low Stock Report"

        }
        if segue.identifier == "toPieLowStock"{
            stockArray.removeAll()
            productArray.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            for (var i = 1; i < stockValue.count ; i += 1) {
                stockArray.append(Double(stockValue[i])!)
                productArray.append(product[i])
            }
            dest.chartData = stockArray
            dest.fromDateStr = ""
            dest.toDateStr = ""
            dest.to = ""
            dest.chartLegend = productArray
            dest.titleTab = "Low Stock Report"
        }
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branch.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = lowStockTable.dequeueReusableCellWithIdentifier("LowStockReportCell", forIndexPath: indexPath) as! LowStockReportCell
        
       
                if indexPath.row == 0 {
            var longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.branch.attributedText = attributedString
             longestWordRange = (product[indexPath.row] as NSString).rangeOfString(product[indexPath.row])
            
             attributedString = NSMutableAttributedString(string: product[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.product.attributedText = attributedString
            longestWordRange = (stockValue[indexPath.row] as NSString).rangeOfString(stockValue[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: stockValue[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.stock.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
                    cell.stock.text = stockValue[indexPath.row]

             cell.product.text = product[indexPath.row]
           cell.branch.text = branch [indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        

        return cell
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func viewGraphPressed(sender: AnyObject) {
        if branchName == "All"{
            performSegueWithIdentifier("toGraphLowStock", sender: self)
        }
        else {
            performSegueWithIdentifier("toPieLowStock", sender: self)
        }
        
    }
    
    
}
