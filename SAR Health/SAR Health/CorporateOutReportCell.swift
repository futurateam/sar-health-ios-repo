//
//  CorporateOutReportCell.swift
//  SAR Health
//
//  Created by Anargha K on 17/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutReportCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    @IBOutlet var customer: UILabel!
    @IBOutlet var outstanding: UILabel!
    @IBOutlet var branch: UILabel!
    @IBOutlet var sales: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
