//
//  GraphViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 02/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit
//import ChartView

class GraphViewController: UIViewController, JBBarChartViewDataSource, JBBarChartViewDelegate {
    @IBOutlet var barChart: JBBarChartView!

    
    @IBOutlet var scroller: UIScrollView!
    @IBOutlet var xaxis: UILabel!
    @IBOutlet var yaxis: UILabel!
    @IBOutlet var cusProd: UILabel!
    @IBOutlet var cusProdName: UILabel!
    @IBOutlet var fromDate: UILabel!
    @IBOutlet var toText: UILabel!
    @IBOutlet var toDate: UILabel!
    @IBOutlet var tabTitle: UILabel!
    @IBOutlet var xaxisText: UILabel!
    @IBOutlet var xaxisValue: UILabel!
    @IBOutlet var yaxisText: UILabel!
    @IBOutlet var yaxisValue: UILabel!
    
    
    
    var xaxisName = "", yaxisName = "", from = "" , to = "", cusprodText = "", cusProdValue = "", titleTab = ""
    var chartLegend = [String]()
    var chartData = [Double]()
    var charDataArray : NSMutableArray = []
    var i = 1
    //var chartLegend = ["11-14", "11-15", "11-16", "11-17","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14","11-14"]
   // var chartData = [70, 100, 90, 50, 50, 50 ,50 ,50, 50, 50, 50, 50,50,50,90,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //        barChart.dataSource = self.view
        barChart.delegate = self
        barChart.dataSource = self
        barChart.minimumValue = 0
        barChart.maximumValue = 100
        
        
        fromDate.text = from
        toDate.text = to
        tabTitle.text = titleTab
//        let frame = barChart.frame.width
//        scroller.contentSize = CGSizeMake(frame, scroller.frame.height)
        barChart.reloadData()
        xaxisValue.hidden = true
        xaxisText.hidden = true
        yaxisText.hidden = true
        yaxisValue.hidden = true
        barChart.setState(.Collapsed, animated: true)
        if fromDate.text == "" {
            fromDate.hidden = true
            toDate.hidden = true
            toText.hidden = true
        }
        print("Chart Legend",chartLegend)
        print("Chart Data",chartData)
        print("Chart Legend",chartLegend.count)
        print("Chart Data",chartData.count)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        let footerView = UIView(frame: CGRectMake(0, 0, barChart.frame.width, 16))
//        
//        print("viewDidLoad: \(barChart.frame.width)")
//        
//        let footer1 = UILabel(frame: CGRectMake(0, 0, barChart.frame.width/2 - 8, 16))
//        footer1.textColor = UIColor.blackColor()
//        footer1.text = "\(chartLegend[0])"
//        
//        let footer2 = UILabel(frame: CGRectMake(barChart.frame.width/2 - 8, 0, barChart.frame.width/2 - 8, 16))
//        footer2.textColor = UIColor.blackColor()
//        footer2.text = "\(chartLegend[chartLegend.count - 1])"
//        footer2.textAlignment = NSTextAlignment.Right
//        
//        footerView.addSubview(footer1)
//        footerView.addSubview(footer2)
//        
//        let header = UILabel(frame: CGRectMake(0, 0, barChart.frame.width, 50))
//        header.textColor = UIColor.blackColor()
//        header.font = UIFont.systemFontOfSize(24)
//        header.text = "Weather: San Jose, CA"
//        header.textAlignment = NSTextAlignment.Center
//        let frame = barChart.frame.width
//        scroller.contentSize = CGSizeMake(frame, scroller.frame.height)
//        barChart.footerView = footerView
//        barChart.headerView = header
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // our code
        barChart.reloadData()
        let frame = barChart.frame.width
        scroller.contentSize = CGSizeMake(frame, scroller.frame.height)
        _ = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("showChart"), userInfo: nil, repeats: false)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        hideChart()
    }
    
    func hideChart() {
        barChart.setState(.Collapsed, animated: true)
    }
    
    func showChart() {
        let frameWidth = barChart.frame.width
        //let frameHeight = barChart.frame.height
        scroller.contentSize = CGSizeMake(frameWidth, scroller.frame.height)
        barChart.setState(.Expanded, animated: true)
    }
    
    // MARK: JBBarChartView
    
    func numberOfBarsInBarChartView(barChartView: JBBarChartView!) -> UInt {
        return UInt(chartData.count)
    }
    
    func barChartView(barChartView: JBBarChartView!, heightForBarViewAtIndex index: UInt) -> CGFloat {
        if CGFloat(chartData[Int(index)]) < 0 {
            return abs(CGFloat(chartData[Int(index)]))
        }
        else {
            return CGFloat(chartData[Int(index)])
        }
    }
    
    func barChartView(barChartView: JBBarChartView!, colorForBarViewAtIndex index: UInt) -> UIColor! {
        
        if (i == 1){
            i += 1
             return UIColor(red: 193.0/255.0 , green: 37.0/255.0, blue: 82.0/255.0, alpha: 1.0)
        }
        else if (i == 2){
            i += 1
            return UIColor(red: 255.0/255.0 , green: 102.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
        else if (i == 3){
            i += 1
            return UIColor(red: 245.0/255.0 , green: 199.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
        else if i == 4 {
            i += 1
            return UIColor(red: 106.0/255.0 , green: 150.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        }
        else if i == 5 {
            i += 1
            return UIColor(red: 179.0/255.0 , green: 100.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        }
        else if i == 6 {
            i += 1
            return UIColor(red: 192.0/255.0 , green: 155.0/255.0, blue: 140.0/255.0, alpha: 1.0)
        }
        else if i == 7 {
           i += 1
            return UIColor(red: 193.0/255.0 , green: 37.0/255.0, blue: 82.0/255.0, alpha: 1.0)
        }
        else if i == 8 {
            i += 1
            return UIColor(red: 255.0/255.0 , green: 208.0/255.0, blue: 140.0/255.0, alpha: 1.0)
        }
        else if i == 9 {
            i += 1
            return UIColor(red: 140.0/255.0 , green: 234.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }else if i == 10 {
            
            i = 1
            return UIColor(red: 255.0/255.0 , green: 140.0/255.0, blue: 157.0/255.0, alpha: 1.0)        }
        else {
            i += 1
            return UIColor.redColor()
        }
        
        
        //return (index % 2 == 0) ? UIColor.redColor() : UIColor.blackColor()
    }
    
    func barChartView(barChartView: JBBarChartView!, didSelectBarAtIndex index: UInt) {
                let data = chartData[Int(index)]
               let key = chartLegend[Int(index)]
        //
                xaxisText.text = xaxisName
                yaxisText.text = yaxisName
                xaxisValue.text = key
                yaxisValue.text = String(data)
        xaxisText.hidden = false
        yaxisText.hidden = false
        xaxisValue.hidden = false
        yaxisValue.hidden = false
    }
    
    //    func didDeselectBarChartView(barChartView: JBBarChartView!) {
    //        infoLabel.text = ""
    //    }

    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
