//
//  ProductExpiryBranch.swift
//  SAR Health
//
//  Created by Prajeesh KK on 18/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductExpiryBranch: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet var productExpiryText: UILabel!    
    @IBOutlet var productExpiryTable: UITableView!
    
    var branchName = ""
    var productName = ""
//    var batch = [String]()
//    var quantity = [String]()
//    var date = [String]()
    
    var productExpiry : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.productExpiryTable.tableFooterView = UIView.init(frame: CGRectZero)
      
       productExpiryText.text = "Product Expiry of " + productName + " in " + branchName
//        batch = ["Batch 1", "Batch 2", "Batch 3", "Batch 4", "Batch 5", "Batch 6"]
//        quantity = ["10", "10", "10", "10", "10", "10"]
//        date = ["02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015", "02 Mar 2015"]
        for (var i=0; i < productExpiry.count; i++ ) {
            for (var j=(i+1); j < productExpiry.count; j++) {
                if ((productExpiry[i].valueForKey("BATCH") as? String == productExpiry[j].valueForKey("BATCH") as? String) && (productExpiry[i].valueForKey("BRANCH") as? String == productExpiry[j].valueForKey("BRANCH") as? String) && (productExpiry[i].valueForKey("EXPIRY") as? String == productExpiry[j].valueForKey("EXPIRY") as? String) && (productExpiry[i].valueForKey("STOCK") as? String == productExpiry[j].valueForKey("STOCK") as? String)) {
                        productExpiry.removeObjectAtIndex(i);
                        j--;
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    //Mark: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productExpiry.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = productExpiryTable.dequeueReusableCellWithIdentifier("ProductExpiryDataCell", forIndexPath: indexPath) as! ProductExpiryBranchCell
        cell.batchNo.text = productExpiry[indexPath.row].valueForKey("BATCH") as? String
        cell.quantity.text = productExpiry[indexPath.row].valueForKey("STOCK") as? String
        cell.date.text = productExpiry[indexPath.row].valueForKey("EXPIRY") as? String
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = productExpiryTable.dequeueReusableCellWithIdentifier("HeaderCell") as! ProductExpiryBranchHeaderCell
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func checkAnother(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
