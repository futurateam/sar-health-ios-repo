//
//  SalesPerfItemInd.swift
//  SAR Health
//
//  Created by Prajeesh KK on 09/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfItemInd: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var salesTable: UITableView!
    @IBOutlet weak var totel: UILabel!
    @IBOutlet var qty: UILabel!
    
    
    var invNo = ["Inv No"], invDate = ["Inv Date"], customer = ["Customer"], batch = ["Batch"], quantity = ["Qty"], value = ["Amount(₹)"]
    var num = NSNumberFormatter(), tot : Double = 0, qtyTot : Int = 0
    var salesDet : NSMutableArray = [], custName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for (var i = 0 ; i < salesDet.count ; i += 1){
            invNo.append(salesDet[i].valueForKey("INV_NO") as! String)
            invDate.append(salesDet[i].valueForKey("INV_DATE") as! String)
            customer.append(salesDet[i].valueForKey("CUST_NAME") as! String)
            batch.append(salesDet[i].valueForKey("Batch") as! String)
            quantity.append(salesDet[i].valueForKey("Qty") as! String)
            value.append(num.stringFromNumber(Double((salesDet[i].valueForKey("VALUE") as? String)!)!)!)
            qtyTot = qtyTot + Int((salesDet[i].valueForKey("Qty") as! String))!
            tot = tot + Double((salesDet[i].valueForKey("VALUE") as? String)!)!
            
           
        }
        totel.text = num.stringFromNumber(tot)
        qty.text = String(qtyTot)
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invNo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalesItemIndCell", forIndexPath: indexPath) as! SalesPerfItemIndCell
        
        if(invNo[indexPath.row] == "Inv No"){
            let longestWordRange = (custName as NSString).rangeOfString(custName)
            
            let attributedString = NSMutableAttributedString(string:custName as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.custName.backgroundColor = UIColor.whiteColor()
            cell.custName.attributedText = attributedString
            cell.invNo.attributedText = headFont(invNo[indexPath.row])
            cell.invDate.attributedText = headFont(invDate[indexPath.row])
            //cell.customerName.attributedText = headFont(customer[indexPath.row])
            cell.batch.attributedText = headFont(batch[indexPath.row])
            cell.quantity.attributedText = headFont(quantity[indexPath.row])
            cell.amount.attributedText = headFont(value[indexPath.row])
            
                       cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.custName.text = ""
            cell.invNo.text = invNo[indexPath.row]
            cell.invDate.text = invDate[indexPath.row]
           // cell.customerName.text = customer[indexPath.row]
            cell.batch.text = batch[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = value[indexPath.row]
            
            cell.backgroundColor = UIColor.whiteColor()
        }

        return cell
        
    }

    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}
