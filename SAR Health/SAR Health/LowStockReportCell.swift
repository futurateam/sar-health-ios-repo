//
//  LowStockReportCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class LowStockReportCell: UITableViewCell {

    @IBOutlet var branch: UILabel!
    @IBOutlet var product: UILabel!
    @IBOutlet var stock: UILabel!
}
