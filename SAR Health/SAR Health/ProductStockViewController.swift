//
//  ProductStockViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 08/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class ProductStockViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate{

    @IBOutlet var branchTable: UITableView!
    @IBOutlet var stockResultText: UILabel!
    @IBOutlet var stockText: UILabel!
    
    @IBOutlet var totText: UILabel!
    var longString : NSString = "", longStringHead : NSString = "", branchLabel = ""
    var stock = 0, finyear = [String:String](), finyearArray : NSMutableArray = [], branchCode = "", productCode = "", branchArray : NSMutableArray = []
    var stringNumber = "1234", productDet = [String : String](), productDetArray : NSMutableArray = [], productStockBranch : NSMutableArray = []
    var numberFromString = 0
    var stockTotal : NSInteger = 0
    var productName = ""
    var productStock : NSMutableArray = []
    var elementValue: String?
    var element : String?
var branch = [String](), quantity = [String]()
//    var branch = [String]()
//    var result = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.hideUIBlockingIndicator()
       
        getFinyear()
//        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6"];
//        result = ["100", "250", "50", "40", "160", "140"];
        if branchLabel == "All" {
            branch.append("Branch")
            quantity.append("Quantity")
            for (var i = 0; i < productStock.count; i++){
                
                branch.append((productStock[i].valueForKey("BRANCH") as? String)!)
                quantity.append((productStock[i].valueForKey("STOCK") as? String)!)
                stockTotal = stockTotal + Int((productStock[i].valueForKey("STOCK") as? String)!)!
            }
            
            longString = productName + " has " + String(stockTotal) + " numbers in stock"
            self.branchTable.tableFooterView = UIView.init(frame: CGRectZero)
            let longestWord = String(stockTotal)
            
            let longestWordRange = (longString as NSString).rangeOfString(longestWord)
            
            let attributedString = NSMutableAttributedString(string: longString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(14)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            
            stockText.hidden = false
            stockResultText.hidden = false

            stockText.attributedText = attributedString
            stockResultText.text = "Stock Result of " + productName + " in All Branches"

        }
        
        else {
            branch.append("Batch")
            quantity.append("Stock")
            for (var i = 0; i < productStockBranch.count; i++){
                
                branch.append((productStockBranch[i].valueForKey("batch") as? String)!)
                quantity.append((productStockBranch[i].valueForKey("stock") as? String)!)
                stockTotal = stockTotal + Int((productStockBranch[i].valueForKey("stock") as? String)!)!
            }
            stockText.hidden = true
            stockResultText.hidden = true
           totText.text = String(stockTotal)

        }
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toProductStockDet" {
            let dest = segue.destinationViewController as! ProductStockDet
            dest.productStock = productDetArray
        }
    }
    
    
    //Mark: - Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branch.count;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! BranchTableViewCell
        
        
       
        if(indexPath.row == 0){
           
            
            var longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.branchLabel.attributedText = attributedString
            longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
             attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(14), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.resultLabel.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.resultLabel.text = quantity[indexPath.row]
            cell.branchLabel.text = branch[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        return cell;
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if branchLabel == "All" {
        for (var i = 0 ; i < branchArray.count ; i++) {
            if  (branchArray[i].valueForKey("BRN_NAME") as? String == branch[indexPath.row]) {
                branchCode = (branchArray[i].valueForKey("BRN_ID") as? String!)!
            }
        }
        productDetArray.removeAllObjects()
        getDataProductAll()
        }
        
    }
    
    
    func getDataProductAll ()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCTSTOCK?BRANCH="+branchCode+"&PRODUCT="+prodCode+"&FINYEAR=7"
        let scriptUrl1 = Constants().API + "MOBAPP_PRODUCTSTOCK_BATCHWISE?BRANCH=" + branchCode + "&PRODUCT=" + productCode + "&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            //            if (data!.length == 38){
            //                self.check = false
            //            }
            //            else{
            //                self.check = true
            //            }
            print("data=\(data?.length)")
            let parserProduct = NSXMLParser(data: data!)
            parserProduct.delegate = self
            parserProduct.parse()
            print("Product Stock", self.productDetArray)
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                self.performSegueWithIdentifier("toProductStockDet", sender: self)
                                   
            })
            
            
            
        }
        
        task.resume()
        
        
    }
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }

    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
               
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
            if (elementName == "batch" || elementName == "stock") {
                
                elementValue = String()
                element = elementName
                productDet[elementName] = ""
            }

        }
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "FIN_YEAR_ID" || element == "FINYEAR") {
                if (string.rangeOfString("\n") == nil){
                    finyear[element!] = string
                }
                
            }
            if (element == "batch" || element == "stock") {
                if (string.rangeOfString("\n") == nil){
                    productDet[element!] = string
                }
                
            }
            
        }
        
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
               if elementName == "FINYEAR" {
            finyearArray.addObject(finyear)
        }
        if elementName == "stock" {
            productDetArray.addObject(productDet)
        }

        
    }

    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func checkAnotherPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
