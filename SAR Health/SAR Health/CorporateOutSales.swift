//
//  CorporateOutSales.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSales: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
    // @IBOutlet weak var totalText: UILabel!
    @IBOutlet var outstandingTable: UITableView!
    @IBOutlet var outstandingText: UILabel!
    @IBOutlet var amountText: UILabel!
   
    @IBOutlet var TotalQtyTable: UITableView!
   
  
    @IBOutlet var outstandingTableHeight: NSLayoutConstraint!
    @IBOutlet var totalTableHeight: NSLayoutConstraint!
    
    
    @IBOutlet var totalQty: UILabel!
    @IBOutlet var totalAmount: UILabel!
    
    var elementValue: String?
    var element : String?
    var sum : Double = 0, manfactName = "", noData : Bool = true
    var corpDet : NSMutableArray = [], branchCode = "", prodCode = "", manCode = "", outstandingInd = [String : String](), outstandingIndArray : NSMutableArray = [], customerArray : NSMutableArray = [], custCode = "", outstandingCust = "", manTitle = ["Manufacture"], totalArray = ["Sales (₹)"], totalVal : Double = 0, manValue = [0.0], qtyArray = ["Qty"], qtyInt = [0], noTot = ["No"], corpTotal : NSMutableArray = [], totalQuantity = 0 ,  totalAmt : Double = 0.0
    var no = ["No"], valueC = 0
    var branch = ["Branch"]
    var manufacture = ["Manufacture"]
    var qtyText = ["Qty"], custId = ["Cust Id"], prodId = ["Prod Id"], branchId = ["Branch Id"], mfgId = ["Mfg Id"], brn = [String]()
    var fromDate = "" , toDate = "", corporateTot = [String : String](), corporateTotArray : NSMutableArray = []
    var value = ["Sales (₹)"]
    var customerLegend = [String](), amount = [Double]()
    var  i = 0, qtyTotal : Int = 0
    var j = 0
    var count = 0
    var limit = 1
    var total : Double = 0
    var customerName = "", num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
       
       /* for (i = 0 ; i < corpDet.count ; i++) {
            no.append(String(i + 1))
            //branch.append(corpDet[i].valueForKey("brn_name") as! String)
            manufacture.append(corpDet[i].valueForKey("MANUFATURE") as! String)
            qtyText.append(corpDet[i].valueForKey("Qty") as! String)
            value.append(num.stringFromNumber(Double((corpDet[i].valueForKey("AMOUNT") as? String)!)!)!)
            custId.append(corpDet[i].valueForKey("SO_CUST_CODE") as! String)
            //prodId.append(corpDet[i].valueForKey("PR_CAT_ID") as! String)
            branchId.append(corpDet[i].valueForKey("brn_id") as! String)
            mfgId.append(corpDet[i].valueForKey("PR_MFG_ID") as! String)
            total = total + Double((corpDet[i].valueForKey("AMOUNT") as? String)!)!
            qtyTotal = qtyTotal + Int((corpDet[i].valueForKey("Qty") as! String))!
            
        }*/
        
        
        for ( i = 0; i < corpDet.count ; i += 1){
            total = 0
            qtyTotal = 0
            print("i: ",i)
            print("Count", count)
            count = i
            limit = 0
            //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
            
            
            let customer = corpDet[i].valueForKey("CUST_NAME") as? String
            //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
            //            prName.append("Product")
            if (i == 0 ){
                brn.append(customer!)
                //branchAllArray.append(branch!)
                for ( j = i ; j < corpDet.count ; j += 1){
                    
                    if corpDet[j].valueForKey("CUST_NAME") as? String == customer{
                        //branchAllArray.append(branch!)
                        brn.append("")
                        
                        //branch.append(corpDet[i].valueForKey("brn_name") as! String)
                        manufacture.append(corpDet[j].valueForKey("MANUFATURE") as! String)
                        qtyText.append(corpDet[j].valueForKey("Qty") as! String)
                        qtyInt.append(Int((corpDet[j].valueForKey("Qty") as! String))!)
                        value.append(num.stringFromNumber(Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!)!)
                         manValue.append(Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!)
                        custId.append(corpDet[j].valueForKey("SO_CUST_CODE") as! String)
                        //prodId.append(corpDet[i].valueForKey("PR_CAT_ID") as! String)
                        branchId.append(corpDet[j].valueForKey("brn_id") as! String)
                        mfgId.append(corpDet[j].valueForKey("PR_MFG_ID") as! String)
                        total = total + Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!
                        qtyTotal = qtyTotal + Int((corpDet[j].valueForKey("Qty") as! String))!
                        limit++
                        no.append(String(limit))
                        //count += 1
                        
                    }
                    
                }
                let tot =  num.stringFromNumber(total)!
                // let qty = "Total Qty = " + String(qtyTot)
                value.append("Total = "+tot)
                manValue.append(0)
               // totalData.append(total)
                sum =  total
                //brn.append("")
                //branchAllArray.append("")
                brn.append("")
                //no.append("")
                no.append("")
                no.append("No")
                //customer.append("")
                manufacture.append("")
                manufacture.append("Manufacture")
                custId.append("")
                custId.append("Cust Id")
                branchId.append("")
                branchId.append("Branch id")
                mfgId.append("")
                mfgId.append("Mfg Id")
                // billDate.append("")
                qtyText.append("Total = " + String(qtyTotal))
                qtyText.append("Qty")
                qtyInt.append(0)
                qtyInt.append(0)
                //billNo.append("")
                //value.append("")
                // days.append("")
                value.append("Sales (₹)")
                manValue.append(0)
                //value.append("")
               // days.append("Days")
                // i = count
                
            }else {
                
                if brn.contains(customer!){
                    
                }
                else {
                    brn.append(customer!)
                    // branchAllArray.append(branch!)
                    for ( j = 0 ; j < corpDet.count ; j += 1){
                        
                        if corpDet[j].valueForKey("CUST_NAME") as? String == customer{
                            brn.append("")
                            manufacture.append(corpDet[j].valueForKey("MANUFATURE") as! String)
                            qtyText.append(corpDet[j].valueForKey("Qty") as! String)
                            qtyInt.append(Int((corpDet[j].valueForKey("Qty") as! String))!)
                            value.append(num.stringFromNumber(Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!)!)
                            manValue.append(Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!)
                            custId.append(corpDet[j].valueForKey("SO_CUST_CODE") as! String)
                            //prodId.append(corpDet[i].valueForKey("PR_CAT_ID") as! String)
                            branchId.append(corpDet[j].valueForKey("brn_id") as! String)
                            mfgId.append(corpDet[j].valueForKey("PR_MFG_ID") as! String)
                            total = total + Double((corpDet[j].valueForKey("AMOUNT") as? String)!)!
                            qtyTotal = qtyTotal + Int((corpDet[j].valueForKey("Qty") as! String))!
                            limit++
                            no.append(String(limit))
                            //count += 1
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                    // let qty = "Total Qty = " + String(qtyTot)
                    value.append("Total = "+tot)
                    manValue.append(0)
                    // totalData.append(total)
                    sum =  sum + total
                    //brn.append("")
                    //branchAllArray.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    manufacture.append("")
                    manufacture.append("Manufacture")
                    custId.append("")
                    custId.append("Cust Id")
                    branchId.append("")
                    branchId.append("Branch id")
                    mfgId.append("")
                    mfgId.append("Mfg Id")
                    // billDate.append("")
                   qtyText.append("Total = " + String(qtyTotal))
                    qtyText.append("Qty")
                    qtyInt.append(0)
                    qtyInt.append(0)
                    //billNo.append("")
                    //value.append("")
                    // days.append("")
                    value.append("Sales (₹)")
                    manValue.append(0)
                    //value.append("")
                    // days.append("Days")
                    // i = count
                    
                }
            }
            
            
        }
        // no.removeAtIndex(no.count - 1)
        no.removeAtIndex(no.count - 1)
        //  brn.removeAtIndex(brn.count - 1)
        // customer.removeAtIndex(customer.count - 1)
        manufacture.removeAtIndex(manufacture.count - 1)
        custId.removeAtIndex(custId.count - 1)
        branchId.removeAtIndex(branchId.count - 1)
        mfgId.removeAtIndex(mfgId.count - 1)
        //billDate.removeAtIndex(billDate.count - 1)
        // billNo.removeAtIndex(billNo.count - 1)
        
        // days.removeAtIndex(days.count - 1)
        qtyText.removeAtIndex(qtyText.count - 1)
        qtyInt.removeAtIndex(qtyInt.count - 1)
        value.removeAtIndex(value.count - 1)
        manValue.removeAtIndex(manValue.count - 1)
         //value.removeAtIndex(value.count - 1)
        //days.removeAtIndex(days.count - 1)
        // value.removeAtIndex(value.count - 1)
        print("Branch", brn)
        // print("Branch All", branchAllArray)
        print("No",no)
        print("Manfact", manufacture)
        print("Qty", qtyText)
        print("Sales", value)
        print("Brn id", branchId)
        print("Cust id", custId)
        print("Mfg ID", mfgId)
        print("Manvalue",manValue)
        print("QtyInt",qtyInt)
        print("Branch", brn.count)
        // print("Branch All", branchAllArray)
        print("No",no.count)
        print("Manfact", manufacture.count)
          print("QtyInt",qtyInt.count)
        print("Qty", qtyText.count)
        print("Sales", value.count)
        print("Brn id", branchId.count)
        print("Cust id", custId.count)
        print("Mfg ID", mfgId.count)
         print("Manvalue",manValue.count)
        

    /*
        for (var i = 0 ; i < manufacture.count ; i++){
            totalVal = 0
            totalQty = 0
            valueC = 0
            for (var j = 0 ; j < manufacture.count ; j++) {
                
                if manufacture[j] != "Manufacture" && manufacture[j] != ""{
                    print(manufacture[i])
                    print(manufacture[j])
                    if manufacture[i] == manufacture[j] {
                        valueC++
                        
                        
                        if manTitle.contains(manufacture[j]) {
                            
                            print(value[j])
                            print(manValue[j])
                            print(qtyInt[j])
                           
                            totalQty = totalQty + qtyInt[j]
                            totalVal = totalVal + manValue[j]
                        }
                        else {
                            print(value[j])
                            print(manValue[j])
                            
                            print(qtyInt[j])
                            manTitle.append(manufacture[j])
                            totalQty =  qtyInt[j]
                           totalVal = manValue[j]
                        }
                       
                        
                    }
                }
            }
            print(totalVal)
            print(totalQty)
            if valueC > 0 {
                if !totalArray.contains(String(totalVal)){
                     totalArray.append(String(totalVal))
                }
                if !qtyArray.contains(String(totalQty)){
                    qtyArray.append(String(totalQty))
                }
                
            }
           
        }
        
        print("ManTiltle",manTitle)
        print("Total", totalArray)
        print("Qty",qtyArray)
        print("ManTiltle",manTitle.count)
        print("Total", totalArray.count)
         print("Qty",qtyArray.count)
        
        
        
       */
        
        
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
       // totText.text = num.stringFromNumber(total)
       // qtyTot.text = String(qtyTotal)
        outstandingText.text = "Total sales for " + customerName + " is "
        amountText.text = "₹ " + num.stringFromNumber(sum)!
        //        totalText.text = String(total)
        //
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
         getCorporateTotal();
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphCorporateOutDet"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Customer"
            dest.yaxisName = "Outstanding"
            dest.cusprodText = "Customer"
            
            dest.cusProdValue = customerName
            
            dest.from = ""
            dest.to = ""
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("CUSTOMER") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("Outstanding") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
            
        }*/
        if segue.identifier == "toPieCorporateOutDet"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("MANUFATURE") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("AMOUNT") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
        
        }
        if segue.identifier == "toCorporateOutSumInd" {
            let dest = segue.destinationViewController as! CorporateOutSaleInd
            dest.outstandingInd = outstandingIndArray
        }
    }
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == TotalQtyTable {
            return manTitle.count
        }
        else {
                 return no.count
        }
       
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == TotalQtyTable {
            let cell = TotalQtyTable.dequeueReusableCellWithIdentifier("CorporateTotalCell", forIndexPath: indexPath) as! CorporateOutTotalCell
            if indexPath.row == 0 {
                cell.no.attributedText = headFont(noTot[indexPath.row])
                cell.manufacture.attributedText = headFont(manTitle[indexPath.row])
                cell.qty.attributedText = headFont(qtyArray[indexPath.row])
                cell.sales.attributedText = headFont(totalArray[indexPath.row])
                //cell.days.attributedText = headFont(days[indexPath.row])
                cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            }
            else {
                cell.no.text = noTot[indexPath.row]
                cell.manufacture.text = manTitle[indexPath.row]
                cell.qty.text = qtyArray[indexPath.row]
                cell.sales.text = totalArray[indexPath.row]
                cell.backgroundColor = UIColor.whiteColor()
            }
            var frame = CGRect()
            
            frame = TotalQtyTable.frame;
            frame.size.height = TotalQtyTable.contentSize.height;
            TotalQtyTable.frame = frame;
            totalTableHeight.constant = TotalQtyTable.contentSize.height
            return cell
        }
        else {
            let cell = outstandingTable.dequeueReusableCellWithIdentifier("CorporateOutSalesDetCell", forIndexPath: indexPath) as! CorporateOutSalesCell
            //        cell.no.text = no[indexPath.row]
            //        cell.product.text = prName[indexPath.row]
            //        cell.quantity.text = quantity[indexPath.row]
            //       // cell.amount.text = value[indexPath.row]
            //        if no[indexPath.row] == "No." {
            //            cell.amount.text = value[indexPath.row]
            //            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            //        }
            //        else {
            //
            //            cell.amount.text = value[indexPath.row]
            //            cell.backgroundColor = UIColor.whiteColor()
            //        }
            cell.customer.text = ""
            cell.no.text = no[indexPath.row]
            //cell.branch.text = branch[indexPath.row]
            cell.manufacture.text = manufacture[indexPath.row]
            cell.qty.text = qtyText[indexPath.row]
            cell.amount.text = value[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
            
            
            cell.customer.text = brn[indexPath.row]
            //  cell.customer.backgroundColor = UIColor.whiteColor()
            
            if no[indexPath.row] == "No" {
                cell.customer.backgroundColor = UIColor.whiteColor()
                //cell.days.textAlignment = NSTextAlignment.Center
                //cell.outstanding.textAlignment = NSTextAlignment.Center
                cell.no.attributedText = headFont(no[indexPath.row])
                cell.manufacture.attributedText = headFont(manufacture[indexPath.row])
                cell.qty.attributedText = headFont(qtyText[indexPath.row])
                cell.amount.attributedText = headFont(value[indexPath.row])
                //cell.days.attributedText = headFont(days[indexPath.row])
                cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            }
            else {
                
                cell.no.text = no[indexPath.row]
                //cell.days.textAlignment = NSTextAlignment.Center
                // cell.outstanding.textAlignment = NSTextAlignment.Center
                cell.manufacture.text = manufacture[indexPath.row]
                cell.qty.text = qtyText[indexPath.row]
                cell.amount.text = value[indexPath.row]
                //cell.days.text = days[indexPath.row]
                cell.backgroundColor = UIColor.whiteColor()
            }
            if value[indexPath.row].containsString("Total") {
                //cell.outstanding.textAlignment = NSTextAlignment.Right
                //cell.days.textAlignment = NSTextAlignment.Left
                var longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
                
                var attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
                cell.amount.attributedText = attributedString
                longestWordRange = (qtyText[indexPath.row] as NSString).rangeOfString(qtyText[indexPath.row])
                
                attributedString = NSMutableAttributedString(string: qtyText[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
                
                attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
                cell.qty.attributedText = attributedString
                
            }
            
            
            
            
            //        if no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "" {
            //            let longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
            //
            //            let attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            //
            //            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            //            cell.product.attributedText = attributedString
            //        }
            
            //        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "") || (quantity[indexPath.row].rangeOfString("Total") != nil){
            //            cell.selectionStyle = UITableViewCellSelectionStyle.None
            //        }
            //        else {
            //            cell.selectionStyle = UITableViewCellSelectionStyle.Gray
            //        }
            
             var frame = CGRect()
            frame = outstandingTable.frame;
            frame.size.height = outstandingTable.contentSize.height;
            outstandingTable.frame = frame;
            outstandingTableHeight.constant = outstandingTable.contentSize.height
            
            
            
            return cell

        }
           }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == outstandingTable {
            outstandingIndArray.removeAllObjects()
            if (no[indexPath.row] == "No") || (no[indexPath.row] == "" && qtyText[indexPath.row] == "" && manufacture[indexPath.row] == "") || (value[indexPath.row].rangeOfString("Total") != nil){
                
            }
            else{
                branchCode = branchId[indexPath.row]
                manCode = mfgId[indexPath.row]
                custCode = custId[indexPath.row]
                getCorporateOutSumInd()
            }

        }
        
    }
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    
    
    func getCorporateOutSumInd()
        
    {
        
        let custName = corpDet[0].valueForKey("CUST_COMPANY_GROUP") as! String
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE_DETAIL_CUSTOMERWISE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + custName + "&CUSTCODE=" + custCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.outstandingIndArray)
            if self.outstandingIndArray.count == 0 {
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutSumInd", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getCorporateTotal()
        
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE_DETAIL_CONSOLIDATED?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + outstandingCust + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
           // self.totFlag = true
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.corporateTotArray)
            if self.corporateTotArray.count == 0 {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                   // self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                     self.corpTotal = self.corporateTotArray
                    for (var i = 0 ; i < self.corpTotal.count ; i++) {
                        self.noTot.append(String(i+1))
                        self.manTitle.append(self.corpTotal[i].valueForKey("MANUFATURE") as! String)
                        self.totalArray.append(self.corpTotal[i].valueForKey("AMOUNT") as! String)
                        self.qtyArray.append(self.corpTotal[i].valueForKey("Qty") as! String)
                        self.totalQuantity = self.totalQuantity + Int((self.corpTotal[i].valueForKey("Qty") as! String))!
                        self.totalAmt = self.totalAmt + Double((self.corpTotal[i].valueForKey("AMOUNT") as! String))!
                        
                    }
                    self.totalQty.text = String(self.totalQuantity)
                    self.totalAmount.text = String(self.totalAmt)


                    self.TotalQtyTable.reloadData()
                    
                })
                
            }
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    

    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
           if (elementName == "CUST_COMPANY_GROUP" || elementName == "MANUFACTURE" || elementName == "CUST_NAME" || elementName == "PRODUCT" || elementName == "QTY" || elementName == "SALE"){
                noData = false
                elementValue = String()
                element = elementName
                outstandingInd[elementName] = ""
            }
            if elementName == "MANUFATURE" || elementName == "Qty" || elementName == "AMOUNT" {
                noData = false
                
                    elementValue = String()
                    element = elementName
                    corporateTot[elementName] = ""
                }
                
            

        }
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "CUST_COMPANY_GROUP" || element == "MANUFACTURE" || element == "CUST_NAME" || element == "PRODUCT" || element == "QTY" || element == "SALE"){
                if (string.rangeOfString("\n") == nil){
                    outstandingInd[element!] = string
                }
                
            }
        }
        if element == "MANUFATURE" || element == "Qty" || element == "AMOUNT" {
            if string.rangeOfString("\n") == nil{
                
                    corporateTot[element!] = string
                
                
            }
        }
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "SALE" {
            // count = 0
            outstandingIndArray.addObject(outstandingInd)
           
        }
        if elementName == "AMOUNT" {
             corporateTotArray.addObject(corporateTot)
        }
        
        
    }
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
       // performSegueWithIdentifier("toGraphCorporateOutDet", sender: self)
        performSegueWithIdentifier("toPieCorporateOutDet", sender: self)
    }
    
    

}
