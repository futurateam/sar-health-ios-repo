//
//  SalesPerfBranchSumAllCell.swift
//  SAR Health
//
//  Created by Anargha K on 21/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchSumAllCell: UITableViewCell {
    @IBOutlet var no: UILabel!
    @IBOutlet var manfact: UILabel!
    
    @IBOutlet var branch: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet var amount: UILabel!
}
