//
//  DashboardCollectionViewCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 08/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var catName: UILabel!
    @IBOutlet var catImage: UIImageView!
}
