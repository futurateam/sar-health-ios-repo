//
//  CorporateOutDetCell.swift
//  SAR Health
//
//  Created by Anargha K on 20/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutDetCell: UITableViewCell {
    
    @IBOutlet var no: UILabel!
    @IBOutlet var customer: UILabel!
    @IBOutlet var billDate: UILabel!
    @IBOutlet var billNo: UILabel!
    @IBOutlet var days: UILabel!
    @IBOutlet var outstanding: UILabel!
    @IBOutlet var branchName: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
