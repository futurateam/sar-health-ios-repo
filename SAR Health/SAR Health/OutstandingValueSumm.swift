//
//  OutstandingValueSumm.swift
//  SAR Health
//
//  Created by Prajeesh KK on 29/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class OutstandingValueSumm: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
    @IBOutlet var valueTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var valueText: UILabel!
    @IBOutlet var valueAmount: UILabel!
    
    
    
    var noDataArray : NSMutableArray = [], noDataDic = [String :String](), branchCode = "", custArray : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var outstandingDetArray : NSMutableArray = [], outstandingDet = [String : String]()
    var total : Double = 0, custCode = "", custmerName = "", branchName = "", amt = [Double]()
    var outstandingValue : NSMutableArray = [], num = NSNumberFormatter()
    var no = ["No."], customer = ["Customer"], amount = ["Outstanding(₹)"], customerLeg = [String](), customerLegend = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        for (var i = 0; i < outstandingValue.count; i += 1){
            no.append(String(i+1))
            customer.append((outstandingValue[i].valueForKey("customer") as? String)!)
            amount.append((outstandingValue[i].valueForKey("amount") as? String)!)
            total = total + Double((outstandingValue[i].valueForKey("amount") as? String)!)!
        }
      //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        totalText.text = num.stringFromNumber(total)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        if custCode == "0" {
            valueText.text = "Total outstanding for All customers in " + branchName + " branch is"
        }
        else {
            valueText.text = "Total oustanding for " + custmerName + " in " + branchName + " branch is"
         }
        valueAmount.text = "₹ " + num.stringFromNumber(total)!
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
      /*  if segue.identifier == "toGraphOutstandingSum"{
            amt.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            for (var i = 1; i < amount.count ; i += 1) {
                amt.append(Double(amount[i])!)
            }
            
            dest.chartData = amt
            dest.xaxisName = "Customer"
            dest.yaxisName = "Amount"
            
           
            dest.cusprodText = "Customer"
            if custmerName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = custmerName
            }
            for (var i = 1 ; i < customer.count ; i++) {
                customerLeg.append(customer[i])
            }
            dest.chartLegend = customerLeg
            dest.titleTab = "Outstanding Report"
        
        }*/
        if segue.identifier == "toPieOutstandingSumm"{
            amt.removeAll()
            customerLegend.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = ""
            dest.toDateStr = ""
            dest.to = ""
            dest.titleTab = "Outstanding Report"
            for (var i = 0 ; i < outstandingValue.count ; i++) {
                customerLegend.append(outstandingValue[i].valueForKey("customer") as! String)
                 amt.append(Double(outstandingValue[i].valueForKey("amount") as! String)!)
            }
            dest.chartData = amt
            dest.chartLegend = customerLegend
        }
        if segue.identifier == "toOutstandingDet" {
            let dest = segue.destinationViewController as? OutstandingValueDet
            dest?.outstandingValue = outstandingDetArray
            dest?.custCode = custCode
            dest?.custmerName = custmerName
            dest?.branchName = branchName
           
            
        }

    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = valueTable.dequeueReusableCellWithIdentifier("OutstandingSumCell", forIndexPath: indexPath) as! OutstandingValueSummCell
        cell.no.text = no[indexPath.row]
        cell.customer.text = customer[indexPath.row]
       // cell.outstanding.text = amount[indexPath.row]
        if indexPath.row == 0 {
            cell.outstanding.text = amount[indexPath.row]
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.outstanding.text = num.stringFromNumber(Double(amount[indexPath.row])!)
            
            cell.backgroundColor = UIColor.whiteColor()
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        outstandingDetArray.removeAllObjects()
//        for (var i = 0; i < custArray.count; i += 1){
//            if custArray[i].valueForKey("CUSTOMER") as? String == customer[indexPath.row]{
//                custCode = (custArray[i].valueForKey("CODE") as? String)!
//            }
//        }
        custCode = outstandingValue[indexPath.row - 1].valueForKey("cust_code") as! String
        getOutstandingDet()
    }
    
    
    
    func getOutstandingDet()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
               let scriptUrl1 = Constants().API + "MOBAPP_OUTSTANDING_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("OutstaningDet", self.outstandingDetArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.outstandingDet.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toOutstandingDet", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                if elementName == "Column1" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    noDataDic[elementName] = ""
                }
            }
            if (elementName == "CUSTOMER" || elementName == "BillDate" || elementName == "BillNo" || elementName == "Outstanding" || elementName == "Days") {
                noData = false
                elementValue = String()
                element = elementName
                outstandingDet[elementName] = ""
                
            }
        }
        
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "CUSTOMER" || element == "BillDate" || element == "BillNo" || element == "Outstanding" || element == "Days") {
                if string.rangeOfString("\n") == nil {
                    outstandingDet[element!] = string
                }
            }
            if (element == "Table1"){
                if element == "Column1" {
                    noDataDic[element!] = string
                }
            }
            
            
        }
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Days" {
            outstandingDetArray.addObject(outstandingDet)
        }
        if elementName == "Table1" {
            if elementName == "Column1" {
                noDataArray.addObject(noDataDic)
            }
        }
    }

    
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func viewGraphPressed(sender: AnyObject) {
        
       // performSegueWithIdentifier("toGraphOutstandingSum", sender: self)
        performSegueWithIdentifier("toPieOutstandingSumm", sender: self)
    }
}
