//
//  SalesPerfManfactCell.swift
//  SAR Health
//
//  Created by Anargha K on 17/09/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfManfactCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    @IBOutlet var manfact: UILabel!
    @IBOutlet var amount: UILabel!
}
