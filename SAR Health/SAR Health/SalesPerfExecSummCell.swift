//
//  SalesPerfExecSummCell.swift
//  SAR Health
//
//  Created by Anargha K on 21/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfExecSummCell: UITableViewCell {

    @IBOutlet var category: UILabel!
    @IBOutlet var no: UILabel!
    
    @IBOutlet var executive: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
