//
//  SalesPerfBranchSummDet.swift
//  SAR Health
//
//  Created by Anargha K on 11/07/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchSummDet: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    @IBOutlet var salesTable: UITableView!
    @IBOutlet var salesText: UILabel!
    @IBOutlet var amountText: UILabel!
    @IBOutlet weak var totalText: UILabel!
    @IBOutlet var totQty: UILabel!
    
    var elementValue: String?
    var element : String?
    var sum : Double = 0, manfactName = "", noData : Bool = true, salesBranchInd = [String:String](), salesBranchIndArray : NSMutableArray = []
    var branchArray : NSMutableArray = [], branchCode = "", prodCode = "", manCode = "", prodSel = ""
    var no = ["No."]
    var product = ["Product"]
    var quantity = ["Qty"]
    var value = ["Amount(₹)"]
   var amount = [Double]()
    var  i = 0, qtyTot : Int = 0
    var j = 0
    var count = 0
    var limit = 1
    var total : Double = 0
    var fromDate = ""
    var toDate = ""
    var branchName = "", num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        for (i = 0 ; i < branchArray.count; i++) {
            no.append(String(i+1))
            product.append(branchArray[i].valueForKey("PRODUCT") as! String)
            quantity.append((branchArray[i].valueForKey("QTY") as? String)!)
            value.append((branchArray[i].valueForKey("AMOUNT") as? String)!)
            qtyTot = qtyTot + Int((branchArray[i].valueForKey("QTY") as? String)!)!
            total = total + Double((branchArray[i].valueForKey("AMOUNT") as? String)!)!
        }
        
        
        /*for ( i = 0; i < branchArray.count ; i += 1){
         print("i: ",i)
         count = i
         limit = 1
         print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
         
         
         let mfg = branchArray[i].valueForKey("MANUFACTURER") as? String
         prName.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
         prName.append("Product")
         
         
         no.append(String(limit))
         prName.append((branchArray[i].valueForKey("PRODUCT") as? String)!)
         quantity.append((branchArray[i].valueForKey("QTY") as? String)!)
         value.append((branchArray[i].valueForKey("AMOUNT") as? String)!)
         qtyTot = qtyTot + Int((branchArray[i].valueForKey("QTY") as? String)!)!
         total = Double((branchArray[i].valueForKey("AMOUNT") as? String)!)!
         for ( j = i+1 ; j < branchArray.count ; j += 1){
         if branchArray[j].valueForKey("MANUFACTURER") as? String == mfg{
         
         prName.append((branchArray[j].valueForKey("PRODUCT") as? String)!)
         quantity.append((branchArray[j].valueForKey("QTY") as? String)!)
         value.append((branchArray[j].valueForKey("AMOUNT") as? String)!)
         total = total + Double((branchArray[j].valueForKey("AMOUNT") as? String)!)!
         qtyTot = qtyTot + Int((branchArray[j].valueForKey("QTY") as? String)!)!
         print("Total",total)
         limit++
         no.append(String(limit))
         count += 1
         
         }
         
         }
         let tot = "Total = " + num.stringFromNumber(total)!
         let qty = "Total Qty = " + String(qtyTot)
         value.append(tot)
         sum = sum + total
         
         no.append("")
         no.append("")
         no.append("No.")
         prName.append("")
         
         
         quantity.append(qty)
         quantity.append("")
         quantity.append("Qty")
         value.append("")
         value.append("Amount(₹)")
         i = count
         
         }
         no.removeAtIndex(no.count - 1)
         no.removeAtIndex(no.count - 1)
         
         // prName.removeAtIndex(prName.count - 1)
         //prName.removeAtIndex(prName.count - 1)
         quantity.removeAtIndex(quantity.count - 1)
         quantity.removeAtIndex(quantity.count - 1)
         value.removeAtIndex(value.count - 1)
         value.removeAtIndex(value.count - 1)
         print("No",no)
         print("PRname", prName)
         print("Quantity",quantity)
         print("Amount",value)
         print("No",no.count)
         print("PRname", prName.count)
         print("Quantity",quantity.count)
         print("Amount",value.count)*/
        
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
        totalText.text = num.stringFromNumber(total)!
        totQty.text = String(qtyTot)
        salesText.text = "Total sales in " + branchName + " branch from " + fromDate + " to " + toDate + " is "
        amountText.text = "₹ " + num.stringFromNumber(total)!
        //        totalText.text = String(total)
        //
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toGraphSalesPerfBranchDet" {
            product.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Product"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Manufacturer"
            if manfactName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = manfactName
            }
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Branch)"
            for(var  i = 0; i < branchArray.count; i += 1){
                product.append((branchArray[i].valueForKey("PRODUCT") as? String)!)
                amount.append(Double((branchArray[i].valueForKey("AMOUNT") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = product
            
        }
        if segue.identifier == "toSalesPerfBranchInd" {
            
            let dest = segue.destinationViewController as! SalesPerfBranchInd
            dest.salesDet = salesBranchIndArray
            dest.manfactName = manfactName
            dest.productName = prodSel
            
        }
        
    }
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalesPerfBranchDetCell", forIndexPath: indexPath) as! SalesPerfBranchSummDetCell
        //        cell.no.text = no[indexPath.row]
        //        cell.product.text = prName[indexPath.row]
        //        cell.quantity.text = quantity[indexPath.row]
        //       // cell.amount.text = value[indexPath.row]
        //        if no[indexPath.row] == "No." {
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        //        }
        //        else {
        //
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor.whiteColor()
        //        }
        if no[indexPath.row] == "No." {
            cell.manfact.text = branchArray[0].valueForKey("MANUFACTURER") as! String
            cell.manfact.backgroundColor = UIColor.whiteColor()
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            longestWordRange = (product[indexPath.row] as NSString).rangeOfString(product[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: product[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.product.attributedText = attributedString
            longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.quantity.attributedText = attributedString
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.amount.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.no.text = no[indexPath.row]
            cell.manfact.text = ""
            cell.product.text = product[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = value[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && product[indexPath.row] != ""){
        }
        else{
              prodSel = product[indexPath.row]
             salesBranchIndArray.removeAllObjects()
            /* for (var i = 0 ; i < branchArray.count ; i++){
             if (prName[indexPath.row] == branchArray[i].valueForKey("PRODUCT") as! String) {
             prodCode = branchArray[i].valueForKey("PR_CAT_ID") as! String
             manCode = branchArray[i].valueForKey("PR_MFG_ID") as! String
             
             }
             }*/
             prodCode = branchArray[indexPath.row - 1].valueForKey("PR_CAT_ID") as! String
            manCode = branchArray[indexPath.row - 1].valueForKey("PR_MFG_ID") as! String
             if Reachability.isConnectedToNetwork(){
              getSalesBranchInd()
             }
             else {
             Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
             }
            
        }
    }
    
    func getSalesBranchInd()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED_PRODUCTWISE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode + "&CUSTOMER=" + custCode +  "&FRMDATE="  + fromDate + "&TODATE=" + toDate
        //
        //        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //
        let scriptUrl1 = "http://103.35.164.20/sarmobapp/servicecommon.asmx/MOBAPP_BRANCH_SALE_DETAIL_PRODUCTWISE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode + "&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Sales Ind", self.salesBranchIndArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfBranchInd", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "INV_NO" || elementName == "INV_DATE" || elementName == "CUST_NAME" || elementName == "MANUFACTURER" || elementName == "PRODUCT" || elementName == "BATCH_NO" || elementName == "QTY" || elementName == "AMOUNT"){
                noData = false
                elementValue = String()
                element = elementName
                salesBranchInd[elementName] = ""
            }
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "INV_NO" || element == "INV_DATE" || element == "CUST_NAME" || element == "MANUFACTURER" || element == "PRODUCT" || element == "BATCH_NO" || element == "QTY" || element == "AMOUNT"){
                if (string.rangeOfString("\n") == nil){
                  
                    salesBranchInd[element!] = string
                    
                    
                }
                
            }
            
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "AMOUNT" {
            count = 0
            salesBranchIndArray.addObject(salesBranchInd)
            
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        performSegueWithIdentifier("toGraphSalesPerfBranchDet", sender: self)
    }


}
