//
//  CorporateOutSalesIndCell.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSalesIndCell: UITableViewCell {

   
    @IBOutlet var product: UILabel!
    
    @IBOutlet var no: UILabel!
    @IBOutlet var invDate: UILabel!
    
    @IBOutlet var invNo: UILabel!
    
    @IBOutlet var batchNo: UILabel!
   
    @IBOutlet var qty: UILabel!
    
    @IBOutlet var sales: UILabel!
    
}
