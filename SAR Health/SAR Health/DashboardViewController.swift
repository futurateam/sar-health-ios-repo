//
//  DashboardViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 08/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NSXMLParserDelegate{

    @IBOutlet var dashboardCollection: UICollectionView!
    var categoryName = [String]()
    var categoryImage = [String]()
    var cell=0
    
    
    var count = 0
    var limit = 0
    var count1 = 0
    var limit1 = 0
    var parserProduct : NSXMLParser!
    var elementArray : NSMutableArray = []
    var elementProduct : NSMutableArray = [], chngPass : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var flag = true
    var sample = [String : String]()
    var sampleProduct = [String : String]()
    var arrayValue : NSDictionary = [String : String](), userId = ""
    var dshbrd = [String](), dshbrdFlag = [Bool](), moduleArray : NSMutableArray = [], module = [String:String](), allBranch = [String : String](), allBranchArray : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
        if chngPass != [] {
            Common().showAlert(Constants().APP_NAME, message: (self.chngPass[0].valueForKey("Column1") as? String)!, viewController: self)
        }
        userId = (NSUserDefaults.standardUserDefaults().valueForKey("UserId") as? String)!
        moduleArray.removeAllObjects()
        elementArray.removeAllObjects()
        elementProduct.removeAllObjects()
        allBranchArray.removeAllObjects()
        if Reachability.isConnectedToNetwork() {
        getDataBranch()
        getDataProduct()
            getUserModules()
            getAllBranches()
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
     //   getDataProduct()
        // Do any additional setup after loading the view.
        categoryName = ["PRODUCT STOCK", "SALES PRICE", "PURCHASE PRICE", "PRODUCT EXPIRY", "REPORTS", "CREDIT NOTE"]
        dshbrdFlag = [false, false, false, false, true, false, false]
        categoryImage = ["product.png", "sales.png", "purchase.png", "prdexp.png", "report.png", "credit.png"]
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toProductStock"{
            let dest = segue.destinationViewController as? ProductStockMainViewController
            //print("Branchhhh", elementArray)
            //print("proo",elementProduct)
            dest?.cellNo = cell;
            dest?.branchList = elementArray
            dest?.product = elementProduct
        }
//        else if segue.identifier == "toReports"{
//            let dest = segue.destinationViewController as? ReportsViewController
//            dest?.branchList = elementArray
//            dest?.itemList = elementProduct
//        }
        else if segue.identifier == "toCreditNote" {
            let dest = segue.destinationViewController as? SalesPerformanceItem
            dest?.branch = elementArray
            dest?.cellNo = cell
        }
    }
    
    
    
    //Mark: - Collection View methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryName.count;
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
        let cell = dashboardCollection.dequeueReusableCellWithReuseIdentifier("DashboardCell", forIndexPath: indexPath) as! DashboardCollectionViewCell
        cell.catName.text = categoryName[indexPath.row];
        //print("cell",cell.catName.text);
        if dshbrdFlag[indexPath.row]{
            let image = UIImage (named: categoryImage[indexPath.row])
            cell.catImage.image = image;
        }
        else {
            let image = UIImage (named: "notinuse.png")
            cell.catImage.image = image
        }
       
        //cell.backgroundColor = UIColor.redColor();
        return cell;
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(88, 119)
    }
  
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(40,20,5,20)
        
    
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 30.0;
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        cell = indexPath.row
        if dshbrdFlag[cell] == true {
        if (cell == 0 || cell == 1 || cell == 2 || cell == 3){
            performSegueWithIdentifier("toProductStock", sender: self)
        }
        else if cell == 4{
            performSegueWithIdentifier("toReports", sender: self)
        }
        
        else if cell == 5 {
            performSegueWithIdentifier("toCreditNote", sender: self)
        }
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "The user can't access this", viewController: self)
        }
        
        
    }
    
    func getDataBranch ()
    {
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let scriptUrl1 = Constants().API + "MOBAPP_USERS_BRANCHES?User=" + userId
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            self.flag = true
            if error != nil
            {
                print("error=\(error)")
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
                return
            }
            
            // Print out response string
           let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
           print("responseString = \(responseString)")
            let parserBranch = NSXMLParser(data: data!)
            parserBranch.delegate = self
            parserBranch.parse()
            NSUserDefaults.standardUserDefaults().setObject(self.elementArray, forKey: "Branches")
            NSUserDefaults.standardUserDefaults().synchronize()
            // Convert server json response to NSDictionary
            //            do {
            //                if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                    // Print out dictionary
            //                    print(convertedJsonIntoDict)
            //
            //                    // Get value by key
            //                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                    print(firstNameValue!)
            //
            //                }
            //            }
            //            catch let error as NSError {
            //                print(error.localizedDescription)
            //            }
            
        }
       
        task.resume()
        
        
    }
    
    
    func getDataProduct ()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")

        let scriptUrl1 = Constants().API + "MOBAPP_PRODUCTS?PRODUCT="
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            self.flag = false
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
             let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            self.parserProduct = NSXMLParser(data: data!)
            self.parserProduct.delegate = self
            self.parserProduct.parse()
            NSUserDefaults.standardUserDefaults().setObject(self.elementProduct, forKey: "Products")
            NSUserDefaults.standardUserDefaults().synchronize()
             dispatch_async(dispatch_get_main_queue(), {
            HUD.hideUIBlockingIndicator()
             })
            // Convert server json response to NSDictionary
            //            do {
            //                if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                    // Print out dictionary
            //                    print(convertedJsonIntoDict)
            //
            //                    // Get value by key
            //                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                    print(firstNameValue!)
            //
            //                }
            //            }
            //            catch let error as NSError {
            //                print(error.localizedDescription)
            //            }
            
        }
      
        task.resume()
        
        
    }
    
    
    

    
    func getUserModules ()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        let scriptUrl1 = Constants().API + "MOBAPP_USERS_MODULES?User=" + userId
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            self.flag = false
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            self.parserProduct = NSXMLParser(data: data!)
            self.parserProduct.delegate = self
            self.parserProduct.parse()
            print("Modules", self.moduleArray)
            NSUserDefaults.standardUserDefaults().setObject(self.moduleArray, forKey: "Module_Array")
            NSUserDefaults.standardUserDefaults().synchronize()
            for (var i = 0; i < self.moduleArray.count ; i++){
                for (var j = 0 ; j < self.categoryName.count ; j++){
                    if(self.moduleArray[i].valueForKey("ModuleName") as? String == self.categoryName[j]){
                        self.dshbrdFlag[j] = true
                    }
                    else {
                        if self.dshbrdFlag[j] != true {
                        self.dshbrdFlag[j] = false
                        }
                    }
                   
                }
            }
           

          
            dispatch_async(dispatch_get_main_queue(), {
                  self.dashboardCollection.reloadData()
                HUD.hideUIBlockingIndicator()
            })
            // Convert server json response to NSDictionary
            //            do {
            //                if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                    // Print out dictionary
            //                    print(convertedJsonIntoDict)
            //
            //                    // Get value by key
            //                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                    print(firstNameValue!)
            //
            //                }
            //            }
            //            catch let error as NSError {
            //                print(error.localizedDescription)
            //            }
            
        }
        
        task.resume()
        
        
    }
    func getAllBranches ()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCHES"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            self.flag = false
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            self.parserProduct = NSXMLParser(data: data!)
            self.parserProduct.delegate = self
            self.parserProduct.parse()
            print("Modules", self.moduleArray)
            NSUserDefaults.standardUserDefaults().setObject(self.moduleArray, forKey: "Module_Array")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            print("All Branch", self.allBranchArray.count)
            NSUserDefaults.standardUserDefaults().setObject(self.allBranchArray.count , forKey: "Branch_Count")
            NSUserDefaults.standardUserDefaults().synchronize()
            dispatch_async(dispatch_get_main_queue(), {
                
                HUD.hideUIBlockingIndicator()
            })
            // Convert server json response to NSDictionary
            //            do {
            //                if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                    // Print out dictionary
            //                    print(convertedJsonIntoDict)
            //
            //                    // Get value by key
            //                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                    print(firstNameValue!)
            //
            //                }
            //            }
            //            catch let error as NSError {
            //                print(error.localizedDescription)
            //            }
            
        }
        
        task.resume()
        
        
    }


    
//    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
//       
//        
//        
//        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
//            if (elementName == "ID" || elementName == "BRANCH") {
//            elementValue = String()
//            element = elementName
//          
//            sample[elementName] = ""
//            }
//        }
//            else if(elementName == "PRODUCT_CODE" || elementName == "PRODUCT") {
//                elementValue = String()
//                element = elementName
//                
//                sampleProduct[elementName] = ""
//            }
//    }
//    
//    func parser(parser: NSXMLParser, foundCharacters string: String) {
//        //        if elementValue != nil {
//        //            elementValue! += string
//        //
//        //
//        //
//        //        }
//        
//        if elementValue != nil{
//            if flag == true {
//            if (string.rangeOfString("\n") == nil){
//                //print("String", string)
//                sample[element!] = string
//            }
//            }
//            else {
//                if (string.rangeOfString("\n") == nil){
//                    //print("String", string)
//                    sampleProduct[element!] = string
//                }
//
//            }
//        }
//        
//    }
//    
//    
//    
//    
//    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
//        if flag == true{
//            if elementName == "BRANCH" {
//                elementArray.addObject(sample)
//            }
//        }
//        else  {
//            if elementName == "PRODUCT" {
//                elementProduct.addObject(sampleProduct)
//            }
//        }
//        
//       
//    }
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "BRN_ID" || elementName == "BRN_NAME"){
                elementValue = String()
                element = elementName
                sample[elementName] = ""
            }
             if (elementName == "PRODUCT_CODE" || elementName == "PRODUCT"){
                elementValue = String()
                element = elementName
                sampleProduct[elementName] = ""
                
            }
            if (elementName == "ModuleId" || elementName == "ModuleName") {
                elementValue = String()
                element = elementName
                module[elementName] = ""
            }
            if (elementName == "ID" || elementName == "BRANCH") {
                elementValue = String()
                element = elementName
                allBranch[elementName] = ""
            }
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
                //print("String", string)
                if (element == "BRN_ID" || element == "BRN_NAME"){
                    if (string.rangeOfString("\n") == nil){
                    sample[element!] = string
                    }
                    
                }
                if (element == "PRODUCT_CODE" || element == "PRODUCT"){
                    if (string.rangeOfString("\n") == nil){
                    count += 1
                    sampleProduct[element!] = string
                    }
                }
                if (element == "ModuleId" || element == "ModuleName") {
                    if (string.rangeOfString("\n") == nil){
                        module[element!] = string
                    }
                }
            if (element == "ID" || element == "BRANCH") {
                if (string.rangeOfString("\n") == nil) {
                    allBranch[element!] = string

                }
            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "BRN_NAME" {
            elementArray.addObject(sample)
        }
         if (elementName == "PRODUCT") {
           
            elementProduct.addObject(sampleProduct)
        }
        if elementName == "ModuleName" {
            moduleArray.addObject(module)
        }
        if elementName == "BRANCH" {
            allBranchArray.addObject(allBranch)
        }
        
    }
    
    @IBOutlet var menuBtn: UIButton!
    @IBAction func menuBtnPressed(sender: AnyObject) {
        self.onSlideMenuButtonPressed(menuBtn)
    }

}
