//
//  SalesPerfCustDetailed.swift
//  SAR Health
//
//  Created by Prajeesh KK on 27/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfCustDetailed: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    
    @IBOutlet var salesText: UILabel!
    
    @IBOutlet var titleText: UILabel!
    @IBOutlet var salesPrice: UILabel!
    
    @IBOutlet var salesTable: UITableView!
    
    var elementValue: String?
    var element : String?
    var sum : Float = 0, amount = [Double](), manfact = [String](), branchArray = [String]()
    var custArray : NSMutableArray = [], salesCustIndArray : NSMutableArray = [], salesCustInd = [String:String]()
    var no = ["No"]
    var prName = ["Product"], branchCode = "", prodCode = "", custCode = "", brn = [String](), mfgAll = [String](), qty = 0, cellNo = 0
    var quantity = ["Qty"]
    var value = ["Amount(₹)"], noData : Bool = true, prod = [String]()
    var  i = 0
    var j = 0
    var count = 0
    var limit = 1
    var total : Float = 0
    var fromDate = ""
    var toDate = "", custName = ""
    var num = NSNumberFormatter()
    var branchName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        if cellNo == 1 {
            for ( i = 0; i < custArray.count ; i += 1){
                total = 0
                qty = 0
                print("i: ",i)
                print("Count", count)
                count = i
                limit = 0
                //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
                
                
                let mfg = custArray[i].valueForKey("MFG_NAME") as? String
                //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
                //            prName.append("Product")
                if (i == 0 ){
                    brn.append(mfg!)
                    mfgAll.append(mfg!)
                    for ( j = i ; j < custArray.count ; j += 1){
                        
                        if custArray[j].valueForKey("MFG_NAME") as? String == mfg{
                            mfgAll.append(mfg!)
                            brn.append("")
                            prName.append((custArray[j].valueForKey("PR_NAME") as? String)!)
                            quantity.append((custArray[j].valueForKey("SO_QTY") as? String)!)
                            value.append((custArray[j].valueForKey("VALUE") as? String)!)
                            qty = qty + Int((custArray[j].valueForKey("SO_QTY") as? String)!)!
                            total = total + Float((custArray[j].valueForKey("VALUE") as? String)!)!
                            limit += 1
                            no.append(String(limit))
                            //count += 1
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                    let qtyText = "Total Qty = " + String(qty)
                    value.append(tot)
                    sum =  total
                    //brn.append("")
                    mfgAll.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    
                    // billDate.append("")
                    prName.append(qtyText)
                    prName.append("Product")
                    //billNo.append("")
                    quantity.append("Total =")
                    // days.append("")
                    quantity.append("Qty")
                    //value.append("")
                    value.append("Amount (₹)")
                    // i = count
                    
                }else {
                    if brn.contains(mfg!){
                        
                    }
                    else {
                        brn.append(mfg!)
                        mfgAll.append(mfg!)
                        for ( j = 0 ; j < custArray.count ; j += 1){
                            
                            if custArray[j].valueForKey("MFG_NAME") as? String == mfg{
                                mfgAll.append(mfg!)
                                brn.append("")
                                prName.append((custArray[j].valueForKey("PR_NAME") as? String)!)
                                quantity.append((custArray[j].valueForKey("SO_QTY") as? String)!)
                                value.append((custArray[j].valueForKey("VALUE") as? String)!)
                                qty = qty + Int((custArray[j].valueForKey("SO_QTY") as? String)!)!
                                total = total + Float((custArray[j].valueForKey("VALUE") as? String)!)!
                                limit += 1
                                no.append(String(limit))
                            }
                            
                        }
                        let tot =  num.stringFromNumber(total)!
                        let qtyText = "Total Qty = " + String(qty)
                        value.append(tot)
                        sum = sum + total
                        
                        //brn.append("")
                        mfgAll.append("")
                        brn.append("")
                        //no.append("")
                        no.append("")
                        no.append("No")
                        //customer.append("")
                        
                        // billDate.append("")
                        prName.append(qtyText)
                        prName.append("Product")
                        //billNo.append("")
                        quantity.append("Total =")
                        // days.append("")
                        quantity.append("Qty")
                        //value.append("")
                        value.append("Amount (₹)")
                        //i = count
                        
                        
                    }
                }
                
                
            }
            // no.removeAtIndex(no.count - 1)
            no.removeAtIndex(no.count - 1)
            //  brn.removeAtIndex(brn.count - 1)
            // customer.removeAtIndex(customer.count - 1)
            //billDate.removeAtIndex(billDate.count - 1)
            // billNo.removeAtIndex(billNo.count - 1)
            
            // days.removeAtIndex(days.count - 1)
            prName.removeAtIndex(prName.count - 1)
            quantity.removeAtIndex(quantity.count - 1)
            value.removeAtIndex(value.count - 1)
            // value.removeAtIndex(value.count - 1)
            print("Branch", brn)
            print("Branch All", mfgAll)
            print("No",no)
            
            print("Product", prName)
            print("Quantity", quantity)
            print("Amount", value)
            print("Branch", brn.count)
            print("No",no.count)
            print("Branch All", mfgAll.count)
            
            print("Product", prName.count)
            print("Quantity", quantity.count)
            print("Amount", value.count)
            titleText.text = "Sales Performance (Customer)"
        }
        else {
            for ( i = 0; i < custArray.count ; i += 1){
                total = 0
                qty = 0
                print("i: ",i)
                print("Count", count)
                count = i
                limit = 0
                //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
                
                
                let mfg = custArray[i].valueForKey("MANUFATURE") as? String
                //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
                //            prName.append("Product")
                if (i == 0 ){
                    brn.append(mfg!)
                    mfgAll.append(mfg!)
                    for ( j = i ; j < custArray.count ; j += 1){
                        
                        if custArray[j].valueForKey("MANUFATURE") as? String == mfg{
                            mfgAll.append(mfg!)
                            brn.append("")
                            prName.append((custArray[j].valueForKey("PRODUCT") as? String)!)
                            quantity.append((custArray[j].valueForKey("QTY") as? String)!)
                            value.append((custArray[j].valueForKey("AMOUNT") as? String)!)
                            qty = qty + Int((custArray[j].valueForKey("QTY") as? String)!)!
                            total = total + Float((custArray[j].valueForKey("AMOUNT") as? String)!)!
                            limit += 1
                            no.append(String(limit))
                            //count += 1
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                    let qtyText = "Total Qty = " + String(qty)
                    value.append(tot)
                    sum =  total
                    //brn.append("")
                    mfgAll.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    
                    // billDate.append("")
                    prName.append(qtyText)
                    prName.append("Product")
                    //billNo.append("")
                    quantity.append("Total =")
                    // days.append("")
                    quantity.append("Qty")
                    //value.append("")
                    value.append("Amount (₹)")
                    // i = count
                    
                }else {
                    if brn.contains(mfg!){
                        
                    }
                    else {
                        brn.append(mfg!)
                        mfgAll.append(mfg!)
                        for ( j = 0 ; j < custArray.count ; j += 1){
                            
                            if custArray[j].valueForKey("MANUFATURE") as? String == mfg{
                                mfgAll.append(mfg!)
                                brn.append("")
                                prName.append((custArray[j].valueForKey("PRODUCT") as? String)!)
                                quantity.append((custArray[j].valueForKey("QTY") as? String)!)
                                value.append((custArray[j].valueForKey("AMOUNT") as? String)!)
                                qty = qty + Int((custArray[j].valueForKey("QTY") as? String)!)!
                                total = total + Float((custArray[j].valueForKey("AMOUNT") as? String)!)!
                                limit += 1
                                no.append(String(limit))
                            }
                            
                        }
                        let tot =  num.stringFromNumber(total)!
                        let qtyText = "Total Qty = " + String(qty)
                        value.append(tot)
                        sum = sum + total
                        
                        //brn.append("")
                        mfgAll.append("")
                        brn.append("")
                        //no.append("")
                        no.append("")
                        no.append("No")
                        //customer.append("")
                        
                        // billDate.append("")
                        prName.append(qtyText)
                        prName.append("Product")
                        //billNo.append("")
                        quantity.append("Total =")
                        // days.append("")
                        quantity.append("Qty")
                        //value.append("")
                        value.append("Amount (₹)")
                        //i = count
                        
                        
                    }
                }
                
                
            }
            // no.removeAtIndex(no.count - 1)
            no.removeAtIndex(no.count - 1)
            //  brn.removeAtIndex(brn.count - 1)
            // customer.removeAtIndex(customer.count - 1)
            //billDate.removeAtIndex(billDate.count - 1)
            // billNo.removeAtIndex(billNo.count - 1)
            
            // days.removeAtIndex(days.count - 1)
            prName.removeAtIndex(prName.count - 1)
            quantity.removeAtIndex(quantity.count - 1)
            value.removeAtIndex(value.count - 1)
            // value.removeAtIndex(value.count - 1)
            print("Branch", brn)
            print("Branch All", mfgAll)
            print("No",no)
            
            print("Product", prName)
            print("Quantity", quantity)
            print("Amount", value)
            print("Branch", brn.count)
            print("No",no.count)
            print("Branch All", mfgAll.count)
            
            print("Product", prName.count)
            print("Quantity", quantity.count)
            print("Amount", value.count)
             titleText.text = "Sales Performance(Manufacture)"
        }

              self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
//        for(var  i = 0; i < salesCust.count; i++){
//            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
//        }
//        
        salesText.text = "Total sales in " + branchName + " branch from " + fromDate + " to " + toDate + " is "
        salesPrice.text = "₹ " + num.stringFromNumber(sum)!
//        totalText.text = String(total)
//        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
       /* if segue.identifier == "toGraphSalePerfCustDet"{
            amount.removeAll()
            manfact.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Manufacturer"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Customer"
            if custName == "" {
                 dest.cusProdValue = "All"
            }
            else{
                 dest.cusProdValue = custName
            }
           
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Customers)"
            for(var  i = 0; i < custArray.count; i++){
                amount.append(Double((custArray[i].valueForKey("VALUE") as? String)!)!)
                manfact.append((custArray[i].valueForKey("MFG_NAME") as? String)!)
            }
            dest.chartData = amount
            dest.chartLegend = manfact

        }*/
        if segue.identifier == "toPieSalesPerfCustDet" {
            if cellNo == 1 {
                amount.removeAll()
                prod.removeAll()
                let dest = segue.destinationViewController as! PieChartViewController
                dest.fromDateStr = fromDate
                dest.toDateStr = toDate
                dest.to = "To"
                dest.titleTab = "Sales Performance (Customers)"
                for(var  i = 0; i < custArray.count; i++){
                    amount.append(Double((custArray[i].valueForKey("VALUE") as? String)!)!)
                    prod.append((custArray[i].valueForKey("PR_NAME") as? String)!)
                }
                dest.chartData = amount
                dest.chartLegend = prod
            }
            else {
                amount.removeAll()
                prod.removeAll()
                let dest = segue.destinationViewController as! PieChartViewController
                dest.fromDateStr = fromDate
                dest.toDateStr = toDate
                dest.to = "To"
                dest.titleTab = "Sales Performance(Manufacture)"
                for(var  i = 0; i < custArray.count; i++){
                    amount.append(Double((custArray[i].valueForKey("AMOUNT") as? String)!)!)
                    prod.append((custArray[i].valueForKey("PRODUCT") as? String)!)
                }
                dest.chartData = amount
                dest.chartLegend = prod
            }
            

        }
        if segue.identifier == "toSalesPerfCustInd" {
            let dest = segue.destinationViewController as! SalesPerfCustomerInd
            dest.salesDet = salesCustIndArray
        }
        
        
    }
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalesPerfCustDetCell", forIndexPath: indexPath) as! SalesPerfCustDetCell
        cell.category.text = brn[indexPath.row]
        cell.product.textAlignment = NSTextAlignment.Left
        if no[indexPath.row] == "No" {
            cell.category.backgroundColor = UIColor.whiteColor()
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
             longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
            
             attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.product.attributedText = attributedString
            longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.quantity.attributedText = attributedString
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.amount.attributedText = attributedString
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.no.text = no[indexPath.row]
            
            cell.product.text = prName[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = value[indexPath.row]
             cell.backgroundColor = UIColor.whiteColor()
        }
        if no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "" {
            let longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
            
            let attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.product.attributedText = attributedString
        }
        if quantity[indexPath.row] == "Total =" {
            var longestWordRange = (quantity[indexPath.row] as NSString).rangeOfString(quantity[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: quantity[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.quantity.attributedText = attributedString
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.amount.attributedText = attributedString
            longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.product.attributedText = attributedString
            cell.product.textAlignment = NSTextAlignment.Right

        }
        
        
        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && brn[indexPath.row] != "") || (quantity[indexPath.row] == "Total ="){
            cell.selectionStyle = UITableViewCellSelectionStyle.None
        }
        else {
            cell.selectionStyle = UITableViewCellSelectionStyle.Gray
        }

        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if cellNo == 1 {
            if (no[indexPath.row] == "No") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && brn[indexPath.row] != "") || (quantity[indexPath.row] == "Total ="){
            }
            else {
                salesCustIndArray.removeAllObjects()
                
                for (var i = 0 ; i < custArray.count ; i++){
                    
                    if (mfgAll[indexPath.row] == custArray[i].valueForKey("MFG_NAME") as? String && prName[indexPath.row] == custArray[i].valueForKey("PR_NAME") as! String) {
                        
                        prodCode = custArray[i].valueForKey("PR_CAT_ID") as! String
                        custCode = custArray[i].valueForKey("SO_CUST_CODE") as! String
                        
                    }
                }
                getSalesItemInd()
                // performSegueWithIdentifier("toSalesPerfCustInd", sender: self)
            }

        }
            }
    
    func getSalesItemInd()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED_PRODUCTWISE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode + "&CUSTOMER=" + custCode +  "&FRMDATE="  + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Sales Ind", self.salesCustIndArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfCustInd", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "INV_NO" || elementName == "INV_DATE" || elementName == "CUST_NAME" || elementName == "MFG_NAME" || elementName == "PR_NAME" || elementName == "SO_BATCH_NO" || elementName == "Qty" || elementName == "VALUE"){
                noData = false
                elementValue = String()
                element = elementName
                salesCustInd[elementName] = ""
            }
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "INV_NO" || element == "INV_DATE" || element == "CUST_NAME" || element == "MFG_NAME" || element == "PR_NAME" || element == "SO_BATCH_NO" || element == "Qty" || element == "VALUE"){
                if (string.rangeOfString("\n") == nil){
                    
                    salesCustInd[element!] = string
                    
                    
                }
                
            }
            
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "VALUE" {
            count = 0
            salesCustIndArray.addObject(salesCustInd)
            
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    

    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
       // performSegueWithIdentifier("toGraphSalePerfCustDet", sender: self)
        performSegueWithIdentifier("toPieSalesPerfCustDet", sender: self)
    }
    
    

}
