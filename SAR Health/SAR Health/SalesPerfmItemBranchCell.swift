//
//  SalesPerfmItemBranchCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 25/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfmItemBranchCell: UITableViewCell {
    @IBOutlet var no: UILabel!
    @IBOutlet var custName: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var category: UILabel!

   
}
