//
//  CorporateOutTotalCell.swift
//  SAR Health
//
//  Created by Anargha K on 04/08/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutTotalCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    @IBOutlet var manufacture: UILabel!
    @IBOutlet var qty: UILabel!
    @IBOutlet var sales: UILabel!
    
    
}
