//
//  SalesPerfCustSummerized.swift
//  SAR Health
//
//  Created by Prajeesh KK on 26/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfCustSummerized: UIViewController, NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var salesTable: UITableView!
    @IBOutlet var totalText: UILabel!
    @IBOutlet var saleText: UILabel!
    @IBOutlet var amountText: UILabel!
    @IBOutlet var qtyTot: UILabel!
    
    @IBOutlet var titleText: UILabel!
    var amount = [Double](), manfact = [String](), custName = "", count = 0, custCode = "", branchCode = "", cellNo = 0
    var noDataArray : NSMutableArray = [], noDataDic = [String :String](), mfgArray : NSMutableArray = [], qty = 0
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var custDetArray : NSMutableArray = []
    var custDet = [String:String](), manCode = "", salesManfactArray : NSMutableArray = [], salesManfact = [String:String]()
    var salesCust : NSMutableArray = []
    var total : Double = 0
    var fromDate = ""
    var toDate = "", branchName = ""
    var num = NSNumberFormatter()
    //    var no = [String]()
    //    var branch = [String]()
    //    var quantity = [String]()
    //    var amount = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
      //  self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        if cellNo == 1 {
            for(var  i = 0; i < salesCust.count; i += 1){
                total = total + Double((salesCust[i].valueForKey("VALUE") as? String)!)!
                qty = qty + Int((salesCust[i].valueForKey("SO_QTY") as? String)!)!
            }
            
            qtyTot.text = String(qty)
            saleText.text = "Total sales in " + branchName + " from " + fromDate + " to " + toDate + " is "
            amountText.text = "₹ " + num.stringFromNumber(total)!
            totalText.text = num.stringFromNumber(total)!
            titleText.text = "Sales Performance (Customer)"

        }
        else {
            for(var  i = 0; i < salesCust.count; i += 1){
                total = total + Double((salesCust[i].valueForKey("AMOUNT") as? String)!)!
                qty = qty + Int((salesCust[i].valueForKey("Qty") as? String)!)!
            }
            
            qtyTot.text = String(qty)
            saleText.text = "Total sales in " + branchName + " from " + fromDate + " to " + toDate + " is "
            amountText.text = "₹ " + num.stringFromNumber(total)!
            totalText.text = num.stringFromNumber(total)!
            titleText.text = "Sales Performance(Manufacture)"

        }
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
      /*  if segue.identifier == "toGraphSalesPerfCustSum" {
            amount.removeAll()
            manfact.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Manufacturer"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Customer"
            if custName == "" {
                dest.cusProdValue = "All"

            }
            else {
                dest.cusProdValue = custName

            }
                        dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Customers)"
            for(var  i = 0; i < salesCust.count; i += 1){
                amount.append(Double((salesCust[i].valueForKey("VALUE") as? String)!)!)
                manfact.append((salesCust[i].valueForKey("MFG_NAME") as? String)!)
            }
            dest.chartData = amount
            dest.chartLegend = manfact
        }*/
        if segue.identifier == "toPieSalesPerfCustSumm" {
            if cellNo == 1 {
                amount.removeAll()
                manfact.removeAll()
                let dest = segue.destinationViewController as! PieChartViewController
                dest.fromDateStr = fromDate
                dest.toDateStr = toDate
                dest.to = "To"
                dest.titleTab = "Sales Performance (Customers)"
                for(var  i = 0; i < salesCust.count; i += 1){
                    amount.append(Double((salesCust[i].valueForKey("VALUE") as? String)!)!)
                    manfact.append((salesCust[i].valueForKey("MFG_NAME") as? String)!)
                }
                dest.chartData = amount
                dest.chartLegend = manfact
            }
            else {
                amount.removeAll()
                manfact.removeAll()
                let dest = segue.destinationViewController as! PieChartViewController
                dest.fromDateStr = fromDate
                dest.toDateStr = toDate
                dest.to = "To"
                dest.titleTab = "Sales Performance(Manufacture)"
                for(var  i = 0; i < salesCust.count; i += 1){
                    amount.append(Double((salesCust[i].valueForKey("AMOUNT") as? String)!)!)
                    manfact.append((salesCust[i].valueForKey("MANUFATURE") as? String)!)
                }
                dest.chartData = amount
                dest.chartLegend = manfact

            }

        }
        if segue.identifier == "toSalesPerfCustDet" {
            let dest = segue.destinationViewController as? SalesPerfCustDetailed
            if cellNo == 1 {
                dest?.custArray = custDetArray
            }
            else {
                dest?.custArray = salesManfactArray
            }
            dest?.cellNo = cellNo
            dest?.fromDate = fromDate
            dest?.toDate = toDate
            dest?.branchName = branchName
            
            dest?.branchCode = branchCode
            dest?.custName = custName
            
            
            
            
        }


        }
    
    
    
    //MARK: -Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salesCust.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalePerfCustCell", forIndexPath: indexPath) as! SalesPerfCustSumCell
        if cellNo == 1 {
            cell.no.text = String(indexPath.row + 1)
            cell.manfact.text = salesCust[indexPath.row].valueForKey("MFG_NAME") as? String
            cell.quantity.text = salesCust[indexPath.row].valueForKey("SO_QTY") as? String
            cell.amount.text = num.stringFromNumber(Double((salesCust[indexPath.row].valueForKey("VALUE") as? String)!)!)
        }
        else {
            cell.no.text = String(indexPath.row + 1)
            cell.manfact.text = salesCust[indexPath.row].valueForKey("MANUFATURE") as? String
            cell.quantity.text = salesCust[indexPath.row].valueForKey("Qty") as? String
            cell.amount.text = num.stringFromNumber(Double((salesCust[indexPath.row].valueForKey("AMOUNT") as? String)!)!)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if cellNo == 1 {
            custDetArray.removeAllObjects()
            custCode = salesCust[indexPath.row].valueForKey("CUST_CODE") as! String
            manCode = salesCust[indexPath.row].valueForKey("PR_MFG_ID") as! String
            getSalesCustDet();
        }
        else {
             salesManfactArray.removeAllObjects()
            branchCode = salesCust[indexPath.row].valueForKey("brn_id") as! String
            branchName = salesCust[indexPath.row].valueForKey("brn_name") as! String
            manCode = salesCust[indexPath.row].valueForKey("MANUFACTURE_ID") as! String
            getSalesManDet();
        }
       
    }
    
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = salesTable.dequeueReusableCellWithIdentifier("HeaderCell") as! SalesPerfCustSumHeaderCell
        return cell
    }
    
    
    
    
    
    
    func getSalesCustDet()
    {
       
        HUD.showUIBlockingIndicatorWithText("Loading...")
      
        // let scriptUrl = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED?BRANCH=1&COMPANY=1&DIVISION=1&CUSTOMER=nahas&FRMDATE=2016-03-10&TODATE=2016-03-30"
    
        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED?MANUFACTURE=" + manCode + "&BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Cust Det Array",self.custDetArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfCustDet", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getSalesManDet()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        // let scriptUrl = Constants().API + "MOBAPP_CUSTOMER_SALE_DETAILED?BRANCH=1&COMPANY=1&DIVISION=1&CUSTOMER=nahas&FRMDATE=2016-03-10&TODATE=2016-03-30"
        
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_MANUFACTURE_DETAIL?MANUFACTURE=" + manCode + "&BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&FRMDATE=" + fromDate + "&TODATE=" + toDate
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Cust Det Array",self.salesManfactArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfCustDet", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName == "Table1"){
            if elementName == "Column1" {
                noData = false
                elementValue = String()
                element = elementName
                noDataDic[elementName] = ""
            }
        }
            if (elementName == "SO_CUST_CODE" || elementName == "CUST_NAME" || elementName  == "MFG_NAME" || elementName == "PR_NAME" || elementName == "PR_CAT_ID" || elementName == "SO_QTY" || elementName == "VALUE"){
                
                noData = false
                elementValue = String()
                element = elementName
                custDet[elementName] = ""
                
            }
        if (elementName == "PR_MFG_ID" || elementName == "MANUFATURE" || elementName  == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "QTY" || elementName == "AMOUNT"){
            
            noData = false
            elementValue = String()
            element = elementName
            salesManfact[elementName] = ""
            
        }
        }
    
        func parser(parser: NSXMLParser, foundCharacters string: String) {
            if elementValue != nil{
                if (element == "SO_CUST_CODE" || element == "CUST_NAME" || element == "MFG_NAME" || element == "PR_CAT_ID" || element == "PR_NAME" || element == "SO_QTY" || element == "VALUE"){
                    if string.rangeOfString("\n") == nil{
                        if element == "MFG_NAME"{
                            if(count == 0){
                                custDet[element!] = string
                                count += 1
                            }
                        }
                        else{
                            custDet[element!] = string
                        }
                        
                        
                    }
                }
                if (element == "PR_MFG_ID" || element == "MANUFATURE" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "QTY" || element == "AMOUNT"){
                    if string.rangeOfString("\n") == nil{
                         salesManfact[element!] = string
                        
                    }
                }

            }
        }
        
            func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
                if (elementName == "VALUE"){
                   
                    custDetArray.addObject(custDet)
                    count = 0
                }
                if (elementName == "AMOUNT"){
                    
                    salesManfactArray.addObject(salesManfact)
                  
                }
            }
            
        
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        //performSegueWithIdentifier("toGraphSalesPerfCustSum", sender: self)
        performSegueWithIdentifier("toPieSalesPerfCustSumm", sender: self)
    }
    

}
