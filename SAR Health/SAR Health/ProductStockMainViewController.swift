//
//  ProductStockMainViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 11/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit


class ProductStockMainViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSXMLParserDelegate{

        @IBOutlet var branchLabel: UILabel!
    @IBOutlet var productLabel: UITextField!
    @IBOutlet var productTable: UITableView!
    @IBOutlet var branchTable: UITableView!
    @IBOutlet var PriceText: UILabel!
    @IBOutlet var PriceAmt: UILabel!
    @IBOutlet var checkAnother: UIButton!
    @IBOutlet var tabTitle: UILabel!
    @IBOutlet var branchButton: UIButton!
    
    
    var count = 0
    var check : Bool = false
    var branchCode = ""
    var prodCode = "", productName = ""
    var branchList : NSMutableArray = []
    var product : NSMutableArray = []
    var cellNo = 0
    var btag : NSInteger = 0
    var i = 0
    var searchActive : Bool = false
    var filter = [String]()
    var productList = [String]()
    var branch = [String](), branchCount : Int = 0
    var limit = 0
    var noData : Bool = false
    var productExpiry : NSMutableArray = [], productBranch = [String : String](), productBranchArray : NSMutableArray = []
    var expiry = [String:String]()
    var priceArray : NSMutableArray = []
    var price = [String:String]()
    var productStock : NSMutableArray = [], productAllArray : NSMutableArray = [], productAll = [String: String](), productArray : NSMutableArray = []
    //var elementProduct : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var flag : Bool!
    var stock = [String : String]()
    var sampleProduct = [String : String]()
    var arrayValue : NSDictionary = [String : String]()
    var num = NSNumberFormatter(), finyear = [String:String](), finyearArray : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        print("ProductList", product)
        branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
        if branchList.count == branchCount {
            branch = ["All"]
        }
        else {
            branch = []
        }
        if Reachability.isConnectedToNetwork(){
            getFinyear()
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        btag = branchButton.tag
        for ( i = 0; i < product.count ; i += 1){
            
            productList.append((product[i].valueForKey("PRODUCT") as? String)!)
            
        }
        for(i = 0; i < branchList.count; i += 1){
            branch.append((branchList[i].valueForKey("BRN_NAME") as? String)!)
        }
       // productList = ["Product 1", "Product 2", "Product 3"]
        //branchList = ["All", "Branch 1", "Branch 2", "Branch 3", "a", "a", "a","a","a","a","a","a","a","a","a","a","a","","",""]
        productTable.hidden = true
        self.branchTable.tableFooterView = UIView.init(frame: CGRectZero)
        self.productTable.tableFooterView = UIView.init(frame: CGRectZero)
//        let tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
//        tapGesture.cancelsTouchesInView = true
//        self.view.addGestureRecognizer(tapGesture)
        self.branchTable.hidden = true;
        // Do any additional setup after loading the view.
        PriceText.hidden = true
        PriceAmt.hidden = true
        if cellNo == 0{
            checkAnother.hidden = true
            tabTitle.text = "Product Stock"
           
        }
        else if cellNo == 1{
            checkAnother.hidden = false
            tabTitle.text = "Sales Prices"
        }
        else if cellNo == 2{
            checkAnother.hidden = false
            tabTitle.text = "Purchase Prices"
        }
        else if cellNo == 3{
            checkAnother.hidden = true
            tabTitle.text = "Product Expiry"
        }
    
      print("Product",productList)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toProductAll"{
            let dest = segue.destinationViewController as! ProductStockViewController
            dest.productName = productLabel.text!
            dest.productStock = productStock
            dest.productCode = prodCode
            dest.branchArray = branchList
            dest.branchLabel = branchLabel.text!
            dest.productStockBranch = productBranchArray
            branchLabel.text = "Branch"
            productLabel.text = ""
            
        }
        else if segue.identifier == "toProductBranch"{
            let dest = segue.destinationViewController as! ProductStockBranchViewController
            dest.productName = productLabel.text!
            dest.branchName = branchLabel.text!
            dest.stockNo = (productStock[0].valueForKey("STOCK") as? String)!
            
            branchLabel.text = "Branch"
            productLabel.text = ""
        
        }
        else if segue.identifier == "toProductExpiryAll"{
          
                let dest = segue.destinationViewController as! ProductExpiryAllViewController
                dest.productName = productLabel.text!
                dest.productExpiry = productExpiry
            dest.prodCode = prodCode
                branchLabel.text = "Branch"
                productLabel.text = ""

        }
        else if segue.identifier == "toProductExpiry"{
            let dest = segue.destinationViewController as! ProductExpiryBranch
            dest.branchName = branchLabel.text!
            dest.productName = productLabel.text!
            dest.productExpiry = productAllArray
            branchLabel.text = "Branch"
            productLabel.text = ""
        }
        else if segue.identifier == "toSalesPriceAll" {
            let dest = segue.destinationViewController as! SalesPriceController
            dest.salesPrice = priceArray
            dest.productName = productName
            dest.cellNo = cellNo
        }
    }

    //Mark: - Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == productTable){
            if(searchActive){
                return filter.count
            }
            return productList.count;
        }
        else{
            
            return branch.count;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(tableView == productTable){
            let cell = productTable.dequeueReusableCellWithIdentifier("ProductCell", forIndexPath: indexPath) as! ProductTableViewCell
            if(searchActive){
                cell.productTitle.text = filter[indexPath.row]
            }
            else{
                cell.productTitle.text = productList[indexPath.row]
            }
            
            return cell;

        }
        else{
            let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! BranchTableSelectCell
            //print("Array",branchList)
           // print("count", branchList.count)
            //print("Indexpath", indexPath.row)
           
         
            cell.branchName.text = branch[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(tableView == productTable){
            if(searchActive){
                productLabel.text = filter[indexPath.row]
            }
            else{
                productLabel.text = productList[indexPath.row]
            }
           
            for (var i = 0 ; i < product.count; i += 1){
                //print("Product Row",product[indexPath.row].valueForKey("PRODUCT") as? String)
                if product[i].valueForKey("PRODUCT") as? String == productLabel.text {
                    prodCode = (product[i].valueForKey("PRODUCT_CODE") as? String)!
                }

            }
            
            view.endEditing(true)
        }
        else{
            branchLabel.text = branch[indexPath.row]
            if branchLabel.text != "All"{
            
                branchCode = (branchList[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
            }
            else {
                branchCode = "0"
            }
            
            branchTable.hidden = true
        }
        
    }
    
    
   //Mark: - Textfields Methods
    func textFieldDidBeginEditing(textField: UITextField) {
        productTable.hidden = false;
        branchTable.hidden = true
        searchActive = true
        textField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        textFieldDidChange(textField)
           }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        productTable.hidden = true;
        searchActive = false
    }
    
    
    func textFieldDidChange(textField:UITextField){
        if textField.text?.characters.count >= 2{
        filter = productList.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
                   searchActive = true
        
        
        self.productTable.reloadData()
            productTable.hidden = false
    }
        else {
            productTable.hidden = true
        }
    }
    
    func dismissKeyboard (){
        view.endEditing(true)
    }
    
    
    
    @IBAction func findPressed(sender: AnyObject) {
        productStock.removeAllObjects()
        priceArray.removeAllObjects()
        productExpiry.removeAllObjects()
        productAllArray.removeAllObjects()
        productBranchArray.removeAllObjects()
        if (branchLabel.text == "Branch" && productLabel.text == ""){
            Common().showAlert(Constants().APP_NAME, message: "Please Select Branch and Product",viewController: self)
        }
        
        else if branchLabel.text == "Branch"{
            Common().showAlert(Constants().APP_NAME, message: "Please Select Branch",viewController: self)
        }
        else if productLabel.text == ""{
            Common().showAlert(Constants().APP_NAME, message: "Please Select Product",viewController: self)
        }

        else if Reachability.isConnectedToNetwork(){
            
            
            if cellNo == 0{
            getDataProductAll()
            
            
//            if branchLabel.text == "All"{
//                
//               
//                //self.performSegueWithIdentifier("toProductAll", sender: self)
//            }
//            else{
//                // getDataProductAll()
//                if check == false{
//                
//                    // print(productStock)
//                //self.performSegueWithIdentifier("toProductBranch", sender: self)
//               }
//                else {
//                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//                }
//            }
//
            
        }
        else if cellNo == 1{
            getSalesPurchasePrice()
        }
        else if cellNo == 2{
            getSalesPurchasePrice()
            
        }
        else if cellNo == 3{
            getProductExpiry()
            }
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
        }
        
    }
    
    
    @IBAction func branchButton(sender: AnyObject) {
        self.productTable.hidden = true
        view.endEditing(true)
               if(btag == 0){
            self.branchTable.hidden = true
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                var frame = self.branchTable.frame
                frame.size.height = 0;
                self.branchTable.frame = frame
                }, completion:{finished in} )
            btag = 1;
        }
        else{
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                                var frame = self.branchTable.frame;
                                frame.size.height = 175;
                                  self.branchTable.frame = frame;
                }, completion: { finished in })
            self.branchTable.hidden = false
            self.productTable.hidden = true
            btag = 0;
        }
        
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    

    @IBAction func checkAnotherPressed(sender: AnyObject) {
        branchLabel.text = "Branch"
        productLabel.text = ""
        PriceAmt.hidden = true
        PriceText.hidden = true

    }
    
    
    func getDataProductAll ()
    {
       
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCTSTOCK?BRANCH="+branchCode+"&PRODUCT="+prodCode+"&FINYEAR=7"
        let scriptUrl1 = Constants().API + "MOBAPP_PRODUCTSTOCK?BRANCH=" + branchCode + "&PRODUCT=" + prodCode + "&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
//            if (data!.length == 38){
//                self.check = false
//            }
//            else{
//                self.check = true
//            }
            print("data=\(data?.length)")
            let parserProduct = NSXMLParser(data: data!)
            parserProduct.delegate = self
            parserProduct.parse()
                        //Convert server json response to NSDictionary
//                        do {
//                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
//            
//                                // Print out dictionary
//                                print(convertedJsonIntoDict)
//            
//                                // Get value by key
//                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
//                                print(firstNameValue!)
//            
//                            }
//                        }
//                        catch let error as NSError {
//                            print(error.localizedDescription)
//                        }
            dispatch_async(dispatch_get_main_queue(), {
              //  if self.branchLabel.text == "All"{
                    
                    if self.noData == true {
                        HUD.hideUIBlockingIndicator()
                        
                        Common().showAlert(Constants().APP_NAME, message: "No data found..", viewController: self)
                        
                        
                    }
                    else {
                        print(self.productStock)
                        
                        self.performSegueWithIdentifier("toProductAll", sender: self)
                    }

                    
                //}
               /* else{
                    //getDataProductAll()
                    if self.noData == true {
                        HUD.hideUIBlockingIndicator()
                        Common().showAlert(Constants().APP_NAME, message: "No data found..", viewController: self)
                        
                        
                    }
                    else {
                        print(self.productStock)
                        self.performSegueWithIdentifier("toProductBranch", sender: self)
                    }
                }*/

            })

            
            
        }
        
        task.resume()
        
        
    }
    
    
    func getSalesPurchasePrice ()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_SELLING_PURCHASE_PRICE?BRANCH="+branchCode+"&COMPANY=1&DIVISION=1&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_SELLING_PURCHASE_PRICE?BRANCH=0" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            if (self.noData == true){
                self.check = false
            }
            else{
                self.check = true
            }
            print("data=\(data?.length)")
            let parserProduct = NSXMLParser(data: data!)
            parserProduct.delegate = self
            parserProduct.parse()
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
                            
            dispatch_async(dispatch_get_main_queue(), {

            if self.cellNo == 1{
                
                if self.check == true{
                    if self.branchLabel.text == "All" {
                        HUD.hideUIBlockingIndicator()
                        self.productName = self.productLabel.text!
                        self.branchLabel.text = "Branch"
                        self.productLabel.text = ""
                        self.performSegueWithIdentifier("toSalesPriceAll", sender: self)
                    }
                    else {
                        self.PriceText.text = "The sale price of " + self.productLabel.text! + " is"
                        self.PriceAmt.text = "₹ " + self.num.stringFromNumber(Float((self.priceArray[0].valueForKey("PRODUCT_SELL_RATE") as? String)!)!)!
                        self.PriceText.hidden = false
                        self.PriceAmt.hidden = false
                        HUD.hideUIBlockingIndicator()
                    }
                    
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Value Found", viewController: self)
                    self.PriceText.hidden = true
                    self.PriceAmt.hidden = true
                    
                }

            }
            else if self.cellNo == 2{
                if self.branchLabel.text == "All" {
                if self.check == true {
                    self.productName = self.productLabel.text!
                    self.branchLabel.text = "Branch"
                    self.productLabel.text = ""
//                    self.PriceText.text = "The purchase price of " + self.productLabel.text! + " is"
//                    self.PriceAmt.text = "₹ " + self.num.stringFromNumber(Float((self.priceArray[0].valueForKey("PRODUCT_BUY_RATE") as? String)!)!)!
//                    self.PriceText.hidden = false
//                    self.PriceAmt.hidden = false
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPriceAll", sender: self)
                }
                else{
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Value Found", viewController: self)
                    self.PriceText.hidden = true
                    self.PriceAmt.hidden = true
                }
                }
                else {
                    self.PriceText.text = "The purchase price of " + self.productLabel.text! + " is"
                    self.PriceAmt.text = "₹ " + self.num.stringFromNumber(Float((self.priceArray[0].valueForKey("PRODUCT_BUY_RATE") as? String)!)!)!
                    self.PriceText.hidden = false
                    self.PriceAmt.hidden = false
                    HUD.hideUIBlockingIndicator()
                }
            }
            })
       
            
        }
        
        task.resume()
        
        
    }

    func getProductExpiry()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        let scriptUrl1 = Constants().API + "MOBAPP_PRODUCT_EXPIRY?BRANCH="+branchCode+"&PRODUCT="+prodCode+"&FINYEAR=" + finyearCode
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_EXPIRY?BRANCH=1&PRODUCT=8260"
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parserProduct = NSXMLParser(data: data!)
            parserProduct.delegate = self
            parserProduct.parse()
            dispatch_async(dispatch_get_main_queue(), {
            
            if (self.noData == true){
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No data found..", viewController: self)
                
            }
            else{
                if self.branchLabel.text == "All"{
                    
                    HUD.hideUIBlockingIndicator()
                    self.productArray = self.productExpiry
                    print(self.productExpiry)
                    self.performSegueWithIdentifier("toProductExpiryAll", sender: self)
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    self.productArray = self.productAllArray
                     print(self.productAllArray)
                    self.performSegueWithIdentifier("toProductExpiry", sender: self)
                    
                }
            }
                })

            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
//            if self.branchLabel.text == "All"{
//                
//                
//                self.performSegueWithIdentifier("toProductAll", sender: self)
//            }
//            else{
//                //getDataProductAll()
//                if self.check == true{
//                    
//                    print(self.productStock)
//                    self.performSegueWithIdentifier("toProductBranch", sender: self)
//                }
//                else {
//                    HUD.hideUIBlockingIndicator()
//                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//                    
//                }
//            }
//            
            
        }
        
        task.resume()
        
        
    }
    
    func getFinyear()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_FINYEAR"
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            print("Finyeararray",self.finyearArray)
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }


    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }

            if (elementName == "BRANCH" || elementName == "STOCK"){
                noData = false
                elementValue = String()
                element = elementName
                stock[elementName] = ""
            }
             if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
                noData = false
                elementValue = String()
                element = elementName
                price[elementName] = ""
                
            }
            if (elementName == "BRN_NO" || elementName == "PR_CAT_ID" || elementName == "BRANCH" || elementName == "STOCK"){
                noData = false
                elementValue = String()
                element = elementName
                expiry[elementName] = ""
                
            }
            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "EXPIRY" || elementName == "STOCK"){
                noData = false
                elementValue = String()
                element = elementName
                productAll[elementName] = ""
                
            }
            if (elementName == "FIN_YEAR_ID" || elementName == "FINYEAR") {
                noData = false
                elementValue = String()
                element = elementName
                finyear[elementName] = ""
            }
            if (elementName == "batch" || elementName == "stock"){
                noData = false
                elementValue = String()
                element = elementName
                productBranch[elementName] = ""
            }

            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "BRANCH" || element == "STOCK"){
                if (string.rangeOfString("\n") == nil){
                    stock[element!] = string
                }
                
            }
             if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
                if (string.rangeOfString("\n") == nil){
                    price[element!] = string
                }
            }
            if (element == "BRN_NO" || element == "PR_CAT_ID" || element == "BRANCH" || element == "STOCK"){
                if (string.rangeOfString("\n") == nil){
                    expiry[element!] = string
                }
            }
            if (element == "BRANCH" || element == "BATCH" || element == "EXPIRY" || element == "STOCK"){
                if (string.rangeOfString("\n") == nil){
                    productAll[element!] = string
                }
            }
            if ((element == "FIN_YEAR_ID" || element == "FINYEAR")) {
                if (string.rangeOfString("\n") == nil) {
                    finyear[element!] = string
                }
            }
            if (element == "batch" || element == "stock") {
                if (string.rangeOfString("\n") == nil) {
                    productBranch[element!] = string
                }
            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "STOCK" {
            productStock.addObject(stock)
        }
        if elementName == "PRODUCT_SELL_RATE" {
            priceArray.addObject(price)
        }
        if (elementName == "STOCK"){
            productExpiry.addObject(expiry)
        }
        if (elementName == "stock") {
            productBranchArray.addObject(productBranch)
        }
        if (elementName == "EXPIRY"){
            productAllArray.addObject(productAll)
        }
        if (elementName == "FINYEAR"){
            finyearArray.addObject(finyear)
        }
        
    }

}






