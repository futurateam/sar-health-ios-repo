//
//  CorporateOutSalesMain.swift
//  SAR Health
//
//  Created by Anargha K on 27/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSalesMain: UIViewController, UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate {
    @IBOutlet var outstandingTable: UITableView!
    @IBOutlet var outstandingText: UILabel!
    @IBOutlet var amountText: UILabel!
    @IBOutlet var totText: UILabel!
    @IBOutlet var qtyTot: UILabel!
    
    var elementValue: String?
    var element : String?
    var sum : Double = 0, manfactName = "", noData : Bool = true
    var corpDet : NSMutableArray = [], branchCode = "", prodCode = "", manCode = "", outstandingInd = [String : String](), outstandingIndArray : NSMutableArray = [], customerArray : NSMutableArray = [], custCode = "", corporate = [String : String](), corporateArray : NSMutableArray = [], corporateTot = [String : String](), corporateTotArray : NSMutableArray = []
    var no = ["No."]
    var branch = ["Branch"]
    var customer = ["Customer"]
    var qtyText = ["Qty"], custId = [""], prodId = [""], branchId = [""], mfgId = [""]
    var fromDate = "" , toDate = ""
    var value = ["Sales (₹)"], brn = [String]()
    var customerLegend = [String](), amount = [Double]()
    var  i = 0, qtyTotal : Int = 0
    var j = 0
    var count = 0
    var limit = 1
    var total : Double = 0, totFlag = false
    var customerName = "", num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        for (i = 0 ; i < corpDet.count ; i++) {
            no.append(String(i + 1))
            branch.append(corpDet[i].valueForKey("brn_name") as! String)
           // customer.append(corpDet[i].valueForKey("CUST_COMPANY_GROUP") as! String)
            qtyText.append(corpDet[i].valueForKey("Qty") as! String)
            value.append(num.stringFromNumber(Double((corpDet[i].valueForKey("amount") as? String)!)!)!)
            //custId.append(corpDet[i].valueForKey("SO_CUST_CODE") as! String)
           
            branchId.append(corpDet[i].valueForKey("brn_id") as! String)
            //mfgId.append(corpDet[i].valueForKey("MANUFACTURE_ID") as! String)
            total = total + Double((corpDet[i].valueForKey("amount") as? String)!)!
            qtyTotal = qtyTotal + Int((corpDet[i].valueForKey("Qty") as! String))!
            
        }
        //        for(var  i = 0; i < salesCust.count; i++){
        //            total = total + Float((salesCust[i].valueForKey("VALUE") as? String)!)!
        //        }
        //
        totText.text = num.stringFromNumber(total)
        qtyTot.text = String(qtyTotal)
        outstandingText.text = "Total sales for " + customerName + " is "
        amountText.text = "₹ " + num.stringFromNumber(total)!
        //        totalText.text = String(total)
        //
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
      /*  if segue.identifier == "toGraphCorporateOutDet"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Customer"
            dest.yaxisName = "Outstanding"
            dest.cusprodText = "Customer"
            
            dest.cusProdValue = customerName
            
            dest.from = ""
            dest.to = ""
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("CUSTOMER") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("Outstanding") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
            
        }*/
        
        if segue.identifier == "toPieCorporateSales"{
            customerLegend.removeAll()
            amount.removeAll()
            let dest = segue.destinationViewController as! PieChartViewController
            dest.fromDateStr = fromDate
            dest.toDateStr = toDate
            dest.to = "To"
            dest.titleTab = "Corporate Customers (Sales)"
            for(var  i = 0; i < corpDet.count; i += 1){
                customerLegend.append((corpDet[i].valueForKey("brn_name") as? String)!)
                amount.append(Double((corpDet[i].valueForKey("amount") as? String)!)!)
            }
            dest.chartData = amount
            dest.chartLegend = customerLegend
        }
      
        if segue.identifier == "toCorporateOutDet" {
            let dest = segue.destinationViewController as! CorporateOutSales
            dest.corpDet = corporateArray
            dest.outstandingCust = customerName
            dest.corpTotal = corporateTotArray
            dest.fromDate = fromDate
            dest.toDate = toDate
            dest.branchCode = branchCode
            dest.manCode = manCode
            dest.customerName = customerName
            
            
        }

    }
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = outstandingTable.dequeueReusableCellWithIdentifier("CorporateOutSalesMainCell", forIndexPath: indexPath) as! CorporateOutSalesMainCell
        //        cell.no.text = no[indexPath.row]
        //        cell.product.text = prName[indexPath.row]
        //        cell.quantity.text = quantity[indexPath.row]
        //       // cell.amount.text = value[indexPath.row]
        //        if no[indexPath.row] == "No." {
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        //        }
        //        else {
        //
        //            cell.amount.text = value[indexPath.row]
        //            cell.backgroundColor = UIColor.whiteColor()
        //        }
        if indexPath.row == 0 {
            
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.branch.attributedText = attributedString
            
           /* longestWordRange = (customer[indexPath.row] as NSString).rangeOfString(customer[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: customer[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.customer.attributedText = attributedString*/
            longestWordRange = (qtyText[indexPath.row] as NSString).rangeOfString(qtyText[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: qtyText[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.qty.attributedText = attributedString
            
            longestWordRange = (value[indexPath.row] as NSString).rangeOfString(value[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: value[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            
            cell.sales.attributedText = attributedString
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.no.text = no[indexPath.row]
            cell.branch.text = branch[indexPath.row]
            //cell.customer.text = customer[indexPath.row]
            cell.qty.text = qtyText[indexPath.row]
            cell.sales.text = value[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
        //        if no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "" {
        //            let longestWordRange = (prName[indexPath.row] as NSString).rangeOfString(prName[indexPath.row])
        //
        //            let attributedString = NSMutableAttributedString(string: prName[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        //
        //            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        //            cell.product.attributedText = attributedString
        //        }
        
        //        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && quantity[indexPath.row] == "" && value[indexPath.row] == "" && prName[indexPath.row] != "") || (quantity[indexPath.row].rangeOfString("Total") != nil){
        //            cell.selectionStyle = UITableViewCellSelectionStyle.None
        //        }
        //        else {
        //            cell.selectionStyle = UITableViewCellSelectionStyle.Gray
        //        }
        
        
        
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        outstandingIndArray.removeAllObjects()
        corporateTot.removeAll()
        corporateTotArray.removeAllObjects()
        if indexPath.row == 0 {
            
        }
        else if (prodCode == "0"){
            branchCode = branchId[indexPath.row]
            
            //manCode = mfgId[indexPath.row]
            getCorporateOut()
            
            
          
            //getCorporateOutSumInd()
        }
        
    }
    
    
     func getCorporateOut()
        
    {
        corporateArray.removeAllObjects()
        corporate.removeAll()
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + customerName + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.corporateArray)
            if self.corporateArray.count == 0 {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    self.totFlag = false
                    //self.getCorporateTotal()
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    
    func getCorporateTotal()
        
    {
       
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE_DETAIL_CONSOLIDATED?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + customerName + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            self.totFlag = true

            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.corporateTotArray)
            if self.corporateTotArray.count == 0 {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                    
                    
                })
                
            }

            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    
    
    

       
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "INV_NO" || elementName == "INV_DATE" || elementName == "CUST_NAME" || elementName == "BATCH_NO" || elementName == "QTY" || elementName == "AMOUNT"){
                noData = false
                elementValue = String()
                element = elementName
                outstandingInd[elementName] = ""
            }
        }
        if elementName == "CUST_COMPANY_GROUP" || elementName == "SO_CUST_CODE" || elementName == "CUST_NAME" || elementName == "brn_id" || elementName == "brn_name" || elementName == "PR_MFG_ID" || elementName == "MANUFATURE" || elementName == "Qty" || elementName == "AMOUNT" {
            noData = false
            
                elementValue = String()
                element = elementName
                corporate[elementName] = ""

            
        }
        
       /* if elementName == "MANUFATURE" || elementName == "Qty" || elementName == "AMOUNT" {
            noData = false
            if totFlag == true {
                elementValue = String()
                element = elementName
                corporateTot[elementName] = ""
            }
           
        }*/

    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "INV_NO" || element == "INV_DATE" || element == "CUST_NAME" || element == "BATCH_NO" || element == "QTY" || element == "AMOUNT"){
                if (string.rangeOfString("\n") == nil){
                    outstandingInd[element!] = string
                }
                
            }
        }
        if element == "CUST_COMPANY_GROUP" || element == "SO_CUST_CODE" || element == "CUST_NAME" || element == "brn_id" || element == "brn_name" || element == "PR_MFG_ID" || element == "MANUFATURE" || element == "Qty" || element == "AMOUNT" {
            if string.rangeOfString("\n") == nil{
              
                    corporate[element!] = string
                
                
            }
        }
       /* if element == "MANUFATURE" || element == "Qty" || element == "AMOUNT" {
            if string.rangeOfString("\n") == nil{
                if totFlag == true {
                     corporateTot[element!] = string
                }
               
            }
        }*/

    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "AMOUNT" {
            // count = 0
            outstandingIndArray.addObject(outstandingInd)
            
        }
        print(outstandingInd)
        if elementName == "AMOUNT" {
           
                corporateArray.addObject(corporate)
            
        }

        
    }
    
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        // performSegueWithIdentifier("toGraphCorporateOutDet", sender: self)
        performSegueWithIdentifier("toPieCorporateSales", sender: self)
    }
    
    

}
