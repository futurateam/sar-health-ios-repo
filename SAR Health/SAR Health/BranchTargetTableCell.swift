//
//  BranchTargetTableCell.swift
//  SAR Health
//
//  Created by Anargha K on 23/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class BranchTargetTableCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    @IBOutlet var fromPeriod: UILabel!
    @IBOutlet var toPeriod: UILabel!
    
    @IBOutlet var targetLabel: UILabel!
    @IBOutlet var sales: UILabel!
    @IBOutlet var acheivement: UILabel!
    
    
    
    
}
