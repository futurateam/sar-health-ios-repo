//
//  SalesPerfCustDetCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 27/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfCustDetCell: UITableViewCell {
   
    
    @IBOutlet var category: UILabel!
    @IBOutlet var no: UILabel!
    @IBOutlet var product: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var amount: UILabel!
}
