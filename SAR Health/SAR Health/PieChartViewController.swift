//
//  PieChartViewController.swift
//  SAR Health
//
//  Created by Anargha K on 28/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit
import Charts

class PieChartViewController: UIViewController {
    
    @IBOutlet var fromDate: UILabel!
    @IBOutlet var toDate: UILabel!
    @IBOutlet var toText: UILabel!
    
    @IBOutlet var titleText: UILabel!
    
    
    @IBOutlet var pieChartView: PieChartView!
    var chartLegend = [String]()
    var chartData = [Double]()
    var fromDateStr = "", toDateStr = "", to = "", titleTab = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fromDate.text = fromDateStr
        toDate.text = toDateStr
        toText.text = to
        titleText.text = titleTab
        // Do any additional setup after loading the view.
        
        //let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        //let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0,20.0, 4.0, 6.0, 3.0, 12.0, 16.0]
        
        setChart(chartLegend, values: chartData)
        
    }

    func setChart(dataPoints: [String], values: [Double]) {
      
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
     
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "")
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        pieChartView.animate(xAxisDuration: 2.0)
               pieChartView.data = pieChartData
       
        var colors: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        
        pieChartDataSet.colors = colors
        
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}


