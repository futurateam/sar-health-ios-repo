//
//  CorporateOutSummInd.swift
//  SAR Health
//
//  Created by Anargha K on 17/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateOutSummInd: UIViewController {

    @IBOutlet var salesTable: UITableView!
    @IBOutlet weak var totel: UILabel!
    
   
    var billNo = ["Bill No"], billDate = ["Bill Date"], customer = ["Customer Name"], days = ["Days"], outstanding = ["Outstanding(₹)"]
    var num = NSNumberFormatter(), tot : Double = 0
    var outstandingInd : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for (var i = 0 ; i < outstandingInd.count ; i += 1){
            billNo.append(outstandingInd[i].valueForKey("BillNo") as! String)
            billDate.append(outstandingInd[i].valueForKey("BillDate") as! String)
            customer.append(outstandingInd[i].valueForKey("CUSTOMER") as! String)
            days.append(outstandingInd[i].valueForKey("Days") as! String)
            
            outstanding.append(num.stringFromNumber(Double((outstandingInd[i].valueForKey("Outstanding") as? String)!)!)!)
            tot = tot + Double((outstandingInd[i].valueForKey("Outstanding") as? String)!)!
            
            
        }
        totel.text = num.stringFromNumber(tot)
        
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billNo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("CorporateOutIndCell", forIndexPath: indexPath) as! CorporateOutSummIndCell
        cell.category.text = customer[1]
        if(billNo[indexPath.row] == "Bill No"){
            cell.category.backgroundColor = UIColor.whiteColor()
            cell.billNo.attributedText = headFont(billNo[indexPath.row])
            cell.billDate.attributedText = headFont(billDate[indexPath.row])
            //cell.customerName.attributedText = headFont(customer[indexPath.row])
            cell.days.attributedText = headFont(days[indexPath.row])
            cell.outstanding.attributedText = headFont(outstanding[indexPath.row])
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.category.text = ""
            cell.billNo.text = billNo[indexPath.row]
            cell.billDate.text = billDate[indexPath.row]
            //cell.customerName.text = customer[indexPath.row]
            cell.days.text = days[indexPath.row]
            cell.outstanding.text = outstanding[indexPath.row]
            
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
    }
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    
   
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

}
