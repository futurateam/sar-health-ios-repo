//
//  SalesPerformanceItem.swift
//  SAR Health
//
//  Created by Prajeesh KK on 19/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerformanceItem: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    @IBOutlet var branchLabel: UILabel!
    @IBOutlet var itemLabel: UITextField!
    @IBOutlet var branchTable: UITableView!
    @IBOutlet var itemTable: UITableView!
    @IBOutlet var fromDate: UITextField!
    @IBOutlet var toDate: UITextField!
    @IBOutlet var datePickerView: UIView!
    @IBOutlet var fromDateButton: UIButton!
    @IBOutlet var tabTitle: UILabel!
    @IBOutlet var toDateButton: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var creditText: UILabel!
    @IBOutlet var creditAmount: UILabel!
    @IBOutlet var checkAnother: UIButton!
    @IBOutlet var branchButton: UIButton!
    
    var salesItemAllArray : NSMutableArray = [], noData : Bool = false
    var salesItemAll = [String:String]()
    var salesItemBranchArray : NSMutableArray = []
    var salesItemBranch = [String : String]()
    var customerArray : NSMutableArray = []
    var customer = [String : String]()
    var purchase = [String : String]()
    var purchaseArray : NSMutableArray = []
    //var elementProduct : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var in_from_date_day = 0, in_from_date_month = 0, in_from_date_year = 0
    var cellNo = 0
    var branchCode = ""
    var prodCode = ""
    var custCode = ""
    var i = 0
    var formatDate = ""
    var format = ""
    var branch : NSMutableArray = [], creditNoteArray : NSMutableArray = [], creditNote = [String : String]()
    var branchName = [String](), branchCount : Int = 0
    var dateFormatter = NSDateFormatter()
    var flag : Bool = false
    var btag : NSInteger = 0
    var btag1 = 0
    var searchActive : Bool = false
    var filter = [String]()
    var itemArray = [String]()
    var customerfilter = [String](), num = NSNumberFormatter()
    //var branchList = [String]()
    //var itemlist = [String]()
    var itemList : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
        if branch.count == branchCount {
            branchName = ["All"]
        }
        else {
            branchName = []
        }
        btag = branchButton.tag
        self.itemTable.tableFooterView = UIView.init(frame: CGRectZero)
        if cellNo == 0 {
            itemLabel.attributedPlaceholder = NSAttributedString(string: "Item", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Item)"
            creditText.hidden = true
            creditAmount.hidden = true
            checkAnother.hidden = true
        }
//        else if cellNo == 4{
//            
//            itemLabel.attributedPlaceholder = NSAttributedString(string: "Customer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
//            tabTitle.text = "Purchase Value(Customers)"
//            creditText.hidden = true
//            creditAmount.hidden = true
//            checkAnother.hidden = true
//
//        }
        else if cellNo == 5 {
            branchName = ["All"]
            itemLabel.attributedPlaceholder = NSAttributedString(string: "Customer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Credit Note"
            creditText.hidden = true
            creditAmount.hidden = true
            checkAnother.hidden = true
        }

        datePickerView.hidden = true
        //branchList = ["All", "Branch 1", "Branch 2", "Branch 3"]
       // itemlist = ["Item 1", "Item 2", "Item 3"]
        itemTable.hidden = true
        branchTable.hidden = true
        // Do any additional setup after loading the view.
        dateFormatter.dateFormat = "yyyy-MM-dd"
       datePicker.maximumDate = NSDate()
        for(i = 0; i < branch.count; i += 1){
            branchName.append((branch[i].valueForKey("BRN_NAME") as? String)!)
        }
        for(i = 0; i < itemList.count; i += 1){
            itemArray.append((itemList[i].valueForKey("PRODUCT") as? String)!)
        }
       branchLabel.text = "Branch"
        itemLabel.text = ""
        fromDate.text = ""
        toDate.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
//        if segue.identifier == "toSalesPerfmAllItem" {
//            let dest = segue.destinationViewController as? SalePerformanceItemAll
//            dest?.salesItem = salesItemAllArray
//            dest?.fromDate = fromDate.text!
//            dest?.toDate = toDate.text!
//        }
//        else if segue.identifier == "toSalesPerfBranch"{
//            let dest = segue.destinationViewController as? SalesPerformanceItemBranch
//            dest?.salesItem = salesItemBranchArray
//            dest?.branchName = branchLabel.text!
//            dest?.fromDate = fromDate.text!
//            dest?.toDate = toDate.text!
//            dest?.productName = itemLabel.text!
//        }
//        else 
//        if segue.identifier == "toPurchaseSale" {
//            let dest = segue.destinationViewController as! PurchaseValueController
//            dest.purchaseValue = purchaseArray
//            dest.custName = itemLabel.text!
//            dest.fromDate = fromDate.text!
//            dest.toDate = toDate.text!
//        }
        if segue.identifier == "toCreditNote" {
            let dest = segue.destinationViewController as! CreditNoteController
            dest.custName = itemLabel.text!
            dest.creditNote = creditNoteArray
            custCode = ""
            branchCode = ""
            branchLabel.text = "Branch"
            itemLabel.text = ""
            
            fromDate.text = ""
            toDate.text = ""
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
         HUD.hideUIBlockingIndicator()
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        HUD.hideUIBlockingIndicator()
        if(tableView == itemTable){
            if(searchActive){
                return filter.count
            }
            if cellNo == 0{
                return itemArray.count;
            }
            else {
                return customerfilter.count
            }

        }
        else{
            return branchName.count;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(tableView == itemTable){
            let cell = itemTable.dequeueReusableCellWithIdentifier("ItemCell", forIndexPath: indexPath) as! SalesItemCell
            if cellNo == 0 {
                
                if(searchActive){
                    cell.itemName.text = filter[indexPath.row]
                }
                else{
                    cell.itemName.text = itemArray[indexPath.row]
                }
            }
            else {
                if(searchActive){
                    cell.itemName.text = filter[indexPath.row]
                }
                else{
                    cell.itemName.text = customerfilter[indexPath.row]
                }
            }
            
            return cell;
            

            
        }
        else{
            let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! SalesItemBranchCell
            cell.branchName.text = branchName[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(tableView == itemTable){
//            if cellNo == 0{
//                if(searchActive){
//                    itemLabel.text = filter[indexPath.row]
//                }
//                else{
//                    itemLabel.text = itemArray[indexPath.row]
//                }
//                if itemLabel.text == ""{
//                    
//                    prodCode = "0"
//                }
//                else{
//                    for (var i = 0; i < salesItemBranchArray.count; i++){
//                        if salesItemBranchArray[i].valueForKey("PRODUCT") as? String == itemLabel.text{
//                            prodCode = (salesItemBranchArray[i].valueForKey("PRODUCT_CODE") as? String)!
//                        }
//                    }
//                }
//            }
//            else
            if  cellNo == 5{
                if(searchActive){
                    itemLabel.text = filter[indexPath.row]
                }
                else{
                    itemLabel.text = customerfilter[indexPath.row]
                }
                if itemLabel.text == ""{
                    
                    custCode = "0"
                }
                else{
                    print(customerArray)
                    for (var i = 0; i < customerArray.count; i += 1){
                        if customerArray[i].valueForKey("CUSTOMER") as? String == itemLabel.text{
                            custCode = (customerArray[i].valueForKey("CODE") as? String)!
                        }
                    }
                }
            }
            
            
            
            view.endEditing(true)
        }
        else{
            branchLabel.text = branchName[indexPath.row]
            
            if  cellNo == 5{
                print(branch)
                if branchLabel.text == "All" {
                    branchCode = "0"
                }
                else {
                     branchCode = (branch[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
                }
               
                filter.removeAll()
                getCustomerList()
                
            }
           
            
            
            
            branchTable.hidden = true
        }
        
    }
    
    
    //Mark: - Textfields Methods
    func textFieldDidBeginEditing(textField: UITextField) {
        if  cellNo == 5{
            for(var i = 0; i < self.customerArray.count; i += 1){
                self.customerfilter.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            }

        }
        flag = false
        branchTable.hidden = true
        datePickerView.hidden = true
        searchActive = true
        textField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        textFieldDidChange(textField)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        itemTable.hidden = true;
        searchActive = false
    }
    
    
    func textFieldDidChange(textField:UITextField){
        if flag == false{
            flag = true
        }
        if textField.text?.characters.count >= 2{
            if cellNo == 0{
                if flag == true{
                    itemTable.hidden = false;
                    filter = itemArray.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    self.itemTable.reloadData()
                }
            }
            else if cellNo == 5{
                
                
                if flag == true{
                    itemTable.hidden = false;
                   
                    filter = customerfilter.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    print(filter)
                    for (var i=0; i < self.filter.count; i += 1 ) {
                        for (var j=(i+1); j < self.filter.count; j += 1) {
                            if ((self.filter[i] == self.filter[j])) {
                                self.filter.removeAtIndex(j)
                                j -= 1;
                            }
                        }
                    }

                    HUD.hideUIBlockingIndicator()
                    self.itemTable.reloadData()
                }
                
                
            }

        }
        else {
            itemTable.hidden = true
        }
        
    }

    
    @IBAction func branchButton(sender: AnyObject) {
        self.itemTable.hidden = true
        view.endEditing(true)
        if(btag == 0){
            self.branchTable.hidden = true
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                var frame = self.branchTable.frame
                frame.size.height = 0;
                self.branchTable.frame = frame
                }, completion:{finished in} )
            btag = 1;
        }
        else{
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                var frame = self.branchTable.frame;
                frame.size.height = 175;
                self.branchTable.frame = frame;
                }, completion: { finished in })
            self.branchTable.hidden = false
            btag = 0;
        }

    }

    @IBAction func findPressed(sender: AnyObject) {
        salesItemAllArray.removeAllObjects()
        salesItemBranchArray.removeAllObjects()
        creditNoteArray.removeAllObjects()
        if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
            Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
        }
        
        else if Reachability.isConnectedToNetwork(){
            if cellNo == 4 {
            getPurchaseValue()
        }
        else if cellNo == 5{
                
            getCreditNote()
        }
        }
        else {
            Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection", viewController: self)
        }
        
    }

    @IBAction func fromDatePressed(sender: AnyObject) {
        btag1 = fromDateButton.tag
        datePickerView.hidden = false
        view.endEditing(true)
    }
    
    @IBAction func toDatePressed(sender: AnyObject) {
        datePicker.setDate(NSDate(), animated: true)

        btag1 = toDateButton.tag
        datePickerView.hidden = false
        view.endEditing(true)
    }
    
    @IBAction func datePickerAction(sender: AnyObject) {
         formatDate = dateFormatter.stringFromDate(datePicker.date)
        if btag1 == 1 {
            fromDate.text = formatDate
        }
        else {
            toDate.text = formatDate
        }
    }
    
    @IBAction func pickerDonePressed(sender: AnyObject) {
        format = dateFormatter.stringFromDate(NSDate())
        print(NSDate())
        if dateFormatter.stringFromDate(datePicker.date) == dateFormatter.stringFromDate(NSDate()){
            
            if btag == 1 {
                
                fromDate.text = format
            }
            else {
                toDate.text = format
            }
            
        }
            
        else {
            // dateValidation(btag)
        }
        datePickerView.hidden = true
    }
    
  /*  func dateValidation (tag: Int){
       // let currentDate = NSDate()
        dateFormatter.dateFormat = "yyyy"
        //let mYear = Int(dateFormatter.stringFromDate(currentDate))
        let selectedYear = Int(dateFormatter.stringFromDate(datePicker.date))
        dateFormatter.dateFormat = "MM"
        //let mMonth = Int(dateFormatter.stringFromDate(currentDate))
        let selectedMonth = Int(dateFormatter.stringFromDate(datePicker.date))
        dateFormatter.dateFormat = "dd"
        //let mDay = Int(dateFormatter.stringFromDate(currentDate))
        let selectedDay = Int(dateFormatter.stringFromDate(datePicker.date))
        if (tag == 1) {
            in_from_date_day = selectedDay!
            in_from_date_month = selectedMonth!
            in_from_date_year = selectedYear!
            dateFormatter.dateFormat = "yyyy-MM-dd"
            fromDate.text = dateFormatter.stringFromDate(datePicker.date)
        }
        if(tag == 0){
            if(in_from_date_month > 3){
                if(selectedMonth > in_from_date_month){
                    if(selectedYear == in_from_date_year){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else if(selectedYear! + 1 == in_from_date_year){
                        if(selectedMonth < 3){
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            toDate.text = dateFormatter.stringFromDate(datePicker.date)
                        }
                        else {
                            Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                            toDate.text = ""
                        }
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else if(selectedYear == in_from_date_year + 1){
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    toDate.text = dateFormatter.stringFromDate(datePicker.date)
                }
                else {
                    if(selectedDay > in_from_date_day){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
            }
            else {
                if(selectedYear == in_from_date_year){
                    if(selectedMonth < 3){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else if(selectedMonth == in_from_date_month){
                        if(selectedDay > in_from_date_day){
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            toDate.text = dateFormatter.stringFromDate(datePicker.date)
                        }
                        else {
                            Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                            toDate.text = ""
                        }
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else if(selectedYear! - 1 == in_from_date_year){
                    if(selectedMonth! + 1 < in_from_date_month){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else{
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else{
                    Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                    toDate.text = ""
                }
            }
        }
        
        
    }*/

    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func checAnotherPressed(sender: AnyObject) {
        creditAmount.hidden = true
        creditText.hidden = true
        branchLabel.text = "Branch"
        itemLabel.text = ""
    }
    
    
//    func getSalesItemAll()
//    {
//        
//        HUD.showUIBlockingIndicatorWithText("Loading...")
//        var scriptUrl = ""
//        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
//        if branchLabel.text == "All"{
//             scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH=0&COMPANY=1&DIVISION=1&PRODUCT=8260&FRMDATE=2016-03-10&TODATE=2016-03-30"
//
//        }
//        else{
//            scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE_DETAIL?BRANCH=1&COMPANY=1&DIVISION=1&PRODUCT=8260&FRMDATE=2016-03-10&TODATE=2016-03-30"
//        }
//                print("url",scriptUrl)
//        // Add one parameter
//        //let urlWithParams = scriptUrl + "?BRANCH=1"
//        
//        // Create NSURL Ibject
//        let myUrl = NSURL(string: scriptUrl);
//        
//        // Creaste URL Request
//        let request = NSMutableURLRequest(URL:myUrl!);
//        
//        // Set request HTTP method to GET. It could be POST as well
//        request.HTTPMethod = "GET"
//        
//        // If needed you could add Authorization header value
//        // Add Basic Authorization
//        /*
//        let username = "myUserName"
//        let password = "myPassword"
//        let loginString = NSString(format: "%@:%@", username, password)
//        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
//        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
//        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
//        */
//        
//        // Or it could be a single Authorization Token value
//        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
//        
//        // Excute HTTP Request
//        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
//            data, response, error in
//            // Check for error
//            if error != nil
//            {
//                print("error=\(error)")
//                return
//            }
//            
//            // Print out response string
//            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//            print("responseString = \(responseString)")
//            
//            
//            
//            print("data=\(data?.length)")
//            let parser = NSXMLParser(data: data!)
//            parser.delegate = self
//            parser.parse()
//            print("Sales Branch",self.salesItemBranchArray)
//            if (data!.length == 38){
//                HUD.hideUIBlockingIndicator()
//                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//                
//                
//            }
//            else{
//                dispatch_async(dispatch_get_main_queue(), {
//                    if self.branchLabel.text == "All"{
//                        
//                        HUD.hideUIBlockingIndicator()
//                        self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
//                    }
//                    else {
//                        HUD.hideUIBlockingIndicator()
//                        self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
//                        
//                    }
//                    
//                })
//            }
//            
//            //Convert server json response to NSDictionary
//            //                        do {
//            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
//            //
//            //                                // Print out dictionary
//            //                                print(convertedJsonIntoDict)
//            //
//            //                                // Get value by key
//            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
//            //                                print(firstNameValue!)
//            //
//            //                            }
//            //                        }
//            //                        catch let error as NSError {
//            //                            print(error.localizedDescription)
//            //                        }
//            //            if self.branchLabel.text == "All"{
//            //
//            //
//            //                self.performSegueWithIdentifier("toProductAll", sender: self)
//            //            }
//            //            else{
//            //                //getDataProductAll()
//            //                if self.check == true{
//            //
//            //                    print(self.productStock)
//            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
//            //                }
//            //                else {
//            //                    HUD.hideUIBlockingIndicator()
//            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//            //
//            //                }
//            //            }
//            //
//            
//        }
//        
//        task.resume()
//        
//        
//    }
    
    func getCustomerList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMERS?BRANCH=" + branchCode + "&CUSTOMER="
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            dispatch_async(dispatch_get_main_queue(), {
                
                HUD.hideUIBlockingIndicator()
            })
                        print("customer",self.customerArray)
            for (var i=0; i < self.customerArray.count; i += 1 ) {
                for (var j=(i+1); j < self.customerArray.count; j += 1) {
                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
                        self.customerArray.removeObjectAtIndex(i);
                        j -= 1;
                    }
                }
            }
           

            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getPurchaseValue()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        if  itemLabel.text == ""{
            custCode = "0"
        }
        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMER_PURCHASE_VALUE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Purchase Value", self.purchaseArray)
            
            
//                        if (data!.length == 38){
//                            HUD.hideUIBlockingIndicator()
//                            Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//            
//            
//                        }
//                        else{
                            dispatch_async(dispatch_get_main_queue(), {
                                if self.noData == true {
                                    HUD.hideUIBlockingIndicator()
                                    Common().showAlert(Constants().APP_NAME, message: "No Data Found..", viewController: self)
                                }
                                else{
                                    HUD.hideUIBlockingIndicator()
                                    self.performSegueWithIdentifier("toPurchaseSale", sender: self)
                                }
                                
            
                            })
                        //}
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getCreditNote()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        if custCode == ""{
            custCode = "0"
        }
        if branchLabel.text == "All" {
            branchCode = "0"
        }
    
        let scriptUrl1 = Constants().API + "MOBAPP_SALE_RETURN?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode +  "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
           // print("Purchase Value", self.creditNoteArray)
                       HUD.hideUIBlockingIndicator()
            dispatch_async(dispatch_get_main_queue(), {
                if self.creditNoteArray.count == 0{
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No data", viewController: self)
                    self.custCode = ""
                    self.branchCode = ""
                   self.branchLabel.text = "Branch"
                    self.itemLabel.text = ""
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCreditNote", sender: self)
                    print("Credit Note", self.creditNoteArray)
                }
                
            })
            //                        if (data!.length == 38){
            //                            HUD.hideUIBlockingIndicator()
            //                            Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //                        }
            //                        else{
//            dispatch_async(dispatch_get_main_queue(), {
//                HUD.hideUIBlockingIndicator()
//                self.performSegueWithIdentifier("toPurchaseSale", sender: self)
//                
//                
//                
//            })
            //}
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    


    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
//            if (elementName == "BRANCH" || elementName == "QTY" || elementName == "VALUE"){
//                elementValue = String()
//                element = elementName
//                salesItemAll[elementName] = ""
//            }
//            if (elementName == "SLNO" || elementName == "CUST_NAME" || elementName == "QTY" || elementName == "VALUE") {
//                elementValue = String()
//                element = elementName
//                salesItemBranch[elementName] = ""
//            }
            if (elementName == "Table1") {
                noData = true
            }
            if (elementName == "CODE" || elementName == "CUSTOMER"){
                elementValue = String()
                element = elementName
                customer[elementName] = ""
            }
            if (elementName == "DATE" || elementName == "CUSTOMER" || elementName == "AMOUNT") {
                elementValue = String()
                element = elementName
                purchase[elementName] = ""
            }
            if (elementName == "BRN_ID" || elementName == "BRANCH" || elementName == "CUST_ID" || elementName == "CUSTOMER" || elementName == "AMOUNT" || elementName == "Bill_No" || elementName == "Txn_Id"){
                elementValue = String()
                element = elementName
                creditNote[elementName] = ""
            }
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
//                elementValue = String()
//                element = elementName
//                price[elementName] = ""
//                
//            }
//            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
//                elementValue = String()
//                element = elementName
//                expiry[elementName] = ""
//                
//            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
//            if (element == "BRANCH" || element == "QTY" || element == "VALUE"){
//                if (string.rangeOfString("\n") == nil){
//                    salesItemAll[element!] = string
//                }
//               
//            }
//            
//            if (element == "SLNO" || element == "CUST_NAME" || element == "QTY" || element == "VALUE"){
//                if string.rangeOfString("\n") == nil{
//                    salesItemBranch[element!] = string
//                }
//            }
            if (element == "CODE" || element == "CUSTOMER"){
                if string.rangeOfString("\n") == nil{
                    customer[element!] = string
                }
            }
            
            if (element == "DATE" || element == "CUSTOMER" || element == "AMOUNT") {
                if string.rangeOfString("\n") == nil {
                    purchase[element!] = string
                }
            }
            if (element == "BRN_ID" || element == "BRANCH" || element == "CUSTOMER"  || element == "CUST_ID" || element == "AMOUNT" || element == "Bill_No" || element == "Txn_Id") {
                if string.rangeOfString("\n") == nil {
                    creditNote[element!] = string
                }
            }
//            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
//                if (string.rangeOfString("\n") == nil){
//                    price[element!] = string
//                }
//            }
//            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
//                if (string.rangeOfString("\n") == nil){
//                    expiry[element!] = string
//                }
//            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
//        if elementName == "VALUE" {
//            salesItemAllArray.addObject(salesItemAll)
//            
//        }
//        if elementName == "VALUE" {
//            salesItemBranchArray.addObject(salesItemBranch)
//        }
        if elementName == "CUSTOMER" {
            customerArray.addObject(customer)
        }
        if elementName == "AMOUNT" {
            purchaseArray.addObject(purchase)
        }
        if elementName == "Txn_Id" {
            creditNoteArray.addObject(creditNote)
        }

//        if elementName == "PRODUCT_SELL_RATE" {
//            priceArray.addObject(price)
//        }
//        if (element == "EXPIRY"){
//            productExpiry.addObject(expiry)
//        }
        
    }
    
   
}
