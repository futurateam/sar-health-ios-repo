//
//  OutstandingValueSummCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 29/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class OutstandingValueSummCell: UITableViewCell {

    @IBOutlet var no: UILabel!
    @IBOutlet var customer: UILabel!
    @IBOutlet var outstanding: UILabel!
}
