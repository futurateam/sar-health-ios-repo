//
//  SalesPerformanceCustomer.swift
//  SAR Health
//
//  Created by Prajeesh KK on 26/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerformanceCustomer: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
    @IBOutlet var branchLabel: UILabel!
    @IBOutlet var customerLabel: UITextField!
    @IBOutlet var branchTable: UITableView!
    @IBOutlet var customerTable: UITableView!
    @IBOutlet var fromDate: UITextField!
    @IBOutlet var toDate: UITextField!
    @IBOutlet var datePickerView: UIView!
    @IBOutlet var fromDateButton: UIButton!
    @IBOutlet var toDateButton: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var summerzdImage: UIImageView!
    @IBOutlet var detaildImage: UIImageView!
    @IBOutlet var summrzdButton: UIButton!
    @IBOutlet var detaildButton: UIButton!
    @IBOutlet var tabTitle: UILabel!
    @IBOutlet var summLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var branchButton: UIButton!
    
    @IBOutlet var summImgtoCust: NSLayoutConstraint!
    @IBOutlet var summImgHeight: NSLayoutConstraint!
  
    @IBOutlet var summbtnHeight: NSLayoutConstraint!
    
    @IBOutlet var fromLabeltoCust: NSLayoutConstraint!
    @IBOutlet var toLabeltoCust: NSLayoutConstraint!
    
    @IBOutlet var fromLabel: UILabel!
    @IBOutlet var fromText: UITextField!
    @IBOutlet var toLabel: UILabel!
    @IBOutlet var toText: UITextField!
    @IBOutlet var toCal: UIImageView!
    @IBOutlet var fromCal: UIImageView!
    
    var count = 0, countBranchSum = 0, countBranchDet = 0
    var salesItemAllArray : NSMutableArray = [], branchtgt = [String : String](), branchtgtArray : NSMutableArray = []
    var salesItemAll = [String:String]()
    var customer = [String : String]()
    var salesItemBranchArray : NSMutableArray = [], noDataArray : NSMutableArray = [], noDataDic = [String :String]()
    var customerArray : NSMutableArray = []
    var salesItemBranch = [String : String](), outstandingCust = "", corporate = [String : String](), corporateArray : NSMutableArray = []
    //var elementProduct : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var custCode = ""
    var noData : Bool = false, c : Int = 0
    var in_from_date_day = 0
    var in_from_date_month = 0
    var in_from_date_year = 0
    var manCode = ""
    var radiotag1 = 0
    var radiotag2 = 1
    var radioText = "summerized"
    var branchCode = "", execCode = ""
    var prodCode = ""
    var i = 0
    var formatDate = ""
    var format = ""
    var branch : NSMutableArray = []
    var branchName = [String](), branchCount : Int = 0
    var custSummArray : NSMutableArray = [], outstandingSumArray : NSMutableArray = [], outstandingDetArray : NSMutableArray = []
    var custSumm = [String:String](), outstandingSum = [String : String](), outstandingDet = [String : String](), salesManfact = [String:String](), salesManfactArray : NSMutableArray = []
    var custDetArray : NSMutableArray = []
    var custDet = [String:String]()
    var branchSumArray : NSMutableArray = [], executiveList = [String : String](), executiveListArray : NSMutableArray = [], finyearArray : NSMutableArray = []
    var branchSum = [String : String]()
    var branchDetArray : NSMutableArray = []
    var branchDet = [String : String]()
    var manfactArray : NSMutableArray = []
    var manfact = [String : String](), executiveSumm = [String : String](), executiveSummArray : NSMutableArray = []
    var dateFormatter = NSDateFormatter()
    var flag : Bool = false
    var btag : NSInteger = 0
    var searchActive : Bool = false
    var filter = [String](), execFilter = [String]()
    var itemArray = [String](), productFilter = [String]()
    var manfactfilter = [String]()
    var cellNo = 0
    var largeNumber = 36000
    //var branchList = [String]()
    //var itemlist = [String]()
    var itemList : NSMutableArray = []
    
    var num = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        btag = branchButton.tag
        self.customerTable.tableFooterView = UIView.init(frame: CGRectZero)
        branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
        print("Number",num.stringFromNumber(largeNumber)!)
      
        if branch.count >= branchCount {
            branchName = ["All"]
        }
        else {
            branchName = []
        }
        if cellNo == 0 {
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Item", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Product)"
        }
        if cellNo == 1 {
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Customer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Customer)"
            branchName = []
        }
        else if cellNo == 2{
            if Reachability.isConnectedToNetwork(){
           // getManfactList()
            }
            else{
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
            }
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Manufacturer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Branch)"
        }
        else if cellNo == 3 {
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0

            customerLabel.attributedPlaceholder = NSAttributedString(string: "Executive", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Executive)"
            
        }
        else if cellNo == 5{
            branchName = []
            fromLabel.hidden = true
            toLabel.hidden = true
            fromText.hidden = true
            toText.hidden = true
            fromCal.hidden = true
            toCal.hidden = true
            fromDateButton.hidden = true
            toDateButton.hidden = true
           
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Customer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Outstanding Report"
        }
        else if cellNo == 4 {
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Manufacturer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Corporate Customers (Sales)"
            
        }
        else if cellNo == 8{
            if Reachability.isConnectedToNetwork(){
                // getManfactList()
            }
            else{
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try again.", viewController: self)
            }
            summrzdButton.hidden = true
            summerzdImage.hidden = true
            summLabel.hidden = true
            detaildButton.hidden = true
            detaildImage.hidden = true
            detailLabel.hidden = true
            fromLabeltoCust.constant = 17.0
            toLabeltoCust.constant = 22.0
            customerLabel.attributedPlaceholder = NSAttributedString(string: "Manufacturer", attributes: [NSForegroundColorAttributeName: UIColor.blackColor()])
            tabTitle.text = "Sales Performance(Manufacture)"
        }

        for(i = 0; i < itemList.count; i += 1){
            productFilter.append((itemList[i].valueForKey("PRODUCT") as? String)!)
        }
        datePickerView.hidden = true
        //branchList = ["All", "Branch 1", "Branch 2", "Branch 3"]
        // itemlist = ["Item 1", "Item 2", "Item 3"]
        customerTable.hidden = true
        branchTable.hidden = true
        // Do any additional setup after loading the view.
        dateFormatter.dateFormat = "yyyy-MM-dd"
        datePicker.maximumDate = NSDate()
        for(i = 0; i < branch.count; i += 1){
            branchName.append((branch[i].valueForKey("BRN_NAME") as? String)!)
        }
//        for(i = 0; i < itemList.count; i++){
//            itemArray.append((itemList[i].valueForKey("PRODUCT") as? String)!)
//        }
        branchLabel.text = "Branch"
        customerLabel.text = ""
        fromDate.text = ""
        toDate.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toSalesPerfCustSum"{
            let dest = segue.destinationViewController as? SalesPerfCustSummerized
            if cellNo == 8 {
                  dest?.salesCust = branchSumArray
            }
            else {
                dest?.salesCust = custSummArray

            }
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.custName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.custCode = custCode
            dest?.branchCode = branchCode
            dest?.mfgArray = manfactArray
            dest?.cellNo = cellNo
            branchLabel.text = "Branch"
            custCode = ""
            branchCode = ""
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
        }
        if segue.identifier == "toSalesManufacture" {
             let dest = segue.destinationViewController as? SalesManufacture
            dest?.outstandingValue = branchSumArray
            dest?.manCode = manCode
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.manfactArray = manfactArray
            dest?.custmerName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.branchCode = branchCode
            dest?.custArray = customerArray
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
        }
               if segue.identifier == "toSalesPerfBranchSumm" {
            let dest = segue.destinationViewController as? SalesPerfBranchSumm
            dest?.salesBranch = branchSumArray
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.manfactName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.mfgArray = manfactArray
            dest?.branchCode = branchCode
                dest?.manCode = manCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
            manCode = ""

        }
        if segue.identifier == "toSalesPerfBranchSummAll" {
            let dest = segue.destinationViewController as? SalesPerfBranchSummAll
            dest?.salesBranch = branchSumArray
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.manfactName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.mfgArray = manfactArray
            dest?.branchArray = branch
            dest?.manCode = manCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
            manCode = ""
            
        }
              if segue.identifier == "toSalesPerfDetAll" {
            let dest = segue.destinationViewController as? SalesPerfBranchDeatAll
            dest?.salesItem = branchDetArray
            dest?.branchName = branchLabel.text!
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.manfactName = customerLabel.text!
            dest?.branchCode = branchCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""

        }
        if segue.identifier == "toOutstandingSumm" {
            let dest = segue.destinationViewController as? OutstandingValueSumm
            dest?.outstandingValue = outstandingSumArray
            dest?.custCode = custCode
            dest?.custmerName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.branchCode = branchCode
            dest?.custArray = customerArray
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""

            
        }
                if segue.identifier == "toSalesPerfSumm" {
            let dest = segue.destinationViewController as? SalePerformanceItemAll
            dest?.salesItem = salesItemAllArray
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.productName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            dest?.branchArray = branch;
            
            dest?.productCode = prodCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""

        }
        if segue.identifier == "toSalesPerfItemDet"{
            let dest = segue.destinationViewController as? SalesPerformanceItemBranch
            dest?.salesItem = salesItemBranchArray
            dest?.branchName = branchLabel.text!
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.productName = customerLabel.text!
            dest?.salesDet = salesItemBranchArray
            //dest?.branchCode = branchCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""

        }
        if segue.identifier == "toSalesPerfItemBranchDet" {
            let dest = segue.destinationViewController as? SalesPerfItemBrachDetViewController
            dest?.salesItem = salesItemBranchArray
            dest?.branchName = branchLabel.text!
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.productName = customerLabel.text!
            dest?.salesDet = salesItemBranchArray
            //dest?.branchCode = branchCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
        }
        
        
        if segue.identifier == "toSalesPerfExecSumm" {
            let dest = segue.destinationViewController as? SalesPerfExecSumm
            dest?.branchName = branchLabel.text!
            dest?.execName = customerLabel.text!
            dest?.fromDate = fromDate.text!
            dest?.toDate = toDate.text!
            dest?.salesSumm = executiveSummArray
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
        }
        if segue.identifier == "toOutstandingDet" {
            let dest = segue.destinationViewController as? OutstandingValueDet
            dest?.outstandingValue = outstandingDetArray
            dest?.custCode = custCode
            dest?.custmerName = customerLabel.text!
            dest?.branchName = branchLabel.text!
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
            
        }
        if segue.identifier == "toCorporateOutDet" {
            let dest = segue.destinationViewController as! CorporateOutSalesMain
            dest.corpDet = corporateArray
            dest.customerName = outstandingCust
            dest.fromDate = fromDate.text!
            dest.toDate = toDate.text!
            for (var i = 0; i < manfactArray.count; i += 1){
                if manfactArray[i].valueForKey("MFG_NAME") as? String == customerLabel.text{
                    manCode = (manfactArray[i].valueForKey("PR_MFG_ID") as? String)!
                }
            }
            dest.manCode = manCode
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
            custCode = ""
            branchCode = ""
        }

    
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == customerTable){
            if(searchActive){
                return filter.count
            }
            if cellNo == 1 || cellNo == 5{
            return itemArray.count;
            }
            else {
                return manfactfilter.count
            }
        }
        else{

            return branchName.count;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(tableView == customerTable){
            let cell = customerTable.dequeueReusableCellWithIdentifier("CustomerCell", forIndexPath: indexPath) as! SalesPerformanceCustomerCell
                                    if cellNo == 1 || cellNo == 5{
                
                if(searchActive){
                    cell.customerName.text = filter[indexPath.row]
                }
                else{
                    cell.customerName.text = itemArray[indexPath.row]
                }
            }
            else if cellNo == 3 {
                if(searchActive){
                    cell.customerName.text = filter[indexPath.row]
                }
                else{
                    cell.customerName.text = execFilter[indexPath.row]
                }
                                    
            }
            else {
                if(searchActive){
                    cell.customerName.text = filter[indexPath.row]
                }
                else{
                    cell.customerName.text = manfactfilter[indexPath.row]
                }
            }
            
            return cell;
            
        }
        else{
            let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! SalesPerformanceBranchCell
            cell.branchName.text = branchName[indexPath.row]
            return cell
        }
        
    }
    
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(tableView == customerTable){
            if cellNo == 0 {
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = productFilter[indexPath.row]
                }
                if customerLabel.text == ""{
                    
                    prodCode = "0"
                }
                else{
                    for (var i = 0; i < itemList.count; i += 1){
                        if itemList[i].valueForKey("PRODUCT") as? String == customerLabel.text{
                            prodCode = (itemList[i].valueForKey("PRODUCT_CODE") as? String)!
                        }
                    }
                }
            }

            if cellNo == 1 || cellNo == 5{
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = itemArray[indexPath.row]
                }
                if customerLabel.text == ""{
            
                    custCode = "0"
                }
                else{
                    for (var i = 0; i < customerArray.count; i += 1){
                        if customerArray[i].valueForKey("CUSTOMER") as? String == customerLabel.text{
                                custCode = (customerArray[i].valueForKey("CODE") as? String)!
                        }
                    }
                }
            }
            else if cellNo == 2  || cellNo == 8{
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = manfactfilter[indexPath.row]
                }
               
                    print("Manfact",manfactArray)
                    for (var i = 0; i < manfactArray.count; i += 1){
                        if manfactArray[i].valueForKey("MFG_NAME") as? String == customerLabel.text{
                            manCode = (manfactArray[i].valueForKey("PR_MFG_ID") as? String)!
                        }
                    }
                    print(manCode)
                
            }
            else if cellNo == 4 {
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = manfactfilter[indexPath.row]
                }
                
                print("Manfact",manfactArray)
                for (var i = 0; i < manfactArray.count; i += 1){
                    if manfactArray[i].valueForKey("MFG_NAME") as? String == customerLabel.text{
                        manCode = (manfactArray[i].valueForKey("PR_MFG_ID") as? String)!
                    }
                }
                print(manCode)

            }
            else if cellNo == 3 {
                if(searchActive){
                    customerLabel.text = filter[indexPath.row]
                }
                else{
                    customerLabel.text = execFilter[indexPath.row]
                }
                if customerLabel.text == ""{
                    
                    execCode = "0"
                }
                else{
                    
                    for (var i = 0; i < executiveListArray.count; i += 1){
                        if executiveListArray[i].valueForKey("NAME") as? String == customerLabel.text{
                            execCode = (executiveListArray[i].valueForKey("SO_INQUIRY_BY") as? String)!
                        }
                    }
                }

            }
            
            
            
            view.endEditing(true)

        }
        else{
            branchLabel.text = branchName[indexPath.row]
            if cellNo == 1 || cellNo == 5{
                branchCode = (branch[indexPath.row].valueForKey("BRN_ID") as? String)!
            }
            else {
                if branchLabel.text == "All"{
                    branchCode = "0"
                }
                else {
                    branchCode = (branch[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
                }

            }
            
            branchTable.hidden = true
            if cellNo == 1 || cellNo == 5{
                filter.removeAll()
                if Reachability.isConnectedToNetwork(){
             getCustomerList()
                }
                else{
                     Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
           // HUD.hideUIBlockingIndicator()
            }
            else if cellNo == 3 {
                execFilter.removeAll()
                if Reachability.isConnectedToNetwork() {
                    branchtgtArray.removeAllObjects()
                    getBranchTarget()
                    getExecutiveList()
                }
                else {
                     Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
            }
        view.endEditing(true)
        }
        
    }
    
    
    //Mark: - Textfields Methods
    func textFieldDidBeginEditing(textField: UITextField) {
        datePickerView.hidden = true
                for(var i = 0; i < self.customerArray.count; i += 1){
            self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
        }
        for(var i = 0; i < self.manfactArray.count; i+=1){
            self.manfactfilter.append((self.manfactArray[i].valueForKey("MFG_NAME") as? String)!)
        }

       

        flag = false
        branchTable.hidden = true
        searchActive = true
        textField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        textFieldDidChange(textField)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        customerTable.hidden = true;
        searchActive = false
    }
    
    
    func textFieldDidChange(textField:UITextField){
        if flag == false{
            flag = true
        }

        if textField.text?.characters.count >= 2{
            if cellNo == 0{
                    if flag == true{
                    customerTable.hidden = false;
                    filter = productFilter.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                        
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    self.customerTable.reloadData()
                }
            }
            else if cellNo == 1 || cellNo == 5{
                
                if flag == true{
                   
                    customerTable.hidden = false;
                    filter = itemArray.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    for (var i=0; i < self.filter.count; i += 1 ) {
                        for (var j=(i+1); j < self.filter.count; j += 1) {
                            if ((self.filter[i] == self.filter[j])) {
                                self.filter.removeAtIndex(j)
                                j -= 1;
                            }
                        }
                    }

                    self.customerTable.reloadData()
                }
            }
            else if cellNo == 2 || cellNo == 4 || cellNo == 8{
                if flag == true{
                    
                    customerTable.hidden = false;
                    filter = manfactfilter.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    for (var i=0; i < self.filter.count; i += 1 ) {
                        for (var j=(i+1); j < self.filter.count; j += 1) {
                            if ((self.filter[i] == self.filter[j])) {
                                self.filter.removeAtIndex(j)
                                j -= 1;
                            }
                        }
                    }
                    self.customerTable.reloadData()
                }
                
                
            }
            else if cellNo == 3{
                if flag == true{
                    
                    customerTable.hidden = false;
                    filter = execFilter.filter({ (text) -> Bool in
                        let tmp: NSString = text
                        let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                        return range.location != NSNotFound
                    })
                    searchActive = true
                    
                    HUD.hideUIBlockingIndicator()
                    
                    self.customerTable.reloadData()
                }
                
            }


        }
                else {
            customerTable.hidden = true
        }
        
    }
    
    
    @IBAction func branchButton(sender: AnyObject) {
        customerTable.hidden = true
       
        view.endEditing(true)
        if(btag == 0){
            self.branchTable.hidden = true
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                var frame = self.branchTable.frame
                frame.size.height = 0;
                self.branchTable.frame = frame
                }, completion:{finished in} )
            btag = 1;
        }
        else{
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                var frame = self.branchTable.frame;
                frame.size.height = 175;
                self.branchTable.frame = frame;
                }, completion: { finished in })
            self.branchTable.hidden = false
            
            btag = 0;
        }
        
        view.endEditing(true)
        
    }
    
    @IBAction func findPressed(sender: AnyObject) {
        customerArray.removeAllObjects()
       corporateArray.removeAllObjects()
        custSummArray.removeAllObjects()
        custDetArray.removeAllObjects()
        branchSumArray.removeAllObjects()
        branchDetArray.removeAllObjects()
        outstandingSumArray.removeAllObjects()
        outstandingDetArray.removeAllObjects()
        salesItemAllArray.removeAllObjects()
        salesItemBranchArray.removeAllObjects()
        executiveListArray.removeAllObjects()
        executiveSummArray.removeAllObjects()
        if cellNo == 0 {
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                getSalesItemAll()
            }
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else  if cellNo == 1{
            if (branchLabel.text == "Branch" || customerLabel.text == "" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                getSalesCustSum()
            }
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else   if cellNo == 2{
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                getSalesBranchSum()
            }
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else if cellNo == 3 {
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                getExecutiveSumm()
            }
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else if cellNo == 4 {
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
               print(branchCode)
                print(manCode)
                print(custCode)
                getCorporateOutSales()
            }
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else   if cellNo == 8{
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "Please Select Branch and Dates",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                    getSalesManfact()
                }
            
                
            else {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }



    if radioText == "summerized"{
        
        if (cellNo == 5){
            if (branchLabel.text == "Branch"){
                Common().showAlert(Constants().APP_NAME, message: "Please Select Branch",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
                getOutstandingSum()
            }
            else{
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }

       
                }
        else if radioText == "detailed"{
        if cellNo == 0 {
            if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
            }
            else if Reachability.isConnectedToNetwork(){
             getSalesItemAll()
            }
            else{
                 Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
        }
        else if cellNo == 1{
                if (branchLabel.text == "Branch" || customerLabel.text == "" || fromDate.text == "" || toDate.text == ""){
                    Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
               
                }
                else{
                     Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
            }
            else if cellNo == 2 {
                if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                    Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
                getSalesBranchDet()
                }
                else{
                     Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
            }
            else if cellNo == 3{
                Common().showAlert(Constants().APP_NAME, message: "Not Implemented", viewController: self)
            }
            else if cellNo == 5 {
                if (branchLabel.text == "Branch"){
                    Common().showAlert(Constants().APP_NAME, message: "Please Select Branch",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
                    getOutstandingDet()
                }
                else{
                     Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
            }
            
        }
//        branchLabel.text = "Branch"
//        customerLabel.text = ""
        filter.removeAll()
        
    }
    
    @IBAction func fromDatePressed(sender: AnyObject) {
        datePicker.reloadInputViews()
        btag = fromDateButton.tag
        datePickerView.hidden = false
        view.endEditing(true)
    }
    
    @IBAction func toDatePressed(sender: AnyObject) {
        
        datePicker.setDate(NSDate(), animated: true)
        btag = toDateButton.tag
        datePickerView.hidden = false
        view.endEditing(true)
    }
    
    @IBAction func datePickerAction(sender: AnyObject) {
        formatDate = dateFormatter.stringFromDate(datePicker.date)
        
        if btag == 1 {
            
            fromDate.text = formatDate
        }
        else {
            toDate.text = formatDate
        }
    }
    
    @IBAction func pickerDonePressed(sender: AnyObject) {
                    format = dateFormatter.stringFromDate(NSDate())
        print(NSDate())
            if dateFormatter.stringFromDate(datePicker.date) == dateFormatter.stringFromDate(NSDate()){
               
                    if btag == 1 {
                        
                        fromDate.text = format
                    }
                    else {
                        toDate.text = format
                    }

                }
            
            else {
               // dateValidation(btag)
                           }
        
        datePickerView.hidden = true
    }
  /*  func dateValidation (tag: Int){
        let currentDate = NSDate()
        dateFormatter.dateFormat = "yyyy"
        //let mYear = Int(dateFormatter.stringFromDate(currentDate))
        let selectedYear = Int(dateFormatter.stringFromDate(datePicker.date))
        dateFormatter.dateFormat = "MM"
        //let mMonth = Int(dateFormatter.stringFromDate(currentDate))
        let selectedMonth = Int(dateFormatter.stringFromDate(datePicker.date))
        dateFormatter.dateFormat = "dd"
        //let mDay = Int(dateFormatter.stringFromDate(currentDate))
        let selectedDay = Int(dateFormatter.stringFromDate(datePicker.date))
        if (tag == 1) {
             in_from_date_day = selectedDay!
             in_from_date_month = selectedMonth!
             in_from_date_year = selectedYear!
             dateFormatter.dateFormat = "yyyy-MM-dd"
            fromDate.text = dateFormatter.stringFromDate(datePicker.date)
        }
        if(tag == 0){
            if(in_from_date_month > 3){
                if(selectedMonth > in_from_date_month){
                    if(selectedYear == in_from_date_year){
                         dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else if(selectedYear! + 1 == in_from_date_year){
                        if(selectedMonth < 3){
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            toDate.text = dateFormatter.stringFromDate(datePicker.date)
                        }
                        else {
                            Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                            toDate.text = ""
                        }
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else if(selectedYear == in_from_date_year + 1){
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    toDate.text = dateFormatter.stringFromDate(datePicker.date)
                }
                else {
                    if(selectedDay > in_from_date_day){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
            }
            else {
                if(selectedYear == in_from_date_year){
                    if(selectedMonth < 3){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else if(selectedMonth == in_from_date_month){
                        if(selectedDay > in_from_date_day){
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            toDate.text = dateFormatter.stringFromDate(datePicker.date)
                        }
                        else {
                            Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                            toDate.text = ""
                        }
                    }
                    else {
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else if(selectedYear! - 1 == in_from_date_year){
                    if(selectedMonth! + 1 < in_from_date_month){
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        toDate.text = dateFormatter.stringFromDate(datePicker.date)
                    }
                    else{
                        Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                        toDate.text = ""
                    }
                }
                else{
                    Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
                    toDate.text = ""
                }
            }
        }
        
        
    }*/
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func summrzdPressed(sender: AnyObject) {
       // radiotag = summrzdButton.tag
        if radiotag1 == 0 {
            summerzdImage.image = UIImage(named: "rdChecked.png")
            detaildImage.image  = UIImage(named: "rdUnChecked.png")
            radioText = "summerized"
            radiotag2 = 1
        }
        else {
            summerzdImage.image = UIImage(named: "rdUnChecked.png")
            detaildImage.image = UIImage(named: "rdChecked.png")
            radioText = "detailed"
            radiotag2 = 0
        }
        
    }
    
    @IBAction func detaildPressed(sender: AnyObject) {
        //radiotag = detaildButton.tag
        if radiotag2 == 0{
            detaildImage.image = UIImage(named: "rdUnChecked.png")
            summerzdImage.image = UIImage(named: "rdChecked.png")
            radioText = "summerized"
            radiotag1 = 1
        }
        else {
            detaildImage.image = UIImage(named: "rdChecked.png")
            summerzdImage.image = UIImage(named: "rdUnChecked.png")
            radioText = "detailed"
            radiotag1 = 0
        }
    }
    
    func getCustomerList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl = Constants().API + "MOBAPP_CUSTOMERS?BRANCH=" + branchCode + "&CUSTOMER="
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
HUD.hideUIBlockingIndicator()
                })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.customerArray)
           
//            for (var i=0; i < self.customerArray.count; i++ ) {
//                for (var j=(i+1); j < self.customerArray.count; j++) {
//                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
//                        self.customerArray.removeObjectAtIndex(i);
//                        j--;
//                    }
//                }
//            }
            

//            if (data!.length == 38){
//                HUD.hideUIBlockingIndicator()
//                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
//                
//                
//            }
//            else{
//                dispatch_async(dispatch_get_main_queue(), {
//                    if self.branchLabel.text == "All"{
//                        
//                        HUD.hideUIBlockingIndicator()
//                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
//                    }
//                    else {
//                        HUD.hideUIBlockingIndicator()
//                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
//                        
//                    }
//                    
//                })
//            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getSalesItemAll()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        if customerLabel.text == ""{
            prodCode = "0"
        }
        var scriptUrl1 = ""
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
       
            scriptUrl1 = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&PRODUCT=" + prodCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
             let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
       
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Sales All",self.salesItemAllArray)
            print("Nodata", self.noDataArray)
            dispatch_async(dispatch_get_main_queue(), {
                print(self.salesItemAll.count)
            if (self.salesItemAllArray.count == 0){
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found..", viewController: self)
                
                
            }
            else{
                
                
                        
                        HUD.hideUIBlockingIndicator()
                        self.performSegueWithIdentifier("toSalesPerfSumm", sender: self)
                
                
                    
                
            }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    
    func getSalesCustSum()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        if(customerLabel.text == ""){
            custCode = "0"
        }
       
        let scriptUrl1 = Constants().API + "MOBAPP_CUSTOMER_SALE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            
            
//            for (var i=0; i < self.customerArray.count; i++ ) {
//                for (var j=(i+1); j < self.customerArray.count; j++) {
//                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
//                        self.customerArray.removeObjectAtIndex(i);
//                        j--;
//                    }
//                }
//            }
//            for(var i = 0; i < self.customerArray.count; i++){
//                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
//            }
            dispatch_async(dispatch_get_main_queue(), {

                        if (self.custSummArray.count == 0){
                            HUD.hideUIBlockingIndicator()
                            Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
            
            
                        }
                        else{
                            
                                    HUD.hideUIBlockingIndicator()
                                    self.performSegueWithIdentifier("toSalesPerfCustSum", sender: self)
                            
            
                            
                        }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    

      
    func getSalesBranchSum()
    {
        if customerLabel.text == ""{
            manCode = "0"
        }
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=0&DIVISION=1&COMPANY=1&MANUFACTURE=3250&FRMDATE=2016-03-10&TODATE=2016-03-30"
       
//        
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode + "&DIVISION=1&COMPANY=1&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
           print("Branch Summ", self.branchSumArray)
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.branchSumArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    if self.branchLabel.text != "All" && self.manCode != "0" {
                        HUD.hideUIBlockingIndicator()
                        self.performSegueWithIdentifier("toSalesPerfBranchSumm", sender: self)
                    }
                    else {
                        HUD.hideUIBlockingIndicator()
                        self.performSegueWithIdentifier("toSalesPerfBranchSummAll", sender: self)
                    }
                    
                   
                    
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getSalesBranchDet()
    {
        if customerLabel.text == ""{
            manCode = "0"
        }
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        //
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            print("Branch Array",self.branchDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.branchDetArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else if self.branchLabel.text == "All" {
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfDetAll", sender: self)
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfBranchDet", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getOutstandingSum()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        if customerLabel.text == "" {
            custCode = "0"
        }
         let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        let scriptUrl1 = Constants().API + "MOBAPP_OUTSTANDING?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&FINYEAR="  + finyearCode + "&CUSTOMER=" + custCode
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Outstaning", self.outstandingSumArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            print("Branch Array",self.branchDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.outstandingSumArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toOutstandingSumm", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func getOutstandingDet()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        if customerLabel.text == "" {
            custCode = "0"
        }
        let scriptUrl1 = Constants().API + "MOBAPP_OUTSTANDING_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //        let urlstrt = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=" + branchCode
        //        let urlmid = "&COMPANY=1&DIVISION=1&CUSTOMER=" + custCode
        //        let urllast = "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        //
        //        let scriptUrl = urlstrt + urlmid + urllast
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("OutstaningDet", self.outstandingDetArray)
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.outstandingDetArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toOutstandingDet", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
   

    func getExecutiveList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        execFilter.removeAll()
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
         let finyearCode = finyearArray[0].valueForKey("FIN_YEAR_ID") as! String
        let scriptUrl1 = Constants().API + "MOBAPP_SALESPERSON?BRANCH=" + branchCode + "&FINYEAR=" + finyearCode + "&SALESPERSON="
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("Executive",self.executiveListArray)
            for (var i = 0 ; i < self.executiveListArray.count ; i++){
                self.execFilter.append((self.executiveListArray[i].valueForKey("NAME") as? String)!)
            }
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    func getExecutiveSumm()
    {
        if customerLabel.text == ""{
            execCode = "0"
        }
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=0&DIVISION=1&COMPANY=1&MANUFACTURE=3250&FRMDATE=2016-03-10&TODATE=2016-03-30"
        
        //
        let scriptUrl1 = Constants().API + "MOBAPP_SALESPERSON_SALE?BRANCH=" + branchCode + "&DIVISION=1&COMPANY=1&SALESPERSON=" + execCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("Branch Summ", self.executiveSummArray)
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.executiveSummArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfExecSumm", sender: self)
                    
                    
                    
                }
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }

    func getBranchTarget()
    {
        if customerLabel.text == ""{
            execCode = "0"
        }
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_BRANCH_SALE?BRANCH=0&DIVISION=1&COMPANY=1&MANUFACTURE=3250&FRMDATE=2016-03-10&TODATE=2016-03-30"
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_TARGET?BRANCH=" + branchCode + "&DIVISION=1&COMPANY=1"
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        //
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("Branch Summ", self.branchtgtArray)
             dispatch_async(dispatch_get_main_queue(), {
            if (self.noData == true) {
                Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
            }
            else {
                if self.branchLabel.text! != "All" {
                    Common().showAlert(Constants().APP_NAME, message: self.branchtgtArray[0].valueForKey("Column1") as! String, viewController: self)
                }
                
            }
             });
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
//            dispatch_async(dispatch_get_main_queue(), {
//                
//                if (self.noData == true){
//                    HUD.hideUIBlockingIndicator()
//                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
//                    
//                    
//                }
//                else{
//                    
//                    HUD.hideUIBlockingIndicator()
//                    self.performSegueWithIdentifier("toSalesPerfExecSumm", sender: self)
//                    
//                    
//                    
//                }
//            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    func getCorporateOutSales()
        
    {
        if customerLabel.text == "" {
            manCode = "0"
        }
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + outstandingCust + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
         request.timeoutInterval = 250
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.corporateArray)
            if self.corporateArray.count == 0 {
                dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
                Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    
    
    func getSalesManfact()
        
    {
        if customerLabel.text == "" {
            manCode = "0"
        }
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_MANUFACTURE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL", myUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.branchSumArray)
            if self.branchSumArray.count == 0 {
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    HUD.hideUIBlockingIndicator()
                    if self.manCode == "0" {
                         self.performSegueWithIdentifier("toSalesManufacture", sender: self)
                    }
                    else {
                         self.performSegueWithIdentifier("toSalesPerfCustSum", sender: self)
                    }
                   
                    
                    
                })
                
            }
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
   
    
    
    

    
  
        


    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
            if elementName == "Column1" {
                noData = false
                elementValue = String()
                element = elementName
                noDataDic[elementName] = ""
            }
            }
            if (elementName == "CODE" || elementName == "CUSTOMER"){
                noData = false
                elementValue = String()
                element = elementName
                customer[elementName] = ""
            }
            if (elementName == "CUST_NAME" || elementName == "MFG_NAME" || elementName == "SO_QTY" || elementName == "VALUE"){
                noData = false
                elementValue = String()
                element = elementName
                custSumm[elementName] = ""
            }
            if (elementName == "CUST_CODE" || elementName == "PR_MFG_ID" || elementName == "CUST_NAME" || elementName  == "MFG_NAME" || elementName == "PR_NAME" || elementName == "SO_QTY" || elementName == "VALUE"){
                
                noData = false
                elementValue = String()
                element = elementName
                custDet[elementName] = ""
                
            }
           
            if (elementName == "brn_id" || elementName == "brn_name" || elementName == "MANUFACTURE_ID" || elementName == "MANUFATURE" || elementName == "Qty" || elementName == "AMOUNT"){
                noData = false
                elementValue = String()
                element = elementName
                branchSum[elementName] = ""
            }
            if (elementName == "AMOUNT" || elementName == "MANUFATURE") {
                noData = false
                elementValue = String()
                element = elementName
                salesManfact[elementName] = ""
            }
            if (elementName == "PR_MFG_ID" || elementName == "Branch" || elementName == "branch_id" ||  elementName == "MANUFACTURER" || elementName == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "QTY" || elementName == "AMOUNT"){
                noData = false
                elementValue = String()
                element = elementName
                branchDet[elementName] = ""
                
            }
            if (elementName == "cust_code" || elementName == "customer" || elementName == "amount") {
                noData = false
                elementValue = String()
                element = elementName
                outstandingSum[elementName] = ""
            }
            if (elementName == "CUSTOMER" || elementName == "BillDate" || elementName == "BillNo" || elementName == "Outstanding" || elementName == "Days") {
                noData = false
                elementValue = String()
                element = elementName
                outstandingDet[elementName] = ""

            }
            if (elementName == "BRANCH" || elementName == "QTY" || elementName == "VALUE"){
                noData = false
                elementValue = String()
                element = elementName
                salesItemAll[elementName] = ""
            }
            if (elementName == "SLNO" || elementName == "Branch_id" || elementName == "Branch" || elementName == "SO_CUST_CODE" || elementName == "PR_CAT_ID" || elementName == "CUST_NAME" || elementName == "QTY" || elementName == "VALUE") {

                noData = false
                elementValue = String()
                element = elementName
                salesItemBranch[elementName] = ""
            }
            if elementName == "SO_INQUIRY_BY" || elementName == "NAME" {
                noData = false
                elementValue = String()
                element = elementName
                executiveList[elementName] = ""
            }
            if elementName == "Branch" || elementName == "SalesPerson" || elementName == "SaleQty" || elementName == "Amount" {
                noData = false
                elementValue = String()
                element = elementName
                executiveSumm[elementName] = ""
            }
            if elementName == "SO_CUST_CODE" || elementName == "brn_id" || elementName == "brn_name" || elementName == "PR_MFG_ID" || elementName == "MANUFATURE" || elementName == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "Qty" || elementName == "AMOUNT" {
                noData = false
                elementValue = String()
                element = elementName
                corporate[elementName] = ""
            }
            if elementName == "CUST_COMPANY_GROUP" || elementName == "SO_CUST_CODE" || elementName == "brn_id" || elementName == "brn_name" || elementName == "MANUFACTURE_ID" || elementName == "PR_CAT_ID" || elementName == "Qty" || elementName == "amount" {
                noData = false
                elementValue = String()
                element = elementName
                corporate[elementName] = ""
            }
            if(elementName == "Column1"){
                noData = false
                elementValue = String()
                element = elementName
                branchtgt[elementName] = ""
            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "CODE" || element == "CUSTOMER"){
                if (string.rangeOfString("\n") == nil){
                  
                    if element == "CODE" {
                        c = 0
                        customer[element!] = string

                    }
                    else if element == "CUSTOMER" {
                        if c > 0 {
                            customer[element!] = customer[element!]! + string
                        }
                        else {
                            customer[element!] = string
                        }
                        c += 1
                    }
                     
                    
                }
                
            }
            if (element == "CUST_CODE" || element == "PR_MFG_ID" || element == "CUST_NAME" || element == "MFG_NAME" || element == "SO_QTY" || element == "VALUE"){
                if (string.rangeOfString("\n") == nil){
                    custSumm[element!] = string
                }
            }
            if (element == "AMOUNT" || element == "MANUFATURE") {
                if (string.rangeOfString("\n") == nil){
                    salesManfact[element!] = string
                }
            }
            if (element == "SO_CUST_CODE" || element == "CUST_NAME" || element == "MFG_NAME" || element == "PR_CAT_ID" || element == "PR_NAME" || element == "SO_QTY" || element == "VALUE"){
                if string.rangeOfString("\n") == nil{
                    if element == "MFG_NAME"{
                        if(count == 0){
                            custDet[element!] = string
                            count += 1
                        }
                    }
                    else{
                            custDet[element!] = string
                    }
                    
                    
                }
            }
                       if (element == "brn_id" || element == "brn_name" || element == "MANUFACTURE_ID" || element == "MANUFATURE" || element ==  "Qty" || element == "AMOUNT"){
                if string.rangeOfString("\n") == nil {
                    /*if element == "brn_name"{
                        if countBranchSum == 0{
                    branchSum[element!] = string
                            countBranchSum += 1
                        }
                    }
                    else {
                     
                    }*/
                     branchSum[element!] = string
                }
                
            }
            
            if (element == "PR_MFG_ID" || element == "Branch" || element == "branch_id" || element == "MANUFACTURER" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "QTY" || element == "AMOUNT") {
                if (string.rangeOfString("\n") == nil){
//                    if element  == "MANUFACTURER"{
//                        if countBranchDet == 0{
//                            branchDet[element!] = string
//                            countBranchDet += 1
//                        }
//                    }
//                    else {
                        branchDet[element!] = string
                    //}
                }
            }
            
            if(element == "cust_code" || element == "customer" || element == "amount"){
                if string.rangeOfString("\n") == nil{
                  outstandingSum[element!] = string
                }
            }
            
            if (element == "CUSTOMER" || element == "BillDate" || element == "BillNo" || element == "Outstanding" || element == "Days") {
                if string.rangeOfString("\n") == nil {
                    outstandingDet[element!] = string
                }
            }
            if (element == "BRANCH" || element == "QTY" || element == "VALUE"){
                if (string.rangeOfString("\n") == nil){
                    salesItemAll[element!] = string
                }
                
            }
            if element == "SO_CUST_CODE" || element == "brn_id" || element == "brn_name" || element == "PR_MFG_ID" || element == "MANUFATURE" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "Qty" || element == "AMOUNT" {
                if string.rangeOfString("\n") == nil{
                    corporate[element!] = string
                }
            }
            if element == "CUST_COMPANY_GROUP" || element == "SO_CUST_CODE" || element == "brn_id" || element == "brn_name" || element == "MANUFACTURE_ID" || element == "PR_CAT_ID" || element == "Qty" || element == "amount" {
                if string.rangeOfString("\n") == nil{
                    corporate[element!] = string
                }

            }
            if (element == "SLNO" || element == "Branch_id" || element == "Branch" || element == "SO_CUST_CODE" || element == "PR_CAT_ID" || element == "CUST_NAME" || element == "QTY" || element == "VALUE"){
                if string.rangeOfString("\n") == nil{
                    salesItemBranch[element!] = string
                }
            }
            
            if element == "SO_INQUIRY_BY" || element == "NAME" {
                if string.rangeOfString("\n") == nil {
                    executiveList[element!] = string
                }
            }
            if element == "Branch" || element == "SalesPerson" || element == "SaleQty" || element == "Amount" {
                if string.rangeOfString("\n") == nil {
                    executiveSumm[element!] = string
                }
            }
            
            if element == "Column1" {
                if string.rangeOfString("\n") == nil {
                    branchtgt[element!] = string
                }
            }
            if (element == "Table1"){
                if element == "Column1" {
                    noDataDic[element!] = string
                }
            }

            
            }
            
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "CUSTOMER" {
            customerArray.addObject(customer)
            
        }
        if (elementName == "VALUE"){
            custSummArray.addObject(custSumm)
            custDetArray.addObject(custDet)
            count = 0
        }
        
       
        
        if elementName == "AMOUNT" {
            branchSumArray.addObject(branchSum)
            branchDetArray.addObject(branchDet)
            countBranchSum = 0
            countBranchDet = 0
        }
        if elementName == "amount" {
            outstandingSumArray.addObject(outstandingSum)
        }
        if elementName == "Days" {
                outstandingDetArray.addObject(outstandingDet)
        }
        if elementName == "VALUE" {
            salesItemAllArray.addObject(salesItemAll)
            
        }
        if elementName == "VALUE" {
            salesItemBranchArray.addObject(salesItemBranch)
        }
        
        if elementName == "NAME" {
            executiveListArray.addObject(executiveList)
        }
        
        if elementName == "Amount" {
            executiveSummArray.addObject(executiveSumm)
        }
        if elementName == "Column1" {
            branchtgtArray.addObject(branchtgt)
        }
        if elementName == "Table1" {
            if elementName == "Column1" {
                noDataArray.addObject(noDataDic)
            }
        }
        if elementName == "MANUFATURE" {
             salesManfactArray.addObject(salesManfact)
        }
        if elementName == "amount" {
             corporateArray.addObject(corporate)
        }
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    

}
