//
//  OutstandingValueDetCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 30/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class OutstandingValueDetCell: UITableViewCell {

    @IBOutlet var category: UILabel!
    @IBOutlet var no: UILabel!
    @IBOutlet var billNo: UILabel!
    
    @IBOutlet var billDate: UILabel!
    
    @IBOutlet var outstanding: UILabel!
    @IBOutlet var days: UILabel!
}
