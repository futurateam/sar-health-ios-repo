//
//  SalesPerfBranchIndCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 12/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchIndCell: UITableViewCell {
    @IBOutlet weak var invNo: UILabel!
    @IBOutlet weak var invDate: UILabel!
    @IBOutlet weak var customerName: UILabel!
    //@IBOutlet weak var manfact: UILabel!
    //@IBOutlet weak var productName: UILabel!
    @IBOutlet weak var batch: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet var prodName: UILabel!
    @IBOutlet var manfactName: UILabel!
    
}
