//
//  SalesPerfBranchSummAll.swift
//  SAR Health
//
//  Created by Anargha K on 21/06/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchSummAll: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {

    @IBOutlet weak var salesText: UILabel!
    @IBOutlet weak var amountText: UILabel!
    
    @IBOutlet weak var qtyText: UILabel!
    @IBOutlet weak var salesTable: UITableView!
    @IBOutlet var totText: UILabel!
  
    
    var noDataArray : NSMutableArray = [], noDataDic = [String :String]()
    var elementValue: String?
    var element : String?
    var noData : Bool = false
    var branchDetArray : NSMutableArray = []
    var branchDet = [String : String](), branchCode = "", manCode = "", branchArray : NSMutableArray = [], mfgArray : NSMutableArray = []

    var salesBranch : NSMutableArray = [], manfactName = "", quantity = [Double](), qtyTot = 0, brn = [String]()
    var no = ["No"]
    var branch = ["Branch"]
    var manfact = ["Manfacturer"]
    var qty = ["Qty"]
    var amount = ["Amount(₹)"]
    var fromDate = ""
    var toDate = "", branchName = "", count = 0, limit = 0, sum : Double = 0, i = 0, j = 0
    var total : Double = 0
    var manfactArray = [String]()
    var num = NSNumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        
        for (var i = 0; i < salesBranch.count; i += 1){
            no.append(String(i+1))
            branch.append((salesBranch[i].valueForKey("brn_name") as? String)!)
            //manfact.append((salesBranch[i].valueForKey("MANUFATURE") as? String)!)
            qty.append((salesBranch[i].valueForKey("Qty") as? String)!)
            amount.append((salesBranch[i].valueForKey("AMOUNT") as? String)!)
        }
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for(var  i = 0; i < salesBranch.count; i += 1){
            total = total + Double((salesBranch[i].valueForKey("AMOUNT") as? String)!)!
            qtyTot = qtyTot + Int((salesBranch[i].valueForKey("Qty") as? String)!)!
        }
        
        
       /* for ( i = 0; i < salesBranch.count ; i += 1){
            total = 0
            qtyTot = 0
            print("i: ",i)
            print("Count", count)
            count = i
            limit = 0
            //  print("mfg: ", branchArray[i].valueForKey("MANUFACTURER") as? String)
            
            
            let custName = salesBranch[i].valueForKey("brn_name") as? String
            //            bill.append((branchArray[i].valueForKey("MANUFACTURER") as? String)!)
            //            prName.append("Product")
            if (i == 0 ){
                brn.append(custName!)
                for ( j = i ; j < salesBranch.count ; j += 1){
                    
                    if salesBranch[j].valueForKey("brn_name") as? String == custName{
                        brn.append("")
                        
                        branch.append((salesBranch[j].valueForKey("brn_name") as? String)!)
                        manfact.append((salesBranch[j].valueForKey("MANUFATURE") as? String)!)
                        qty.append((salesBranch[j].valueForKey("Qty") as? String)!)
                        amount.append(num.stringFromNumber(Double((salesBranch[j].valueForKey("AMOUNT") as? String)!)!)!)
                        total = total + Double((salesBranch[j].valueForKey("AMOUNT") as? String)!)!
                        qtyTot = qtyTot + Int((salesBranch[j].valueForKey("Qty") as? String)!)!
                        limit++
                        no.append(String(limit))
                        //count += 1
                        
                    }
                    
                }
                let tot =  num.stringFromNumber(total)!
                 let qtyText = "Total Qty = " + String(qtyTot)
                amount.append(tot)
                sum =  total
                //brn.append("")
                brn.append("")
                //no.append("")
                no.append("")
                no.append("No")
                //customer.append("")
                manfact.append(qtyText)
                manfact.append("Manfacturer")
                // billDate.append("")
                
                //billNo.append("")
                qty.append("Total =")
                // days.append("")
                qty.append("Qty")
                //value.append("")
                amount.append("Amount (₹)")
                // i = count
                
            }else {
                
                if brn.contains(custName!){
                    
                }
                else {
                    brn.append(custName!)
                    for ( j = 0 ; j < salesBranch.count ; j += 1){
                        
                        if salesBranch[j].valueForKey("brn_name") as? String == custName{
                            brn.append("")
                            manfact.append((salesBranch[j].valueForKey("MANUFATURE") as? String)!)
                            
                             branch.append((salesBranch[j].valueForKey("brn_name") as? String)!)
                            qty.append((salesBranch[j].valueForKey("Qty") as? String)!)
                            amount.append(num.stringFromNumber(Double((salesBranch[j].valueForKey("AMOUNT") as? String)!)!)!)
                            total = total + Double((salesBranch[j].valueForKey("AMOUNT") as? String)!)!
                            qtyTot = qtyTot + Int((salesBranch[j].valueForKey("Qty") as? String)!)!
                            limit++
                            no.append(String(limit))
                            // count += 1
                            
                        }
                        
                    }
                    let tot =  num.stringFromNumber(total)!
                     let qtyText = "Total Qty = " + String(qtyTot)
                    amount.append(tot)
                    sum = sum + total
                    
                    //brn.append("")
                    brn.append("")
                    //no.append("")
                    no.append("")
                    no.append("No")
                    //customer.append("")
                    manfact.append(qtyText)
                    manfact.append("Manfacturer")
                    // billDate.append("")
                    
                    //billNo.append("")
                    qty.append("Total =")
                    // days.append("")
                    qty.append("Qty")
                    //value.append("")
                    amount.append("Amount (₹)")
                    //i = count
                    
                    
                }
            }
            
                   }
        // no.removeAtIndex(no.count - 1)
        no.removeAtIndex(no.count - 1)
        //  brn.removeAtIndex(brn.count - 1)
        // customer.removeAtIndex(customer.count - 1)
        manfact.removeAtIndex(manfact.count - 1)
        //billDate.removeAtIndex(billDate.count - 1)
        // billNo.removeAtIndex(billNo.count - 1)
        
        // days.removeAtIndex(days.count - 1)
        qty.removeAtIndex(qty.count - 1)
        amount.removeAtIndex(amount.count - 1)
        // value.removeAtIndex(value.count - 1)
        
        print("Branch", branch)
        print("No",no)
        print("Customer", manfact)
        
        print("Quantity", quantity)
        print("Amount", amount)
        print("Branch", branch.count)
        print("No",no.count)
        print("Customer", manfact.count)
        print("Quantity",qty.count)
        print("Amount", amount.count)*/

        
        
        totText.text = num.stringFromNumber(total)
        salesText.text = "Total sales in " + branchName + " from " + fromDate + " to " + toDate + " is "
        amountText.text = "₹ " + num.stringFromNumber(total)!
        
        qtyText.text = String(qtyTot)
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toGraphSelfPerfBranchSum" {
            manfactArray.removeAll()
            quantity .removeAll()
            let dest = segue.destinationViewController as! GraphViewController
            dest.xaxisName = "Manfacturer"
            dest.yaxisName = "Amount"
            dest.cusprodText = "Manufacturer"
            if manfactName == "" {
                dest.cusProdValue = "All"
            }
            else {
                dest.cusProdValue = manfactName
            }
            dest.from = fromDate
            dest.to = toDate
            dest.titleTab = "Sales Performance (Branch)"
            for(var  i = 0; i < salesBranch.count; i += 1){
                
                quantity.append(Double((salesBranch[i].valueForKey("AMOUNT") as? String)!)!)
                manfactArray.append((salesBranch[i].valueForKey("brn_name") as? String)!)
            }
            dest.chartData = quantity
            dest.chartLegend = manfactArray
            
        }
        if segue.identifier == "toSalesPerfBranchDet" {
            let dest = segue.destinationViewController as? SalesPerfBranchDet
            dest?.branchArray = branchDetArray
            dest?.branchName = branchName
            dest?.fromDate = fromDate
            dest?.toDate = toDate
            dest?.manfactName = manfactName
            dest?.branchCode = branchCode
            
            
        }
        if segue.identifier == "toSalesPerfBranchDetMan" {
            let dest = segue.destinationViewController as? SalesPerfBranchSummDet
            dest?.branchArray = branchDetArray
            dest?.branchName = branchName
            dest?.fromDate = fromDate
            dest?.toDate = toDate
            dest?.manfactName = manfactName
            dest?.branchCode = branchCode
            
            
        }

    }
    
    
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return no.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalePerfBranchCell", forIndexPath: indexPath) as! SalesPerfBranchSumAllCell
        //cell.manfact.textAlignment = NSTextAlignment.Left
        // cell.branch.text = brn[indexPath.row]
        if no[indexPath.row] == "No" {
           
            var longestWordRange = (no[indexPath.row] as NSString).rangeOfString(no[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: no[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.no.attributedText = attributedString
//            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
//            
//            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
//            
//            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
//            cell.branch.attributedText = attributedString

            longestWordRange = (branch[indexPath.row] as NSString).rangeOfString(branch[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: branch[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.branch.attributedText = attributedString
            
            longestWordRange = (qty[indexPath.row] as NSString).rangeOfString(qty[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: qty[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.quantity.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(13), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
            cell.amount.attributedText = attributedString
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            
            
        }
        else {
            cell.quantity.text = qty[indexPath.row]
           
            cell.branch.text = branch[indexPath.row]
            cell.no.text = no[indexPath.row]
            cell.amount.text = amount[indexPath.row]
            cell.backgroundColor = UIColor.whiteColor()
        }
      /*  if qty[indexPath.row].rangeOfString("Total =") != nil {
            var longestWordRange = (qty[indexPath.row] as NSString).rangeOfString(qty[indexPath.row])
            
            var attributedString = NSMutableAttributedString(string: qty[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.quantity.attributedText = attributedString
            longestWordRange = (amount[indexPath.row] as NSString).rangeOfString(amount[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: amount[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.amount.attributedText = attributedString
            longestWordRange = (manfact[indexPath.row] as NSString).rangeOfString(manfact[indexPath.row])
            
            attributedString = NSMutableAttributedString(string: manfact[indexPath.row] as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(12), NSForegroundColorAttributeName : UIColor.redColor()], range: longestWordRange)
            cell.manfact.attributedText = attributedString
            cell.manfact.textAlignment = NSTextAlignment.Right
            
        }*/

        
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (no[indexPath.row] == "No.") || (no[indexPath.row] == "" && qty[indexPath.row] == "" && amount[indexPath.row] == "" && brn[indexPath.row] != "") || (qty[indexPath.row] == "Total ="){
        }
        else {
           /* branchName = branch[indexPath.row]
            
            for(var i = 0; i < branchArray.count; i++){
                
                if  (branch[indexPath.row] == branchArray[i].valueForKey("BRN_NAME") as? String){
                    branchCode = (branchArray[i].valueForKey("BRN_ID") as? String)!
                }
            }
            for (var i = 0; i < mfgArray.count; i += 1){
                if mfgArray[i].valueForKey("MFG_NAME") as? String == manfact[indexPath.row]{
                    manCode = (mfgArray[i].valueForKey("PR_MFG_ID") as? String)!
                }
            }*/
            branchCode = salesBranch[indexPath.row - 1].valueForKey("brn_id") as! String
           // manCode = salesBranch[indexPath.row - 1].valueForKey("MANUFACTURE_ID") as! String
            branchName = branch[indexPath.row]
            branchDetArray.removeAllObjects()
            getSalesBranchDet()
            

        }
       
    }
    
    func getSalesBranchDet()
    {
        
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        //
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
         let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            print("Branch Array",self.branchDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                
                if self.manCode != "0" {
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfBranchDetMan", sender: self)
                }
                else {
                    HUD.hideUIBlockingIndicator()
                    self.performSegueWithIdentifier("toSalesPerfBranchDet", sender: self)
                }
                
              
                
                
                
                
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
    func getSalesBranchDetMan()
    {
        
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        
        
        //
        let scriptUrl1 = Constants().API + "MOBAPP_BRANCH_SALE_DETAIL?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&FRMDATE=" + fromDate + "&TODATE=" + toDate
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        print("URL : ",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        request.timeoutInterval = 250
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            //            for(var i = 0; i < self.customerArray.count; i++){
            //                self.itemArray.append((self.customerArray[i].valueForKey("CUSTOMER") as? String)!)
            //            }
            
            print("Branch Array",self.branchDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.branchDetArray.count == 0){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    
                    
                }
                
                HUD.hideUIBlockingIndicator()
                self.performSegueWithIdentifier("toSalesPerfBranchDetMan", sender: self)
                
                
                
                
            })
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    
   
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                if elementName == "Column1" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    noDataDic[elementName] = ""
                }
            }
            if (elementName == "PR_MFG_ID" || elementName == "Branch" || elementName == "branch_id" ||  elementName == "MANUFACTURER" || elementName == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "QTY" || elementName == "AMOUNT"){
                noData = false
                elementValue = String()
                element = elementName
                branchDet[elementName] = ""
                
            }
   
        }
        
    }
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if elementValue != nil{
            if (element == "PR_MFG_ID" || element == "Branch" || element == "branch_id" || element == "MANUFACTURER" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "QTY" || element == "AMOUNT") {
                if (string.rangeOfString("\n") == nil){
                    //                    if element  == "MANUFACTURER"{
                    //                        if countBranchDet == 0{
                    //                            branchDet[element!] = string
                    //                            countBranchDet += 1
                    //                        }
                    //                    }
                    //                    else {
                    branchDet[element!] = string
                    //}
                }
            }
            if (element == "Table1"){
                if element == "Column1" {
                    noDataDic[element!] = string
                }
            }
        }
    }
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "AMOUNT" {
            
            branchDetArray.addObject(branchDet)
            
            
        }
        if elementName == "Table1" {
            if elementName == "Column1" {
                noDataArray.addObject(noDataDic)
            }
        }
    }
    

    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func viewGraphPressed(sender: AnyObject) {
        performSegueWithIdentifier("toGraphSelfPerfBranchSum", sender: self)
    }
    
    
    
}

