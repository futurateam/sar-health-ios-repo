//
//  SalesPerfBranchInd.swift
//  SAR Health
//
//  Created by Prajeesh KK on 12/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class SalesPerfBranchInd: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var salesTable: UITableView!
    @IBOutlet weak var totalText: UILabel!
    @IBOutlet var qtyTot: UILabel!
    
    
    var invNo = ["Inv No"], invDate = ["Inv Date"], customer = ["Customer"], mfgName = ["Manufact"], prodName = ["Product Name"],  batch = ["Batch"], quantity = ["Qty"], value = ["Amt (₹)"], manfactName = "", productName = "", qty = 0
    var num = NSNumberFormatter(), tot : Double = 0
    var salesDet : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        num.numberStyle = NSNumberFormatterStyle.DecimalStyle
        let indiaLocale = NSLocale(localeIdentifier: "en_IN")
        num.locale = indiaLocale
        print("sales detail",salesDet)
        //self.salesTable.tableFooterView = UIView.init(frame: CGRectZero)
        for (var i = 0 ; i < salesDet.count ; i += 1){
            invNo.append(salesDet[i].valueForKey("INV_NO") as! String)
            invDate.append(salesDet[i].valueForKey("INV_DATE") as! String)
            customer.append(salesDet[i].valueForKey("CUST_NAME") as! String)
            //mfgName.append(salesDet[i].valueForKey("MANUFACTURER") as! String)
            prodName.append(salesDet[i].valueForKey("PRODUCT") as! String)
            batch.append(salesDet[i].valueForKey("BATCH_NO") as! String)
            quantity.append(salesDet[i].valueForKey("QTY") as! String)
            value.append(num.stringFromNumber(Double((salesDet[i].valueForKey("AMOUNT") as? String)!)!)!)
            tot = tot + Double((salesDet[i].valueForKey("AMOUNT") as? String)!)!
            qty = qty + Int((salesDet[i].valueForKey("QTY") as! String))!
            
        }
        totalText.text = num.stringFromNumber(tot)
        qtyTot.text = String(qty)
        //  self.purchaseTable.tableFooterView = UIView.init(frame: CGRectZero)
        
        
        //        no = ["1", "2", "3", "4", "5", "6", "7"]
        //        branch = ["Branch 1", "Branch 2", "Branch 3", "Branch 4", "Branch 5", "Branch 6", "Branch 7"]
        //        quantity = ["50", "100", "75", "100", "100", "100", "100"]
        //        amount = ["20,000", "20,000", "20,000", "20,000", "20,000", "20,000", "20,000"]
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invNo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = salesTable.dequeueReusableCellWithIdentifier("SalesPerfBranchIndCell", forIndexPath: indexPath) as! SalesPerfBranchIndCell
        
        if(invNo[indexPath.row] == "Inv No"){
            cell.manfactName.text = manfactName
            cell.prodName.text = productName
            cell.manfactName.backgroundColor = UIColor.whiteColor()
            cell.prodName.backgroundColor = UIColor.whiteColor()
            cell.invNo.attributedText = headFont(invNo[indexPath.row])
            cell.invDate.attributedText = headFont(invDate[indexPath.row])
            cell.customerName.attributedText = headFont(customer[indexPath.row])
            //cell.manfact.attributedText = headFont(mfgName[indexPath.row])
           // cell.productName.attributedText = headFont(prodName[indexPath.row])
            cell.batch.attributedText = headFont(batch[indexPath.row])
            cell.quantity.attributedText = headFont(quantity[indexPath.row])
            cell.amount.attributedText = headFont(value[indexPath.row])
            
            cell.backgroundColor = UIColor(red: 210.0/255.0 , green: 208.0/255.0, blue: 208.0/255.0, alpha: 1.0)
        }
        else {
            cell.manfactName.text = ""
            cell.prodName.text = ""
            cell.invNo.text = invNo[indexPath.row]
            cell.invDate.text = invDate[indexPath.row]
            cell.customerName.text = customer[indexPath.row]
            //cell.manfact.text = mfgName[indexPath.row]
            //cell.productName.text = prodName[indexPath.row]
            cell.batch.text = batch[indexPath.row]
            cell.quantity.text = quantity[indexPath.row]
            cell.amount.text = value[indexPath.row]
            
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    
    
    func headFont(headString : String) -> NSMutableAttributedString {
        let longestWordRange = (headString as NSString).rangeOfString(headString)
        
        let attributedString = NSMutableAttributedString(string:headString as String, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(11)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(11), NSForegroundColorAttributeName : UIColor(red: 0.0/255.0 , green: 26.0/255.0, blue: 69.0/255.0, alpha: 1.0)], range: longestWordRange)
        return attributedString
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

}
