//
//  ChartView.h
//  ChartView
//
//  Created by Prajeesh KK on 03/05/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ChartView.
FOUNDATION_EXPORT double ChartViewVersionNumber;

//! Project version string for ChartView.
FOUNDATION_EXPORT const unsigned char ChartViewVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ChartView/PublicHeader.h>

#import <ChartView/JBChartView.h>
#import <ChartView/JBBarChartView.h>