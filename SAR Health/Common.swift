//
//  Common.swift
//  SAR Health
//
//  Created by Prajeesh KK on 16/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class Common: NSObject  {

    func showAlert(title:NSString , message:NSString, viewController: UIViewController){
    let alert = UIAlertController(title: title as String , message: message as String, preferredStyle: UIAlertControllerStyle.Alert)
    
    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
    
    }))
        viewController.presentViewController(alert, animated: true, completion: nil)
    
    }
    
    
    
  
}

