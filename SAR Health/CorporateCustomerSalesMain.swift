//
//  CorporateCustomerSalesMain.swift
//  SAR Health
//
//  Created by Anargha K on 25/08/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class CorporateCustomerSalesMain: UIViewController, UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate {
        @IBOutlet var branchLabel: UILabel!
        @IBOutlet var customerLabel: UITextField!
    
    @IBOutlet var productLabel: UITextField!
    @IBOutlet var productTable: UITableView!
    
        @IBOutlet var branchTable: UITableView!
        @IBOutlet var customerTable: UITableView!
        @IBOutlet var fromDate: UITextField!
        @IBOutlet var toDate: UITextField!
        @IBOutlet var datePickerView: UIView!
        @IBOutlet var fromDateButton: UIButton!
        @IBOutlet var toDateButton: UIButton!
        @IBOutlet var datePicker: UIDatePicker!
        @IBOutlet var summerzdImage: UIImageView!
        @IBOutlet var detaildImage: UIImageView!
        @IBOutlet var summrzdButton: UIButton!
        @IBOutlet var detaildButton: UIButton!
        @IBOutlet var tabTitle: UILabel!
        @IBOutlet var summLabel: UILabel!
        @IBOutlet var detailLabel: UILabel!
        @IBOutlet var branchButton: UIButton!
        
        @IBOutlet var summImgtoCust: NSLayoutConstraint!
        @IBOutlet var summImgHeight: NSLayoutConstraint!
        
        @IBOutlet var summbtnHeight: NSLayoutConstraint!
        
        @IBOutlet var fromLabeltoCust: NSLayoutConstraint!
        @IBOutlet var toLabeltoCust: NSLayoutConstraint!
        
        @IBOutlet var fromLabel: UILabel!
        @IBOutlet var fromText: UITextField!
        @IBOutlet var toLabel: UILabel!
        @IBOutlet var toText: UITextField!
        @IBOutlet var toCal: UIImageView!
        @IBOutlet var fromCal: UIImageView!

        var count = 0, countBranchSum = 0, countBranchDet = 0
        var salesItemAllArray : NSMutableArray = [], branchtgt = [String : String](), branchtgtArray : NSMutableArray = []
        var salesItemAll = [String:String]()
        var customer = [String : String]()
        var salesItemBranchArray : NSMutableArray = [], noDataArray : NSMutableArray = [], noDataDic = [String :String]()
        var customerArray : NSMutableArray = []
        var salesItemBranch = [String : String](), outstandingCust = "", corporate = [String : String](), corporateArray : NSMutableArray = []
        //var elementProduct : NSMutableArray = []
        var elementValue: String?
        var element : String?
        var custCode = ""
        var noData : Bool = false, c : Int = 0
        var in_from_date_day = 0
        var in_from_date_month = 0
        var in_from_date_year = 0
        var manCode = ""
        var radiotag1 = 0
        var radiotag2 = 1
        var radioText = "summerized"
        var branchCode = "", execCode = ""
        var prodCode = ""
        var i = 0
        var formatDate = ""
        var format = ""
        var branch : NSMutableArray = []
        var branchName = [String](), branchCount : Int = 0
        var custSummArray : NSMutableArray = [], outstandingSumArray : NSMutableArray = [], outstandingDetArray : NSMutableArray = []
        var custSumm = [String:String](), outstandingSum = [String : String](), outstandingDet = [String : String]()
        var custDetArray : NSMutableArray = []
        var custDet = [String:String]()
        var branchSumArray : NSMutableArray = [], executiveList = [String : String](), executiveListArray : NSMutableArray = [], finyearArray : NSMutableArray = []
        var branchSum = [String : String]()
        var branchDetArray : NSMutableArray = []
        var branchDet = [String : String]()
        var manfactArray : NSMutableArray = []
        var manfact = [String : String](), executiveSumm = [String : String](), executiveSummArray : NSMutableArray = [], productList = [String : String](), productListArray : NSMutableArray = []
        var dateFormatter = NSDateFormatter()
        var flag : Bool = false
        var btag : NSInteger = 0
        var searchActive : Bool = false
        var filter = [String](), execFilter = [String](), productFilterArray = [String]()
        var itemArray = [String](), productFilter = [String]()
        var manfactfilter = [String](), prodFilter = [String]()
        var cellNo = 0
        var largeNumber = 36000
        //var branchList = [String]()
        //var itemlist = [String]()
        var itemList : NSMutableArray = []
        
        var num = NSNumberFormatter()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            num.numberStyle = NSNumberFormatterStyle.DecimalStyle
            let indiaLocale = NSLocale(localeIdentifier: "en_IN")
            num.locale = indiaLocale
            btag = branchButton.tag
            self.customerTable.tableFooterView = UIView.init(frame: CGRectZero)
            branchCount = NSUserDefaults.standardUserDefaults().valueForKey("Branch_Count") as! Int
            print("Number",num.stringFromNumber(largeNumber)!)
            
            if branch.count >= branchCount {
                branchName = ["All"]
            }
            else {
                branchName = []
            }
                       for(i = 0; i < itemList.count; i += 1){
                productFilter.append((itemList[i].valueForKey("PRODUCT") as? String)!)
            }
            datePickerView.hidden = true
            //branchList = ["All", "Branch 1", "Branch 2", "Branch 3"]
            // itemlist = ["Item 1", "Item 2", "Item 3"]
            customerTable.hidden = true
            branchTable.hidden = true
            productTable.hidden = true
            // Do any additional setup after loading the view.
            dateFormatter.dateFormat = "yyyy-MM-dd"
            datePicker.maximumDate = NSDate()
            for(i = 0; i < branch.count; i += 1){
                branchName.append((branch[i].valueForKey("BRN_NAME") as? String)!)
            }
            //        for(i = 0; i < itemList.count; i++){
            //            itemArray.append((itemList[i].valueForKey("PRODUCT") as? String)!)
            //        }
            branchLabel.text = "Branch"
            customerLabel.text = ""
            fromDate.text = ""
            toDate.text = ""
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        
        // MARK: - Navigation
        
        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
                       if segue.identifier == "toOutstandingDet" {
                let dest = segue.destinationViewController as? OutstandingValueDet
                dest?.outstandingValue = outstandingDetArray
                dest?.custCode = custCode
                dest?.custmerName = customerLabel.text!
                dest?.branchName = branchLabel.text!
                branchLabel.text = "Branch"
                customerLabel.text = ""
                fromDate.text = ""
                toDate.text = ""
                custCode = ""
                branchCode = ""
                
            }
            if segue.identifier == "toCorporateOutDet" {
                let dest = segue.destinationViewController as! CorporateOutSalesMain
                dest.corpDet = corporateArray
                print(outstandingCust)
                dest.customerName = outstandingCust
                dest.fromDate = fromDate.text!
                dest.toDate = toDate.text!
                dest.prodCode = prodCode
                dest.branchCode = branchCode
                
                for (var i = 0; i < manfactArray.count; i += 1){
                    if manfactArray[i].valueForKey("MFG_NAME") as? String == customerLabel.text{
                        manCode = (manfactArray[i].valueForKey("PR_MFG_ID") as? String)!
                    }
                }
                dest.manCode = manCode
                branchLabel.text = "Branch"
                customerLabel.text = ""
                productLabel.text = ""
                prodCode = ""
                fromDate.text = ""
                toDate.text = ""
                custCode = ""
                branchCode = ""
            }
            
            
        }
        
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1;
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if(tableView == customerTable){
                if(searchActive){
                    return filter.count
                }
               
                    return manfactfilter.count
                
            }
            else if tableView == branchTable{
                
                return branchName.count;
            }
            else {
                if(searchActive){
                    return productFilterArray.count
                }
                
                return productFilter.count
            }
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            if(tableView == customerTable){
                let cell = customerTable.dequeueReusableCellWithIdentifier("CustomerCell", forIndexPath: indexPath) as! SalesPerformanceCustomerCell
        
                    if(searchActive){
                        cell.customerName.text = filter[indexPath.row]
                    }
                    else{
                        cell.customerName.text = manfactfilter[indexPath.row]
                    }
                
                
                return cell;
                
            }
            else if tableView == branchTable{
                let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! SalesPerformanceBranchCell
                cell.branchName.text = branchName[indexPath.row]
                return cell
            }
            else {
                let cell = branchTable.dequeueReusableCellWithIdentifier("BranchCell", forIndexPath: indexPath) as! SalesPerformanceBranchCell
                if(searchActive){
                    cell.branchName.text = productFilterArray[indexPath.row]
                }
                else{
                    cell.branchName.text = prodFilter[indexPath.row]
                }
                
                
                return cell;
            }
            
        }
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
           corporateArray.removeAllObjects()
            if(tableView == customerTable){
        
                    if(searchActive){
                        customerLabel.text = filter[indexPath.row]
                    }
                    else{
                        customerLabel.text = manfactfilter[indexPath.row]
                    }
                    
                    print("Manfact",manfactArray)
                    for (var i = 0; i < manfactArray.count; i += 1){
                        if manfactArray[i].valueForKey("MFG_NAME") as? String == customerLabel.text{
                            manCode = (manfactArray[i].valueForKey("PR_MFG_ID") as? String)!
                        }
                    }
                    print(manCode)
                    
                
                getProductList();
                
                
                
                view.endEditing(true)
                
            }
            else if tableView == branchTable{
                branchLabel.text = branchName[indexPath.row]
               
                    if branchLabel.text == "All"{
                        branchCode = "0"
                    }
                    else {
                        branchCode = (branch[indexPath.row - 1].valueForKey("BRN_ID") as? String)!
                    }
                    
                
                
                branchTable.hidden = true
                                view.endEditing(true)
            }
            else {
                if(searchActive){
                    productLabel.text = productFilterArray[indexPath.row]
                }
                else{
                    productLabel.text = productFilter[indexPath.row]
                }
                
                
                for (var i = 0; i < productListArray.count; i += 1){
                    if productListArray[i].valueForKey("PR_NAME") as? String == productLabel.text{
                        prodCode = (productListArray[i].valueForKey("PR_ID") as? String)!
                    }
                }
                print(prodCode)
                
                
             
                
                
                
                view.endEditing(true)
            }
            
        }
    
    
        //Mark: - Textfields Methods
        func textFieldDidBeginEditing(textField: UITextField) {
            datePickerView.hidden = true
                       for(var i = 0; i < self.manfactArray.count; i+=1){
                self.manfactfilter.append((self.manfactArray[i].valueForKey("MFG_NAME") as? String)!)
            }
            for(var i = 0; i < self.productListArray.count; i+=1){
                self.prodFilter.append((self.productListArray[i].valueForKey("PR_NAME") as? String)!)
            }

            
            
            
            flag = false
            branchTable.hidden = true
            
            searchActive = true
            textField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
            textFieldDidChange(textField)
        }
        
        
        func textFieldDidEndEditing(textField: UITextField) {
            customerTable.hidden = true;
            productTable.hidden = true;
            searchActive = false
        }
        
        
        func textFieldDidChange(textField:UITextField){
            if flag == false{
                flag = true
            }
            
            if textField.text?.characters.count >= 2{
                
                
                if textField == customerLabel {
                    if flag == true{
                        
                        customerTable.hidden = false;
                        filter = manfactfilter.filter({ (text) -> Bool in
                            let tmp: NSString = text
                            let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                            return range.location != NSNotFound
                        })
                        searchActive = true
                        
                        HUD.hideUIBlockingIndicator()
                        for (var i=0; i < self.filter.count; i += 1 ) {
                            for (var j=(i+1); j < self.filter.count; j += 1) {
                                if ((self.filter[i] == self.filter[j])) {
                                    self.filter.removeAtIndex(j)
                                    j -= 1;
                                }
                            }
                        }
                        self.customerTable.reloadData()
                    }

                }
                else {
                    if flag == true{
                        
                        productTable.hidden = false;
                        productFilterArray = prodFilter.filter({ (text) -> Bool in
                            let tmp: NSString = text
                            let range = tmp.rangeOfString(textField.text!, options: NSStringCompareOptions.CaseInsensitiveSearch)
                            return range.location != NSNotFound
                        })
                        searchActive = true
                        
                        HUD.hideUIBlockingIndicator()
                        for (var i=0; i < self.productFilterArray.count; i += 1 ) {
                            for (var j=(i+1); j < self.productFilterArray.count; j += 1) {
                                if ((self.productFilterArray[i] == self.productFilterArray[j])) {
                                    self.productFilterArray.removeAtIndex(j)
                                    j -= 1;
                                }
                            }
                        }
                        self.productTable.reloadData()
                    }

                }
                    
                
                
                
            }
            else {
                customerTable.hidden = true
                productTable.hidden = true
            }
            
        }
        
        
        @IBAction func branchButton(sender: AnyObject) {
            customerTable.hidden = true
            
            view.endEditing(true)
            if(btag == 0){
                self.branchTable.hidden = true
                
                UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:{
                    var frame = self.branchTable.frame
                    frame.size.height = 0;
                    self.branchTable.frame = frame
                    }, completion:{finished in} )
                btag = 1;
            }
            else{
                
                UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    var frame = self.branchTable.frame;
                    frame.size.height = 175;
                    self.branchTable.frame = frame;
                    }, completion: { finished in })
                self.branchTable.hidden = false
                
                btag = 0;
            }
            
            view.endEditing(true)
            
        }
        
        @IBAction func findPressed(sender: AnyObject) {
            customerArray.removeAllObjects()
            corporateArray.removeAllObjects()
            custSummArray.removeAllObjects()
            custDetArray.removeAllObjects()
            branchSumArray.removeAllObjects()
            branchDetArray.removeAllObjects()
            outstandingSumArray.removeAllObjects()
            outstandingDetArray.removeAllObjects()
            salesItemAllArray.removeAllObjects()
            salesItemBranchArray.removeAllObjects()
            executiveListArray.removeAllObjects()
            executiveSummArray.removeAllObjects()
           productListArray.removeAllObjects()
                if (branchLabel.text == "Branch" || fromDate.text == "" || toDate.text == ""){
                    Common().showAlert(Constants().APP_NAME, message: "All fields are mandatory",viewController: self)
                }
                else if Reachability.isConnectedToNetwork(){
                    print(branchCode)
                    print(manCode)
                    print(custCode)
                    getCorporateOutSales()
                }
                else {
                    Common().showAlert(Constants().APP_NAME, message: "Please check your internet connection and try agian", viewController: self)
                }
            
    
            
            

            filter.removeAll()
            
        }
        
        @IBAction func fromDatePressed(sender: AnyObject) {
            datePicker.reloadInputViews()
            btag = fromDateButton.tag
            datePickerView.hidden = false
            view.endEditing(true)
        }
        
        @IBAction func toDatePressed(sender: AnyObject) {
            
            datePicker.setDate(NSDate(), animated: true)
            btag = toDateButton.tag
            datePickerView.hidden = false
            view.endEditing(true)
        }
        
        @IBAction func datePickerAction(sender: AnyObject) {
            formatDate = dateFormatter.stringFromDate(datePicker.date)
            
            if btag == 1 {
                
                fromDate.text = formatDate
            }
            else {
                toDate.text = formatDate
            }
        }
        
        @IBAction func pickerDonePressed(sender: AnyObject) {
            format = dateFormatter.stringFromDate(NSDate())
            print(NSDate())
            if dateFormatter.stringFromDate(datePicker.date) == dateFormatter.stringFromDate(NSDate()){
                
                if btag == 1 {
                    
                    fromDate.text = format
                }
                else {
                    toDate.text = format
                }
                
            }
                
            else {
                // dateValidation(btag)
            }
            
            datePickerView.hidden = true
        }
        /*  func dateValidation (tag: Int){
         let currentDate = NSDate()
         dateFormatter.dateFormat = "yyyy"
         //let mYear = Int(dateFormatter.stringFromDate(currentDate))
         let selectedYear = Int(dateFormatter.stringFromDate(datePicker.date))
         dateFormatter.dateFormat = "MM"
         //let mMonth = Int(dateFormatter.stringFromDate(currentDate))
         let selectedMonth = Int(dateFormatter.stringFromDate(datePicker.date))
         dateFormatter.dateFormat = "dd"
         //let mDay = Int(dateFormatter.stringFromDate(currentDate))
         let selectedDay = Int(dateFormatter.stringFromDate(datePicker.date))
         if (tag == 1) {
         in_from_date_day = selectedDay!
         in_from_date_month = selectedMonth!
         in_from_date_year = selectedYear!
         dateFormatter.dateFormat = "yyyy-MM-dd"
         fromDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         if(tag == 0){
         if(in_from_date_month > 3){
         if(selectedMonth > in_from_date_month){
         if(selectedYear == in_from_date_year){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else if(selectedYear! + 1 == in_from_date_year){
         if(selectedMonth < 3){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else {
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         else {
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         else if(selectedYear == in_from_date_year + 1){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else {
         if(selectedDay > in_from_date_day){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else {
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         }
         else {
         if(selectedYear == in_from_date_year){
         if(selectedMonth < 3){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else if(selectedMonth == in_from_date_month){
         if(selectedDay > in_from_date_day){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else {
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         else {
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         else if(selectedYear! - 1 == in_from_date_year){
         if(selectedMonth! + 1 < in_from_date_month){
         dateFormatter.dateFormat = "yyyy-MM-dd"
         toDate.text = dateFormatter.stringFromDate(datePicker.date)
         }
         else{
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         else{
         Common().showAlert(Constants().APP_NAME, message: "The financial year starts from APRIL 1 and ends on MARCH 31. Please select accordingly.", viewController: self)
         toDate.text = ""
         }
         }
         }
         
         
         }*/
        @IBAction func backPressed(sender: AnyObject) {
            navigationController?.popViewControllerAnimated(true)
        }
        
        @IBAction func summrzdPressed(sender: AnyObject) {
            // radiotag = summrzdButton.tag
            if radiotag1 == 0 {
                summerzdImage.image = UIImage(named: "rdChecked.png")
                detaildImage.image  = UIImage(named: "rdUnChecked.png")
                radioText = "summerized"
                radiotag2 = 1
            }
            else {
                summerzdImage.image = UIImage(named: "rdUnChecked.png")
                detaildImage.image = UIImage(named: "rdChecked.png")
                radioText = "detailed"
                radiotag2 = 0
            }
            
        }
        
        @IBAction func detaildPressed(sender: AnyObject) {
            //radiotag = detaildButton.tag
            if radiotag2 == 0{
                detaildImage.image = UIImage(named: "rdUnChecked.png")
                summerzdImage.image = UIImage(named: "rdChecked.png")
                radioText = "summerized"
                radiotag1 = 1
            }
            else {
                detaildImage.image = UIImage(named: "rdChecked.png")
                summerzdImage.image = UIImage(named: "rdUnChecked.png")
                radioText = "detailed"
                radiotag1 = 0
            }
        }
        
        func getCustomerList()
        {
            
            HUD.showUIBlockingIndicatorWithText("Loading...")
            //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
            let scriptUrl = Constants().API + "MOBAPP_CUSTOMERS?BRANCH=" + branchCode + "&CUSTOMER="
            print("url",scriptUrl)
            // Add one parameter
            //let urlWithParams = scriptUrl + "?BRANCH=1"
            
            // Create NSURL Ibject
            let myUrl = NSURL(string: scriptUrl);
            
            // Creaste URL Request
            let request = NSMutableURLRequest(URL:myUrl!);
            
            // Set request HTTP method to GET. It could be POST as well
            request.HTTPMethod = "GET"
            
            // If needed you could add Authorization header value
            // Add Basic Authorization
            /*
             let username = "myUserName"
             let password = "myPassword"
             let loginString = NSString(format: "%@:%@", username, password)
             let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
             let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
             request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
             */
            
            // Or it could be a single Authorization Token value
            //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
            
            // Excute HTTP Request
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                // Check for error
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                // Print out response string
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                
                print("data=\(data?.length)")
                let parser = NSXMLParser(data: data!)
                parser.delegate = self
                parser.parse()
                
                print("customer",self.customerArray)
                
                //            for (var i=0; i < self.customerArray.count; i++ ) {
                //                for (var j=(i+1); j < self.customerArray.count; j++) {
                //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
                //                        self.customerArray.removeObjectAtIndex(i);
                //                        j--;
                //                    }
                //                }
                //            }
                
                
                //            if (data!.length == 38){
                //                HUD.hideUIBlockingIndicator()
                //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
                //
                //
                //            }
                //            else{
                //                dispatch_async(dispatch_get_main_queue(), {
                //                    if self.branchLabel.text == "All"{
                //
                //                        HUD.hideUIBlockingIndicator()
                //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
                //                    }
                //                    else {
                //                        HUD.hideUIBlockingIndicator()
                //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
                //
                //                    }
                //
                //                })
                //            }
                
                //Convert server json response to NSDictionary
                //                        do {
                //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                //
                //                                // Print out dictionary
                //                                print(convertedJsonIntoDict)
                //
                //                                // Get value by key
                //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
                //                                print(firstNameValue!)
                //
                //                            }
                //                        }
                //                        catch let error as NSError {
                //                            print(error.localizedDescription)
                //                        }
                //            if self.branchLabel.text == "All"{
                //
                //
                //                self.performSegueWithIdentifier("toProductAll", sender: self)
                //            }
                //            else{
                //                //getDataProductAll()
                //                if self.check == true{
                //
                //                    print(self.productStock)
                //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
                //                }
                //                else {
                //                    HUD.hideUIBlockingIndicator()
                //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
                //
                //                }
                //            }
                //
                
            }
            
            task.resume()
            
            
        }
    
    func getProductList()
    {
        
        HUD.showUIBlockingIndicatorWithText("Loading...")
        //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
        let scriptUrl = Constants().API + "MOBAPP_MANUCATURE_PRODUCTS?MANUFACTURE=" + manCode + "&PRODUCT="
        print("url",scriptUrl)
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            dispatch_async(dispatch_get_main_queue(), {
                HUD.hideUIBlockingIndicator()
            })
            
            print("data=\(data?.length)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
            print("customer",self.productListArray)
            
            //            for (var i=0; i < self.customerArray.count; i++ ) {
            //                for (var j=(i+1); j < self.customerArray.count; j++) {
            //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
            //                        self.customerArray.removeObjectAtIndex(i);
            //                        j--;
            //                    }
            //                }
            //            }
            
            
            //            if (data!.length == 38){
            //                HUD.hideUIBlockingIndicator()
            //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //
            //            }
            //            else{
            //                dispatch_async(dispatch_get_main_queue(), {
            //                    if self.branchLabel.text == "All"{
            //
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfmAllItem", sender: self)
            //                    }
            //                    else {
            //                        HUD.hideUIBlockingIndicator()
            //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
            //
            //                    }
            //
            //                })
            //            }
            
            //Convert server json response to NSDictionary
            //                        do {
            //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
            //
            //                                // Print out dictionary
            //                                print(convertedJsonIntoDict)
            //
            //                                // Get value by key
            //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
            //                                print(firstNameValue!)
            //
            //                            }
            //                        }
            //                        catch let error as NSError {
            //                            print(error.localizedDescription)
            //                        }
            //            if self.branchLabel.text == "All"{
            //
            //
            //                self.performSegueWithIdentifier("toProductAll", sender: self)
            //            }
            //            else{
            //                //getDataProductAll()
            //                if self.check == true{
            //
            //                    print(self.productStock)
            //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
            //                }
            //                else {
            //                    HUD.hideUIBlockingIndicator()
            //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
            //
            //                }
            //            }
            //
            
        }
        
        task.resume()
        
        
    }
    

    
        func getCorporateOutSales()
            
        {
            if customerLabel.text == "" {
                manCode = "0"
            }
            if productLabel.text == "" {
                prodCode = "0"
            }
            HUD.showUIBlockingIndicatorWithText("Loading...")
            //let scriptUrl = Constants().API + "MOBAPP_PRODUCT_SALE?BRANCH="+branchCode+"&PRODUCT="+prodCode
            let scriptUrl1 = Constants().API + "MOBAPP_CORPORATE_SALE?BRANCH=" + branchCode + "&COMPANY=1&DIVISION=1&MANUFACTURE=" + manCode + "&CUSTOMER=" + outstandingCust + "&PRODUCT=" + prodCode + "&FRMDATE=" + fromDate.text! + "&TODATE=" + toDate.text!
            
            let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
            
            // Add one parameter
            //let urlWithParams = scriptUrl + "?BRANCH=1"
            
            // Create NSURL Ibject
            let myUrl = NSURL(string: scriptUrl);
            print("URL", myUrl)
            // Creaste URL Request
            let request = NSMutableURLRequest(URL:myUrl!);
            
            // Set request HTTP method to GET. It could be POST as well
            request.HTTPMethod = "GET"
            request.timeoutInterval = 250
            // If needed you could add Authorization header value
            // Add Basic Authorization
            /*
             let username = "myUserName"
             let password = "myPassword"
             let loginString = NSString(format: "%@:%@", username, password)
             let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
             let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
             request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
             */
            
            // Or it could be a single Authorization Token value
            //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
            
            // Excute HTTP Request
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                // Check for error
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                // Print out response string
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                dispatch_async(dispatch_get_main_queue(), {
                    HUD.hideUIBlockingIndicator()
                })
                
                print("data=\(data?.length)")
                let parser = NSXMLParser(data: data!)
                parser.delegate = self
                parser.parse()
                
                print("customer",self.corporateArray)
                if self.corporateArray.count == 0 {
                    dispatch_async(dispatch_get_main_queue(), {
                        HUD.hideUIBlockingIndicator()
                        Common().showAlert(Constants().APP_NAME, message: "No Data Found", viewController: self)
                    })
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        HUD.hideUIBlockingIndicator()
                        self.performSegueWithIdentifier("toCorporateOutDet", sender: self)
                        
                        
                    })
                    
                }
                
                //            for (var i=0; i < self.customerArray.count; i++ ) {
                //                for (var j=(i+1); j < self.customerArray.count; j++) {
                //                    if ((self.customerArray[i].valueForKey("CODE") as? String == self.customerArray[j].valueForKey("CODE") as? String) && (self.customerArray[i].valueForKey("CUSTOMER") as? String == self.customerArray[j].valueForKey("CUSTOMER") as? String)) {
                //                        self.customerArray.removeObjectAtIndex(i);
                //                        j--;
                //                    }
                //                }
                //            }
                
                
                //            if (data!.length == 38){
                //                HUD.hideUIBlockingIndicator()
                //                Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
                //
                //
                //            }
                //            else{
                //                dispatch_async(dispatch_get_main_queue(), {
                //                    if self.branchLabel.text == "All"{
                //
                //                        HUD.hideUIBlockingIndicator()
                //                        //self.performSegueWithIdentifier("toCorporateOutSumm", sender: self)
                //                    }
                //                    else {
                //                        HUD.hideUIBlockingIndicator()
                //                        //self.performSegueWithIdentifier("toSalesPerfBranch", sender: self)
                //
                //                    }
                //
                //                })
                //            }
                
                //Convert server json response to NSDictionary
                //                        do {
                //                            if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                //
                //                                // Print out dictionary
                //                                print(convertedJsonIntoDict)
                //
                //                                // Get value by key
                //                                let firstNameValue = convertedJsonIntoDict["userName"] as? String
                //                                print(firstNameValue!)
                //
                //                            }
                //                        }
                //                        catch let error as NSError {
                //                            print(error.localizedDescription)
                //                        }
                //            if self.branchLabel.text == "All"{
                //
                //
                //                self.performSegueWithIdentifier("toProductAll", sender: self)
                //            }
                //            else{
                //                //getDataProductAll()
                //                if self.check == true{
                //
                //                    print(self.productStock)
                //                    self.performSegueWithIdentifier("toProductBranch", sender: self)
                //                }
                //                else {
                //                    HUD.hideUIBlockingIndicator()
                //                    Common().showAlert(Constants().APP_NAME, message: "Something went wrong..Please try again", viewController: self)
                //
                //                }
                //            }
                //
                
            }
            
            task.resume()
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
            //check = false
            if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
                if (elementName == "Table1"){
                    if elementName == "Column1" {
                        noData = false
                        elementValue = String()
                        element = elementName
                        noDataDic[elementName] = ""
                    }
                }
                if (elementName == "CODE" || elementName == "CUSTOMER"){
                    noData = false
                    elementValue = String()
                    element = elementName
                    customer[elementName] = ""
                }
                
                if elementName == "PR_NAME" || elementName == "PR_ID" {
                    elementValue = String()
                    element = elementName
                    productList[elementName] = ""
                }
                
                
               
                if elementName == "SO_CUST_CODE" || elementName == "brn_id" || elementName == "brn_name" || elementName == "PR_MFG_ID" || elementName == "MANUFATURE" || elementName == "PR_CAT_ID" || elementName == "PRODUCT" || elementName == "Qty" || elementName == "AMOUNT" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    corporate[elementName] = ""
                }
                if elementName == "CUST_COMPANY_GROUP" || elementName == "SO_CUST_CODE" || elementName == "brn_id" || elementName == "brn_name" || elementName == "MANUFACTURE_ID" || elementName == "PR_CAT_ID" || elementName == "Qty" || elementName == "amount" {
                    noData = false
                    elementValue = String()
                    element = elementName
                    corporate[elementName] = ""
                }
                if(elementName == "Column1"){
                    noData = false
                    elementValue = String()
                    element = elementName
                    branchtgt[elementName] = ""
                }
                
                
            }
            
        }
        
        func parser(parser: NSXMLParser, foundCharacters string: String) {
            //        if elementValue != nil {
            //            elementValue! += string
            //
            //
            //
            //        }
            
            if elementValue != nil{
                
                
                //print("String", string)
                if (element == "CODE" || element == "CUSTOMER"){
                    if (string.rangeOfString("\n") == nil){
                        
                        if element == "CODE" {
                            c = 0
                            customer[element!] = string
                            
                        }
                        else if element == "CUSTOMER" {
                            if c > 0 {
                                customer[element!] = customer[element!]! + string
                            }
                            else {
                                customer[element!] = string
                            }
                            c += 1
                        }
                        
                        
                    }
                    
                }
                
                if element == "PR_NAME" || element == "PR_ID" {
                    if string.rangeOfString("\n") == nil{
                        productList[element!] = string
                    }
                }
                
                if element == "SO_CUST_CODE" || element == "brn_id" || element == "brn_name" || element == "PR_MFG_ID" || element == "MANUFATURE" || element == "PR_CAT_ID" || element == "PRODUCT" || element == "Qty" || element == "AMOUNT" {
                    if string.rangeOfString("\n") == nil{
                        corporate[element!] = string
                    }
                }
                if element == "CUST_COMPANY_GROUP" || element == "SO_CUST_CODE" || element == "brn_id" || element == "brn_name" || element == "MANUFACTURE_ID" || element == "PR_CAT_ID" || element == "Qty" || element == "amount" {
                    if string.rangeOfString("\n") == nil{
                        corporate[element!] = string
                    }
                    
                }
                if element == "Column1" {
                    if string.rangeOfString("\n") == nil {
                        branchtgt[element!] = string
                    }
                }
                if (element == "Table1"){
                    if element == "Column1" {
                        noDataDic[element!] = string
                    }
                }
                
                
            }
            
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
        
        
        
        
        
        func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
            
            if elementName == "CUSTOMER" {
                customerArray.addObject(customer)
                
            }
            if (elementName == "VALUE"){
                custSummArray.addObject(custSumm)
                custDetArray.addObject(custDet)
                count = 0
            }
            
            if elementName == "PR_ID" {
                productListArray.addObject(productList)
            }
            
            if elementName == "AMOUNT" {
                branchSumArray.addObject(branchSum)
                branchDetArray.addObject(branchDet)
                countBranchSum = 0
                countBranchDet = 0
            }
            if elementName == "amount" {
                outstandingSumArray.addObject(outstandingSum)
            }
            if elementName == "Days" {
                outstandingDetArray.addObject(outstandingDet)
            }
            if elementName == "VALUE" {
                salesItemAllArray.addObject(salesItemAll)
                
            }
            if elementName == "VALUE" {
                salesItemBranchArray.addObject(salesItemBranch)
            }
            
            if elementName == "NAME" {
                executiveListArray.addObject(executiveList)
            }
            
            if elementName == "Amount" {
                executiveSummArray.addObject(executiveSumm)
            }
            if elementName == "Column1" {
                branchtgtArray.addObject(branchtgt)
            }
            if elementName == "Table1" {
                if elementName == "Column1" {
                    noDataArray.addObject(noDataDic)
                }
            }
            if elementName == "amount" {
                corporateArray.addObject(corporate)
            }
            
            //        if elementName == "PRODUCT_SELL_RATE" {
            //            priceArray.addObject(price)
            //        }
            //        if (element == "EXPIRY"){
            //            productExpiry.addObject(expiry)
            //        }
            
        }
        
        
        
    

}
