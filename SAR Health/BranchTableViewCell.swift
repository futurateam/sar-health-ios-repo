//
//  BranchTableViewCell.swift
//  SAR Health
//
//  Created by Prajeesh KK on 08/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class BranchTableViewCell: UITableViewCell {

    @IBOutlet var branchLabel: UILabel!
    @IBOutlet var resultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
