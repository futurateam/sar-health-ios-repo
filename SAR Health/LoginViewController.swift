//
//  LoginViewController.swift
//  SAR Health
//
//  Created by Prajeesh KK on 08/04/16.
//  Copyright © 2016 Prajeesh KK. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, NSXMLParserDelegate{

    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    
    
    var tap = UITapGestureRecognizer();
    var loginDetArray : NSMutableArray = []
    var elementValue: String?
    var element : String?
    var success = false
    var loginDet = [String : String]()
    var arrayValue : NSDictionary = [String : String](), noData : Bool = false
    
//    var parser = NSXMLParser()
//    var posts = NSMutableArray()
//    var elements = NSMutableDictionary()
//    var element = NSString()
//    var batch = NSMutableString()
//    var branch = NSMutableString()
//    var expiry = NSMutableString()
//    var stock = NSMutableString()
//
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getData()
        
        tap = UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard"))
        self.view.addGestureRecognizer(tap);
        // Do any additional setup after loading the view.
        if(NSUserDefaults.standardUserDefaults().valueForKey("UserId") != nil){
            performSegueWithIdentifier("toDashboard", sender: self)
        }
        username.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginPressed(sender: AnyObject) {
        
       //print("Array Value", elementArray[0].valueForKey("BATCH"))
        //performSegueWithIdentifier("toDashboard", sender: self)
        if username.text == "" && password.text == "" {
            Common().showAlert(Constants().APP_NAME, message: "Please Enter Username and Password", viewController: self)
        }
        else if username.text == "" {
            Common().showAlert(Constants().APP_NAME, message: "Please Enter Username", viewController: self)
        }
        else if password.text == "" {
            Common().showAlert(Constants().APP_NAME, message: "Please Enter Password", viewController: self)
        }
        else {
            view.endEditing(true)
            loginDetArray.removeAllObjects()
            getLoginDet()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func dismissKeyboard(){
         view.endEditing(true)
    }
    func getLoginDet ()
    {
        
       // let scriptUrl = Constants().API + "MOBAPP_USERS?Username=" + username.text! + "&password=" + password.text!
        let scriptUrl1 = Constants().API + "MOBAPP_USERS?Username=" + username.text! + "&password=" + password.text!
        
        let scriptUrl = scriptUrl1.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        // Add one parameter
        //let urlWithParams = scriptUrl + "?BRANCH=1"
        
        // Create NSURL Ibject
        let myUrl = NSURL(string: scriptUrl);
        print("URL",scriptUrl)
        // Creaste URL Request
        let request = NSMutableURLRequest(URL:myUrl!);
        
        // Set request HTTP method to GET. It could be POST as well
        request.HTTPMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
        let username = "myUserName"
        let password = "myPassword"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
        */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                
                Common().showAlert(Constants().APP_NAME, message: "Unable to Connect to Server", viewController: self)
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let parser = NSXMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            print("Login array", self.loginDetArray)
            dispatch_async(dispatch_get_main_queue(), {
                
                if (self.noData == true){
                    HUD.hideUIBlockingIndicator()
                    Common().showAlert(Constants().APP_NAME, message: "Invalid User", viewController: self)
                    
                    
                }
                else{
                    
                    HUD.hideUIBlockingIndicator()
                    NSUserDefaults.standardUserDefaults().setObject(self.loginDetArray[0].valueForKey("UserId"), forKey: "UserId")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    self.performSegueWithIdentifier("toDashboard", sender: self)
                    
                    
                    
                }
            })
            
            

            // Convert server json response to NSDictionary
//            do {
//                if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
//                    
//                    // Print out dictionary
//                    print(convertedJsonIntoDict)
//                    
//                    // Get value by key
//                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
//                    print(firstNameValue!)
//                    
//                }
//            } catch let error as NSError {
//                print(error.localizedDescription)
//            }
            
        }
        
        task.resume()
        
        
    }
        
        
    
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //check = false
        if (elementName != "Table" && elementName != "NewDataSet" && elementName != "") {
            if (elementName == "Table1"){
                noData = true
            }
            if (elementName == "UserId" || elementName == "Password"){
                noData = false
                elementValue = String()
                element = elementName
                loginDet[elementName] = ""
            }
           
            //            if (elementName == "brn_name" || elementName == "PRODUCT_BUY_RATE" || elementName == "PRODUCT_SELL_RATE"){
            //                elementValue = String()
            //                element = elementName
            //                price[elementName] = ""
            //
            //            }
            //            if (elementName == "BRANCH" || elementName == "BATCH" || elementName == "STOCK" || elementName == "EXPIRY"){
            //                elementValue = String()
            //                element = elementName
            //                expiry[elementName] = ""
            //
            //            }
            
            
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //        if elementValue != nil {
        //            elementValue! += string
        //
        //
        //
        //        }
        
        if elementValue != nil{
            
            
            //print("String", string)
            if (element == "UserId" || element == "Password"){
                if (string.rangeOfString("\n") == nil){
                    loginDet[element!] = string
                }
                
            }
        
            //            if(element == "brn_name" || element == "PRODUCT_BUY_RATE" || element == "PRODUCT_SELL_RATE"){
            //                if (string.rangeOfString("\n") == nil){
            //                    price[element!] = string
            //                }
            //            }
            //            if (element == "BRANCH" || element == "BATCH" || element == "STOCK" || element == "EXPIRY"){
            //                if (string.rangeOfString("\n") == nil){
            //                    expiry[element!] = string
            //                }
            //            }
            
            
        }
        
    }
    
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "Password" {
            loginDetArray.addObject(loginDet)
            
        }
        
        
        //        if elementName == "PRODUCT_SELL_RATE" {
        //            priceArray.addObject(price)
        //        }
        //        if (element == "EXPIRY"){
        //            productExpiry.addObject(expiry)
        //        }
        
    }
    
    
  }


